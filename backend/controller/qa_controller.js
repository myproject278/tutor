const db = require("../db/dbConnection");
const { getResult } = require("../utils/util");

const add_question = (req, res) => {
  const user_id = req.user.user_id;
  //id,type , name ,image_url ,qa_text , date , parent_id ,user_id
  const { title, body, tags } = req.body;
  const statement = `insert into doubts(title, body, tags,user_id) values(
        '${title}','${body}','${tags}',${user_id}
    )`;
  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};
const add_answer = (req, res) => {
  const user_id = req.user.user_id;
  //id,type , name ,image_url ,qa_text , date , parent_id ,user_id
  const { question_id, answer } = req.body;
  const statement = `insert into answers(answer,user_id,question_id) values(
        '${answer}',${user_id},${question_id}
    )`;
  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};

const get_questions = (req, res) => {
  const { post_id } = req.params;
  const statement = `SELECT q.*, u.name, u.image_url, (select count(*) from answers a where a.question_id = q.id) answer_count
  FROM doubts q
  INNER JOIN user u ON q.user_id = u.id
  ORDER BY q.id DESC;`;

  db.query(statement, (error, data) => {
    console.log(data);
    res.send(getResult(error, data));
  });
};

const get_answer_by_question_id = (req, res) => {
  const { question_id } = req.params;
  let statement = `SELECT q.*, u.name, u.image_url, (select count(*) from answers a where a.question_id = q.id) answer_count
   FROM doubts q
   INNER JOIN user u ON q.user_id = u.id
   where q.id = ${question_id};`;
  db.query(statement, (error, data) => {
    if (error) res.send(getResult(error));
    else if (data.length == 0) res.send(getResult("Invalid Question id !!!"));
    else {
      statement = `select a.*, u.name, u.image_url answer_user_image from answers a
      INNER JOIN user u ON a.user_id = u.id
    where a.question_id=${question_id}`;
      db.query(statement, (err, d) => {
        if (err) res.send(getResult(err));
        else res.send(getResult(null, { question: data[0], anwers: d }));
      });
    }
  });
};

const get_questions_by_tags_search = (req, res) => {
  const { search, tags } = req.query;

  let conditions = [];

  if (search && search.length > 0) {
    conditions.push(`q.title LIKE '%${search}%' OR u.name LIKE '%${search}%'`);
  }

  if (tags && tags.length > 0) {
    conditions.push(`q.tags REGEXP '${tags}'`);
  }

  let whereClause = "";
  if (conditions.length > 0) {
    whereClause = "WHERE (" + conditions.join(" OR ") + " )";
  }

  let statement = `
  SELECT q.*, u.name, u.image_url, (select count(*) from answers a where a.question_id = q.id) answer_count
  FROM doubts q
  INNER JOIN user u ON q.user_id = u.id
    ${whereClause}
  `;

  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};

module.exports = {
  add_question,
  add_answer,
  get_questions,
  get_answer_by_question_id,
  get_questions_by_tags_search,
};
