const db= require('../db/dbConnection')
const { getResult } = require('../utils/util')


const add_review = (req, res) => {
    console.log('in add ')
    const student_id = req.user.user_id;
    console.log(req.user)
    const {  description, rating, subject_id ,teacher_id} = req.body
    const images = req.files.map((file) => {
        return { img: "/public/" + file.filename };
    });
    const image_urls = JSON.stringify(images);
    const statement = `insert into reviews(student_id,subject_id,description,image_urls,rating,teacher_id) values(
        ${student_id},${subject_id},'${description}','${image_urls}',${rating},${teacher_id}
    )`
    db.query(statement , (error,data)=>{
        res.send(getResult(error , data))
    })
}
const get_review = (req, res) => {
    const student_id = req.user.user_id
    const tutor_subject_id = req.params.tutor_subject_id
    const statement = `select r.title,r.description,r.image_urls,r.rating,r.date,u.name from reviews r 
    inner join user u on r.student_id=u.id where r.tutor_subject_id=${tutor_subject_id} and r.student_id = ${student_id}`
    console.log(statement)
    db.query(statement , (error,data)=>{
        res.send(getResult(error , data))
    })
}

const get_review_for_teacher = (req, res) => {

    const { subject_id, teacher_id } = req.params
    console.log(subject_id)
    const statement = `select r.description,r.image_urls,r.rating,r.date,u.name,u.image_url as student_image from reviews r 
    inner join user u on r.student_id=u.id where r.subject_id=${subject_id} and r.teacher_id=${teacher_id}`;
    
    db.query(statement , (error,data)=>{
        res.send(getResult(error , data))
    })

}

module.exports = {
    add_review,
    get_review_for_teacher,
    get_review
}