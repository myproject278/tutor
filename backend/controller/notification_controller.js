const db = require("../db/dbConnection");
const { getResult } = require("../utils/util");

const add_notification = (req, res) => {
  const { type } = req.params;
  //id,type , name ,image_url ,qa_text , date , parent_id ,user_id
  const { message, redirect_link, user_id } = req.body;
  console.log(req.body);
  const statement = `insert into notifications(message ,redirect_link,user_id ) values(
        '${message}','${redirect_link}',${user_id}
    )`;
  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};

const get_notifications = (req, res) => {
  const { type } = req.params;
  const user_id = req.user.user_id;

  const statement = `select id,message , redirect_link , is_read from notifications where user_id= ${user_id} order by id desc`;
  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};

const handle_notification_read = (req, res) => {
  const { notification_id } = req.body;
  const statement = `update notifications set is_read=1 where id = ${notification_id}
      `;
  console.log(statement);
  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};

const get_unread_msg_count = (req, res) => {
  const { type } = req.params;
  const user_id = req.user.user_id;

  const statement = `select count(id) count from notifications where user_id= ${user_id}  and is_read=0`;
  db.query(statement, (error, data) => {
    res.send(getResult(error, data[0]));
  });
};
module.exports = {
  add_notification,
  get_notifications,
  handle_notification_read,
  get_unread_msg_count,
};
