const db= require('../db/dbConnection')
const {getResult} = require('../utils/util')


//Transactions(student_id , teacher_id , request_id  , total_rate  ,time )
const add_transaction_for_student= (req,res)=>{
    const student_id = req.user.user_id
    const {teacher_id , request_id  , total_rate }=req.body
    const statement = `insert into Transactions(teacher_id , request_id  , total_rate, student_id)
     values(${teacher_id} , ${request_id}  , ${total_rate}, ${student_id})`
    db.query(statement , (error,data)=>{
        res.send(getResult(error , data))
    })
}
////Transactions(student_id , teacher_id , request_id  , total_rate  ,time )

const get_transcations_for_student = (req,res)=>{
    const student_id = req.user.user_id
    const {page ,rowsPerPage} = req.params
    const statement = `select t.total_rate,,t.time from Transactions t where
          t.student_id=${student_id} order by id desc limit ${page*rowsPerPage} , ${rowsPerPage}`
    db.query(statement , (error,data)=>{
        res.send(getResult(error , data))
    })
}

////Transactions(student_id , teacher_id , request_id  , total_rate  ,time )

const get_count_transcations_for_student = (req,res)=>{
    const student_id = req.user.user_id
    const statement = `select count(*) as count from Transactions  where student_id=${student_id}`
    db.query(statement , (error,data)=>{
        res.send(getResult(error , data[0].count))
    })
}
// const get_transcations_for_user_owner = (req,res)=>{
//     const student_id = req.user.user_id
//     const owner_id = req.params.owner_id
//     const {page ,rowsPerPage} = req.params
//     const statement = `select t.*,u.name from Transactions t inner join user u on t.owner_id=u.id where
//           t.user_id=${student_id} and t.owner_id=${owner_id} order by id desc limit ${page*rowsPerPage} , ${rowsPerPage}`
//     db.query(statement , (error,data)=>{
//         res.send(getResult(error , data))
//     })
// }
// const get_count_transcations_for_user_and_owner = (req,res)=>{
//     const user_id = req.user.user_id
//     const owner_id = req.params.owner_id
//     const statement = `select count(*) as count from Transactions  where user_id=${user_id}  and owner_id=${owner_id} `
//     db.query(statement , (error,data)=>{
//         res.send(getResult(error , data[0].count))
//     })
// }
// const get_users_due = (req,res)=>{
//     const user_id = req.user.user_id
//     const {page ,rowsPerPage} = req.params
//     const statement = `select uo.*,u.name,u.email,u.mobile_no from user_owner_due uo inner join user u on uo.owner_id = u.id where
//      uo.user_id=${user_id} and remaining_amount!=0 order by remaining_amount limit ${page*rowsPerPage} , ${rowsPerPage}`
//     console.log(statement)
//      db.query(statement , (error,data)=>{
//         res.send(getResult(error , data))
//     })
// }
// const get_count_user_owner_due = (req,res)=>{
//     const user_id = req.user.user_id
//     const statement = `select count(*) as count from user_owner_due where  user_id=${user_id} and remaining_amount!=0`
//     db.query(statement , (error,data)=>{
//         res.send(getResult(error , data[0].count))
//     })
// }
const get_student_transactions_amt_for_dates = (req,res)=>{
    const {start_date,end_date} = req.params
    const student_id = req.user.user_id 
    let statement= `SELECT
    SUM(total_rate) as  total
  FROM
    transactions where time >= '${start_date}' and time<='${end_date}' and student_id=${student_id}`

    db.query(statement , (error,data)=>{
        res.send(getResult(error , data))
    })
   
   
}


const get_total_paid_amount_student= (req,res)=>{
    const student_id = req.user.user_id
    const statement = `select sum(total_rate)as total from  where student_id=${student_id}`
    db.query(statement , (error,data)=>{
        res.send(getResult(error , data[0].total))
    })
}

module.exports = {
    add_transaction_for_student,
    get_count_transcations_for_student,
    get_transcations_for_student,
    get_student_transactions_amt_for_dates,
    get_total_paid_amount_student
}