
const db= require('../db/dbConnection')
const {getResult} = require('../utils/util')




const add_subject_info = (req,res) => {
    let {name, parent_id}=req.body
    
    if(parent_id == undefined )
        parent_id = 0 ; 
 
    const image_url =req.file.filename;
    
    const  statement = ` insert into subjects_info(name, image_url , parent_id) value('${name}','${image_url}',${parent_id})`

    db.query(statement , (error,data)=>{
        res.send(getResult(error , data))
    })
}

//subjects_info (id , name ,image_url ,parent_id  );


const get_subject_info_by_parentId= (req,res) => {
    let parent_id = req.params.service_id
    if(parent_id==undefined)
        parent_id = 0;  
    statement =    `select * from subjects_info where parent_id = ${parent_id}`;
    db.query(statement , (error,data)=>{
        res.send(getResult(error , data))
    })
}
const get_parent_subjects= (req,res)=>{
    statement =    `select id,name from subjects_info where parent_id = 0`;
    db.query(statement , (error,data)=>{
        res.send(getResult(error , data))
    })
}
const get_top_subjects = (req, res) => {
  statement = `select * from subjects_info where parent_id = 0 limit 6`;
  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};
const get_subject_for_teacher_dropdown = (req, res) => {
  const teacher_id = req.user.user_id;
  const statement = `select id,name from subjects_info s  where parent_id != 0 and not exists ( select subject_id from tutor_subject_info ts
     where ts.teacher_id = ${teacher_id} and ts.subject_id = s.id)`;
  console.log(statement);
  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};
module.exports = {
  add_subject_info,
  get_subject_info_by_parentId,
  get_parent_subjects,
  get_top_subjects,
  get_subject_for_teacher_dropdown,
};