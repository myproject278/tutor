const db= require('../db/dbConnection')
const {getResult} = require('../utils/util')

//create table Wishlist(student_id int, tutor_subject_id int, PRIMARY KEY(student_id,tutor_subject_id))

const get_wishlist_by_student_id = (req,res)=>{
    const user_id = req.user.user_id
    const statement = `select * from Wishlist  where student_id = ${student_id}`
    db.query(statement , (error,data)=>{
        res.send(getResult(error , data))
    })
}


const add_wishlist = (req,res) => {
    const { product_id } = req.body
    const student_id = req.user.user_id
    const statement = `insert into Wishlist values(${student_id},${tutor_subject_id})`
    db.query(statement , (error,data)=>{
        res.send(getResult(error , data))
    })
}
const remove_wishlist = (req,res) => {
    const  tutor_subject_id  = req.params.id

    const student_id = req.user.user_id
    const statement = `delete from Wishlist where student_id = ${student_id} and product_id = ${tutor_subject_id}`
    db.query(statement , (error,data)=>{
        res.send(getResult(error , data))
    })
}


module.exports = {
    get_wishlist_by_student_id,
    remove_wishlist,
    add_wishlist
}