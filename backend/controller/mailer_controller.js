const { getResult } = require("../utils/util");
const nodemailer = require("nodemailer");

const transporter = nodemailer.createTransport({
  service: "gmail",
  host: "smtp.gmail.com",
  port: 587,
  secure: false,
  auth: {
    user: "tutorservice95@gmail.com",
    pass: "qrooqprtmsganwse",
  },
});
const send_mail = async (req, res) => {
  try {
    const { to, message, subject } = req.body;
    const options = {
      from: "tutor student app", // sender address
      to: to, // receiver email
      subject: subject,
      text: message,
      html: `<h1>${message} </h1>`,
    };

    const info = await transporter.sendMail(options);
    console.log(info);
    res.send(getResult(false, "Mail Send Successfully"));
  } catch (error) {
    res.send(getResult(error));
  }
};

const send_sign_up_mail = async (req, res) => {
  try {
    const { to, message, subject } = req.body;
    const options = {
      from: "tutor student app", // sender address
      to: to, // receiver email
      subject: subject,
      text: message,
      html: message,
    };

    const info = await transporter.sendMail(options);
    console.log(info);
    res.send(
      getResult(
        false,
        "Account is created Successfully , please confirm email address on Gmail !!!"
      )
    );
  } catch (error) {
    res.send(getResult(error));
  }
};

module.exports = {
  send_mail,
  send_sign_up_mail,
};
