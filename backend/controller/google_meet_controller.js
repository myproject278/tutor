const { google } = require("googleapis");
const { web } = require("../goole/google_mee_credentials.json");
const { client_id, client_secret, redirect_uris } = web;
const db = require("../db/dbConnection");

const SCOPES = ["https://www.googleapis.com/auth/calendar.events"];

console.log(redirect_uris);
const oAuth2Client = new google.auth.OAuth2(
  client_id,
  client_secret,
  redirect_uris
);

const scheduled_meeting = async (req, res) => {
  const data = req.body;

  try {
    const url = oAuth2Client.generateAuthUrl({
      access_type: "offline",
      scope: SCOPES,
      state: JSON.stringify(data),
    });

    res.send(url);
  } catch (error) {
    res.status(500).json({ error: "Error scheduling Google Meet link." });
  }
};
const get_request = (request_id) => {
  return new Promise((resolve, reject) => {
    const statement = `select approve_time, total_hours from requests where id = ${request_id}`;

    db.query(statement, (error, data) => {
      if (error) {
        console.error(error);
        reject(error);
      } else if (data.length > 0) {
        resolve(data[0]);
      } else {
        resolve(null); // Resolve with null if no data is found
      }
    });
  });
};

const add_meet_link_in_request = async (request_id, meetLink) => {
  const statement = `update requests set meet_link = '${meetLink}' , status = 2  where id = ${request_id}`;
  db.query(statement, (error, data) => {});
};

const s2 = async (req, res) => {
  try {
    const { teacher_email, student_email, request_id } = JSON.parse(
      req.query.state
    );

    const code = req.query.code;
    if (code) {
      const { tokens } = await oAuth2Client.getToken(code);
      oAuth2Client.setCredentials(tokens);
    }
    const calendar = google.calendar({ version: "v3", auth: oAuth2Client });

    const { approve_time, total_hours } = await get_request(request_id);
    // Convert selectedTime to ISO format
    const isoTime = new Date(approve_time);
    isoTime.setHours(isoTime.getHours() + total_hours);
    const event = {
      summary: "Scheduled Meeting by Tutor Application",
      start: { dateTime: new Date(approve_time).toISOString() },
      end: { dateTime: isoTime.toISOString() },
      attendees: [{ email: student_email }, { email: teacher_email }],
      conferenceData: {
        createRequest: { requestId: "my-conference-request" },
      },
    };

    const createdEvent = await calendar.events.insert({
      calendarId: "primary",
      resource: event,
      conferenceDataVersion: 1,
    });

    const meetLink = createdEvent.data.hangoutLink;
    add_meet_link_in_request(request_id, meetLink);
    res.send(`<!DOCTYPE html>
        <html lang="en">
        <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Success Page</title>
        <script>
          // Function to close the window after 2 seconds
          setTimeout(function() {
            window.close();
          }, 2000);
        </script>
        </head>
        <body>
        <div style="text-align: center; padding: 50px;">
          <h1>Success!</h1>
          <p>Your request was completed successfully.</p>
        </div>
        </body>
        </html>`);
  } catch (error) {
    res.status(500).json({ error: "Error scheduling Google Meet link." });
  }
};

module.exports = {
  scheduled_meeting,
  s2,
};
