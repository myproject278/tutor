const db = require("../db/dbConnection");
const { getResult } = require("../utils/util");

const get_teacher_info_by_subject = (req, res) => {
  let { subject_id, sorting_column, page_id } = req.params;
  const { days_of_availability, expertise_level, teacher_name } = req.query;
  const days = days_of_availability.split(",");

  let statement = `select t.id,t.name ,hourly_rate , header_info,other_info,level_of_knowledge,t.image_url from tutor_subject_info s inner join user t
     on s.teacher_id=t.id where subject_id = ${subject_id} `;
  if (expertise_level > 0)
    statement = `${statement} and  level_of_knowledge = ${expertise_level}`;
  if (days.length > 0) {
    const v = days.join(" = 1 OR ");
    statement = `${statement} and (${v} = 1)`;
  }
  if (teacher_name && teacher_name.length > 0)
    statement = `${statement} and t.name like '%${teacher_name}%'`;
  statement = `${statement} order by ${sorting_column} limit ${page_id * 9},9`;
  console.log(statement);
  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};

const get_top_teachers = (req, res) => {
  const statement = `select t.name ,hourly_rate ,header_info, level_of_knowledge,t.image_url from tutor_subject_info s inner join user t
    on s.teacher_id=t.id  order by total_reviews limit 8`;
  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};

const get_subject_teacher_by_id = (req, response) => {
  const { teacher_id, subject_id } = req.params;

  const statement = `select s.teacher_id,s.subject_id,t.name ,hourly_rate , header_info,level_of_knowledge,t.image_url,t.email,t.phone_no,
      t.secound_last_education,t.last_education,t.bio,t.available_slots,t.speaking_languaes,other_info from tutor_subject_info s inner join user t
    on s.teacher_id=t.id where s.teacher_id = ${teacher_id} and s.subject_id=${subject_id}`;
  db.query(statement, (error, data) => {
    let res = {};
    if (error) res = getResult(error);
    else if (data.length == 0) res = getResult("Invalid data");
    else res = getResult(error, data[0]);
    response.send(res);
  });
};

const add_subject_tutor = (req, res) => {
  const teacher_id = req.user.user_id;
  const {
    subject_id,
    hourly_rate,
    level_of_knowledge,
    header_info,
    other_info,
  } = req.body;

  const statement = `insert into tutor_subject_info(teacher_id ,subject_id ,hourly_rate ,level_of_knowledge ,header_info ,other_info)
       values(${teacher_id},${subject_id} , ${hourly_rate},${level_of_knowledge},'${header_info}','${other_info}')`;
  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};

const update_subject_tutor = (req, res) => {
  const teacher_id = req.user.user_id;
  const {
    subject_id,
    hourly_rate,
    level_of_knowledge,
    header_info,
    other_info,
  } = req.body;

  const statement = `update  tutor_subject_info set 
        hourly_rate = ${hourly_rate} ,
        level_of_knowledge =${level_of_knowledge},
        header_info ='${header_info}',
        other_info ='${other_info}'
        where teacher_id = ${teacher_id} and subject_id = ${subject_id}`;
  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};

const get_subject_list_for_teacher = (req, res) => {
  const { teacher_id } = req.params;
  const statement = `select ts.hourly_rate ,ts.level_of_knowledge,s.image_url,s.name,s.id from tutor_subject_info ts inner join 
    subjects_info s
  on s.id=ts.subject_id where ts.teacher_id = ${teacher_id} `;
  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};

const get_teacher_side_subject_list = (req, res) => {
  const teacher_id = req.user.user_id;
  const statement = `select ts.hourly_rate ,ts.level_of_knowledge,header_info,other_info,s.name,s.id  from tutor_subject_info ts inner join 
    subjects_info s
  on s.id=ts.subject_id where ts.teacher_id = ${teacher_id} `;
  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};

const get_subject_info_for_update = (req, res) => {
  const teacher_id = req.user.user_id;
  const { subject_id } = req.params;
  const statement = `select ts.hourly_rate ,ts.level_of_knowledge,header_info,other_info from 
  tutor_subject_info ts 
     where ts.teacher_id = ${teacher_id} and ts.subject_id = ${subject_id}`;
  db.query(statement, (error, data) => {
    let result = {};
    if (error) {
      result = getResult(error);
    } else if (data.length == 0) {
      result = getResult("Invalid Subject");
    } else {
      result = getResult(error, data[0]);
    }
    res.send(result);
  });
};

module.exports = {
  get_teacher_info_by_subject,
  get_top_teachers,
  get_subject_teacher_by_id,
  add_subject_tutor,
  get_subject_list_for_teacher,
  get_teacher_side_subject_list,
  get_subject_info_for_update,
  update_subject_tutor,
};
