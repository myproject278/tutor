const db= require('../db/dbConnection')
const {getResult} = require('../utils/util')


const get_user_address = (req, res) => {
    const { user_id } = req.params
    const statement = `select * from address where user_id = ${user_id}`
    db.query(statement, (error, data) => {
        res.send(getResult(error , data))
    })
}

module.exports = {
    get_user_address
}