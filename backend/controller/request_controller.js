const db= require('../db/dbConnection')
const {getResult} = require('../utils/util')



const addReqeustFromStudent = (req, res) => {
  const student_id = req.user.user_id;
  console.log(req.user);
  const { subject_id, total_hours, timeSlots, goal_of_tutoring, teacher_id } =
    req.body;
  const statement = `insert into requests(subject_id,teacher_id , student_id ,total_hours ,slot_1,slot_2,
        slot_3,goal_of_tutoring)
     values(${subject_id} , ${teacher_id},${student_id} ,${total_hours} ,${
    timeSlots.length > 0 ? `'${timeSlots[0]}'` : null
  },${timeSlots.length > 1 ? `'${timeSlots[1]}'` : null},  ${
    timeSlots.length > 2 ? `'${timeSlots[2]}'` : null
  },'${goal_of_tutoring}')`;
  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};

const getRequestsByStudentId = (req, res) => {
  const student_id = req.user.user_id;
  const { search, status } = req.query;
  console.log(search);
  console.log(status);

  let statement = `select r.id,r.total_hours ,r.slot_1,r.slot_2,
    r.slot_3,r.status,r.approve_time ,r.request_time,r.goal_of_tutoring ,
    s.image_url subject_image,s.name subject_name,t.name teacher_name ,t.image_url teacher_image from requests r inner join subjects_info s on
    r.subject_id = s.id  inner join user t on t.id=r.teacher_id where r.student_id=${student_id} `;
  if (search)
    statement = `${statement} and s.name like '%${search}%' OR t.name like '%${search}%'`;
  if (status) statement = `${statement} and r.status = ${status}`;
  statement = `${statement} order by id desc`;
  console.log(statement);
  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};

const get_request_by_id = (req, res) => {
  const id = req.params.id;
  const statement = `select r.id,r.total_hours ,r.slot_1,r.meet_link,r.slot_2,
  r.slot_3,r.status,r.approve_time ,r.request_time,r.goal_of_tutoring ,
  s.image_url subject_image,s.name subject_name,t.name teacher_name ,t.id teacher_id,t.image_url teacher_image,t.phone_no,
  t.email,t.bio,t.current_position from requests r inner join subjects_info s on
  r.subject_id = s.id  inner join user t on t.id=r.teacher_id where r.id=${id}`;
  db.query(statement, (error, data) => {
    res.send(
      error
        ? getResult(error, data)
        : data.length == 0
        ? getResult("record not found ")
        : getResult(error, data[0])
    );
  });
};

const get_request_by_id_with_student = (req, res) => {
  const id = req.params.id;
  const statement = `select r.id,r.total_hours ,r.slot_1,r.slot_2,
  r.slot_3,r.status,r.approve_time ,r.request_time,r.goal_of_tutoring ,
  s.image_url subject_image,s.name subject_name,st.name student_name ,st.image_url student_image,
  st.email,st.phone_no from requests r inner join subjects_info s on
  r.subject_id = s.id  inner join user st on st.id=r.student_id where r.id=${id}`;
  db.query(statement, (error, data) => {
    res.send(
      error
        ? getResult(error, data)
        : data.length == 0
        ? getResult("record not found ")
        : getResult(error, data[0])
    );
  });
};

const getRequestsByTeacherId = (req, res) => {
  const teacher_id = req.user.user_id;
  const statement = `select r.id,r.total_hours,r.status,r.approve_time ,r.request_time,r.goal_of_tutoring ,
   s.name subject_name, st.name,st.phone_no,st.email from requests r inner join subjects_info s on
    r.subject_id = s.id  inner join user st on st.id=r.student_id where r.teacher_id=${teacher_id} order by r.id desc`;
  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};

const handle_submit_slot = (req, res) => {
  const { approve_time, total_hours } = req.body;
  console.log(req.body);
  const { request_id } = req.params;
  const teacher_id = req.user.user_id;
  console.log(teacher_id);

  let statement = `select available_slots from user where id  = ${teacher_id}`;
  db.query(statement, (error, data) => {
    if (error) res.send(getResult(error, data));
    else {
      const available_slots = JSON.parse(data[0].available_slots);

      const days = [
        "sunday",
        "monday",
        "tuesday",
        "wednesday",
        "thursday",
        "friday",
        "saturday",
      ];
      const ap_time = new Date(approve_time);
      const boking_day = available_slots[days[ap_time.getDay()]];
      let flag = false;
      for (let i = 0; i < total_hours; i++) {
        if (boking_day[ap_time.getHours() + i] == 2) {
          flag = true;
          break;
        }
      }
      if (flag) res.send(getResult("Slot is already booking "));
      else {
        for (let i = 0; i < total_hours; i++) {
          if (boking_day[ap_time.getHours() + i] == 0) {
            boking_day[ap_time.getHours() + i] = 2;
          }
        }
        const end_aval_slots = JSON.stringify(available_slots);
        statement = `update requests set approve_time='${approve_time}',status =1  where id = ${request_id}`;
        db.query(statement, (error, data) => {
          if (error) res.send(getResult(error));
          else {
            statement = `update user set available_slots='${end_aval_slots}' where id=${teacher_id}`;
            db.query(statement, (error, data) => {
              res.send(getResult(error, data));
            });
          }
        });
      }
    }
  });
};
const get_request_for_teacher_calender = (req, res) => {
  const teacher_id = req.user.user_id;
  const statement = `select r.id,r.total_hours,r.approve_time  ,
   s.name subject_name, st.name from requests r inner join subjects_info s on
    r.subject_id = s.id  inner join user st on st.id=r.student_id where r.teacher_id=${teacher_id} and r.status=2 order by r.id desc`;
  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};
const get_request_for_student_calender = (req, res) => {
  const student_id = req.user.user_id;
  const statement = `select r.id,r.total_hours,r.approve_time  ,
   s.name subject_name, t.name from requests r inner join subjects_info s on
    r.subject_id = s.id  inner join user t on t.id=r.teacher_id where r.student_id=${student_id} and r.status=2 order by r.id desc`;
  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};

const handle_request_state_change = (req, res) => {
  const { request_id, status } = req.body;
  const statement = `update requests set status = ${status} where id = ${request_id}`;
  console.log(statement);
  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};

module.exports = {
  addReqeustFromStudent,
  getRequestsByStudentId,
  get_request_by_id,
  getRequestsByTeacherId,
  get_request_by_id_with_student,
  handle_submit_slot,
  get_request_for_teacher_calender,
  get_request_for_student_calender,
  handle_request_state_change,
};



