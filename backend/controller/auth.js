const db = require("../db/dbConnection");
const crypto = require("crypto-js");
const jwt = require("jsonwebtoken");
const { getResult, secret } = require("../utils/util");
const { content } = require("googleapis/build/src/apis/content");

//student -   (id , name , email ,password ,phone_no , type , image_url )

const signup = (req, res, next) => {
  const { name, phone_no, email, password } = req.body;
  const encryptPassword = crypto.SHA256(password);
  const image_url = req.file.filename;
  const statement = `insert into user(name,phone_no,email,password,image_url,type) values('${name}','${phone_no}','${email}',
    '${encryptPassword}','${image_url}',1)`;
  db.query(statement, (error, data) => {
    const result = {};
    if (error) {
      result.status = "error";
      result.error = error;
      res.send(result);
    } else {
      const query = `select * from user where id=${data.insertId}`;
      db.query(query, (error, data) => {
        if (error) {
          result.status = "error";
          result.error = "User is created but internal failure";
          res.send(result);
        } else {
          // console.log(data)
          // result.status = "success";
          const ret = data[0];

          const token = jwt.sign(
            { user_id: ret.id, role: "STUDENT", authenticated: 0 },
            secret
          );
          // result.data = {
          //   id: ret.id,
          //   name: ret.name,
          //   phone_no: ret.phone_no,
          //   email: ret.email,
          //   image_url: ret.image_url,
          // };
          // result.token = token;
          // res.send(result);
          req.body = {
            to: ret.email,
            subject: "Confirm your Email",
            message: `<!DOCTYPE html>
              <html lang="en">
              <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Welcome to Tutor App</title>
              </head>
              <body style="font-family: Arial, sans-serif; text-align: center;">
                <table style="max-width: 600px; margin: 0 auto; padding: 20px;">
                  <tr>
                    <td>
                      <h1>Welcome to Tutor App</h1>
                      <p>Please confirm your email to get started.</p>
                      <a href="http://localhost:3001/student/confirm/${token}" style="display: inline-block; padding: 10px 20px; background-color: #007bff; color: #fff; text-decoration: none; border-radius: 5px;">Confirm Email</a>
                      
                      <p>Thank you for joining us!</p>
                      <p>The Tutor App Team</p>
                    </td>
                  </tr>
                </table>
              </body>
              </html>
              `,
          };
          next();
        }
      });
    }
  });
};

const signin = (req, res) => {
  const { email, password } = req.body;
  console.log(email);
  console.log(password);
  const encryptPassword = crypto.SHA256(password);
  const statement = `select * from user where email='${email}'`;
  db.query(statement, (error, data) => {
    const result = {};
    if (error) {
      result.status = "error";
      result.error = error;
    } else if (data.length == 0) {
      result.status = "error";
      result.error = "invalid email";
    } else if (
      data[0].password == encryptPassword &&
      data[0].authenticated == 0
    ) {
      result.status = "error";
      result.error = "please confirm the email first";
    } else if (data[0].password == encryptPassword) {
      result.status = "success";
      const ret = data[0];
      const token = jwt.sign(
        { user_id: ret.id, type: ret.type, authenticated: 1 },
        secret
      );
      result.data = {
        id: ret.id,
        name: ret.name,
        phone_no: ret.phone_no,
        email: ret.email,
        image_url: ret.image_url,
      };
      result.token = token;
    } else {
      result.status = "error";
      result.error = "invalid password";
    }
    res.send(result);
  });
};

const get_user_info = (req, response) => {
  const user_id = req.user.user_id;

  const statement = `select  name ,phone_no ,email,
  image_url ,secound_last_education ,last_education ,current_position , bio 
        from user where id = ${user_id}`;
  db.query(statement, (error, data) => {
    let res = {};
    if (error) res = getResult(error);
    else if (data.length == 0) res = getResult("Invalid id");
    else res = getResult(error, data[0]);
    response.send(res);
  });
};

const confirm_user_email = (req, res) => {
  const { token } = req.params;
  const { user_id } = jwt.verify(token, secret);
  console.log(user_id);
  console.log("in confirm");
  let statement = `select authenticated from user where id = ${user_id}`;
  db.query(statement, (error, data) => {
    let err_msg = null;
    if (error) err_msg = error;
    else if (data.length == 0) err_msg = " User Not Found";
    else if (data[0].authenticated == 1) err_msg = "User Already authenticated";
    if (err_msg) {
      res.send(`<!DOCTYPE html>
        <html>
        <head>
            <title>Email authenticated</title>
            <style>
                body {
                    font-family: Arial, sans-serif;
                    background-color: #f4f4f4;
                    margin: 0;
                    padding: 0;
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    min-height: 100vh;
                }
                
                .container {
                    text-align: center;
                    padding: 20px;
                    border-radius: 10px;
                    background-color: #ffffff;
                    box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.1);
                }
                
                h1 {
                    color: #333333;
                    margin-bottom: 10px;
                }
                
                p {
                    color: #666666;
                    margin-bottom: 20px;
                }
                
                a.button {
                    display: inline-block;
                    padding: 10px 20px;
                    background-color: #007bff;
                    color: #ffffff;
                    text-decoration: none;
                    border-radius: 5px;
                    transition: background-color 0.3s ease;
                }
                
                a.button:hover {
                    background-color: #0056b3;
                }
            </style>
        </head>
        <body>
            <div class="container">
                <h1>Error Occured</h1>
                <p>${err_msg}</p>
                
            </div>
        </body>
        </html>
        
        `);
    } else {
      statement = `update user set authenticated=1 where id = ${user_id}`;
      db.query(statement, (error1, data1) => {
        let content = `<!DOCTYPE html>
    <html>
    <head>
        <title>Email authenticated</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                background-color: #f4f4f4;
                margin: 0;
                padding: 0;
                display: flex;
                justify-content: center;
                align-items: center;
                min-height: 100vh;
            }
            
            .container {
                text-align: center;
                padding: 20px;
                border-radius: 10px;
                background-color: #ffffff;
                box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.1);
            }
            
            h1 {
                color: #333333;
                margin-bottom: 10px;
            }
            
            p {
                color: #666666;
                margin-bottom: 20px;
            }
            
            a.button {
                display: inline-block;
                padding: 10px 20px;
                background-color: #007bff;
                color: #ffffff;
                text-decoration: none;
                border-radius: 5px;
                transition: background-color 0.3s ease;
            }
            
            a.button:hover {
                background-color: #0056b3;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <h1>Your email is authenticated</h1>
            <p>Thank you for confirming your email address.</p>
            
            <a class="button" href="http://localhost:3000">Go to Sign In</a>
        </div>
    </body>
    </html>
    
    `;
        if (error1)
          content = `<!DOCTYPE html>
      <html>
      <head>
          <title>Email authenticated</title>
          <style>
              body {
                  font-family: Arial, sans-serif;
                  background-color: #f4f4f4;
                  margin: 0;
                  padding: 0;
                  display: flex;
                  justify-content: center;
                  align-items: center;
                  min-height: 100vh;
              }
              
              .container {
                  text-align: center;
                  padding: 20px;
                  border-radius: 10px;
                  background-color: #ffffff;
                  box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.1);
              }
              
              h1 {
                  color: #333333;
                  margin-bottom: 10px;
              }
              
              p {
                  color: #666666;
                  margin-bottom: 20px;
              }
              
              a.button {
                  display: inline-block;
                  padding: 10px 20px;
                  background-color: #007bff;
                  color: #ffffff;
                  text-decoration: none;
                  border-radius: 5px;
                  transition: background-color 0.3s ease;
              }
              
              a.button:hover {
                  background-color: #0056b3;
              }
          </style>
      </head>
      <body>
          <div class="container">
              <h1>Error Occured while updating</h1>
              <p>${error1}</p>
              
              
          </div>
      </body>
      </html>
      
      `;
        res.send(content);
      });
    }
  });
};

module.exports = {
  signup,
  signin,
  confirm_user_email,
  get_user_info,
};
