const db = require("../db/dbConnection");
const { getResult } = require("../utils/util");

const get_question_by_subject_id = (req, res) => {
  const { subject_id } = req.params;
  const statement = `SELECT * FROM questions where subject_id =${subject_id} ORDER BY RAND() LIMIT 15;`;
  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};

const add_quiz_result = (req, res) => {
  const {
    num_correct_answers,
    num_wrong_answers,
    hints_used,
    fifty_fifty_used,
    subject_id,
  } = req.body;
  const user_id = req.user.user_id;
  const statement = `insert into quizzes(num_correct_answers ,
    num_wrong_answers ,
    hints_used,
    fifty_fifty_used,user_id,subject_id) values(${num_correct_answers} ,
    ${num_wrong_answers} ,
    ${hints_used},
    ${fifty_fifty_used},${user_id},${subject_id})`;
  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};

const get_quiz_result_by_id = (req, res) => {
  const { quiz_id } = req.params;
  const statement = `SELECT * FROM quizzes where quiz_id =${quiz_id}`;
  db.query(statement, (error, data) => {
    let result = {};
    if (error) {
      result = getResult(error);
    } else if (data.length == 0) {
      result = getResult("Invalid quiz  id ");
    } else {
      result = getResult(error, data[0]);
    }
    res.send(result);
  });
};
const get_quiz_history_result = (req, res) => {
  const user_id = req.user.user_id;
  const statement = `SELECT q.*,s.name FROM quizzes q inner join subjects_info s on q.subject_id = s.id where 
  q.user_id = ${user_id} order by q.quiz_id desc`;
  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};

const get_quiz_score_by_subject = (req, res) => {
  const user_id = req.user.user_id;
  const statement = `SELECT s.name, COALESCE(avg((q.num_correct_answers / 15) * 100), 0) AS quiz_score
   FROM subjects_info s LEFT JOIN quizzes q ON q.subject_id = s.id AND q.user_id = ${user_id} WHERE s.parent_id = 0 GROUP BY s.name`;
  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};
module.exports = {
  get_question_by_subject_id,
  add_quiz_result,
  get_quiz_result_by_id,
  get_quiz_history_result,
  get_quiz_score_by_subject,
};


