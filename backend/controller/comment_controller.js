const db = require("../db/dbConnection");
const { getResult } = require("../utils/util");

const add_comments = (req, res) => {
  const student_id = req.user.user_id;
  const { comment, post_id } = req.body;
  const statement = `insert into comments(comment , post_id,student_id) values(
        '${comment}',${post_id},${student_id}
    )`;
  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};
const get_comments_for_post = (req, res) => {
  const student_id = req.user.user_id;
  const { post_id } = req.params;
  const statement = `select c.comment,s.name,s.image_url,s.id student_id  from comments c inner join user s on 
    c.student_id= s.id where c.post_id= ${post_id}`;
  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};

module.exports = {
  add_comments,
  get_comments_for_post,
};
