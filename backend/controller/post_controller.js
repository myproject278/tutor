const db = require("../db/dbConnection");
const { getResult } = require("../utils/util");

const add_post = (req, res) => {
  const teacher_id = req.user.user_id;
  const { description, header, tags } = req.body;
  const images = req.files.map((file) => {
    return { img: file.filename };
  });
  const image_urls = JSON.stringify(images);
  const statement = `insert into posts(teacher_id,header,description,image_urls,tags) values(
        ${teacher_id},'${header}','${description}','${image_urls}'
        ,'${tags}'
    )`;
  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};
const get_post = (req, res) => {
  const statement = `select p.*,t.name from posts p inner join teachers t on p.teacher_id = t.id;`;
  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};
const get_posts_for_teacher = (req, res) => {
  const teacher_id = req.user.user_id;
  const statement = `select p.* from posts p where p.teacher_id = ${teacher_id}`;
  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};

const get_post_by_id = (req, res) => {
  const { post_id } = req.params;
  const statement = `select p.*,t.name,t.image_url teacher_image,t.current_position from posts p inner join user t on p.teacher_id = t.id
    where p.id = ${post_id}`;

  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};

const get_recent_and_famous_posts = (req, res) => {
  const statement = `select p.*,t.name from posts p inner join user t on p.teacher_id = t.id limit 3`;
  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};

const get_post_by_tags_search = (req, res) => {
  const { search, tags, teacher } = req.query;

  let conditions = [];

  if (search && search.length > 0) {
    conditions.push(`p.header LIKE '%${search}%' OR t.name LIKE '%${search}%'`);
  }

  if (tags && tags.length > 0) {
    conditions.push(`p.tags REGEXP '${tags}'`);
  }

  let whereClause = "";
  if (conditions.length > 0) {
    whereClause = "WHERE (" + conditions.join(" OR ") + " )";
  }

  let statement = `
    SELECT p.*, t.name
    FROM posts p
    INNER JOIN user t ON p.teacher_id = t.id
    ${whereClause}
  `;
  if (teacher) {
    const teacher_id = req.user.user_id;
    statement = `${statement} and p.teacher_id = ${teacher_id}`;
  }

  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};

const get_post_for_update = (req, res) => {
  const { post_id } = req.params;
  const statement = `select * from posts where id=${post_id}`;
  db.query(statement, (error, data) => {
    let result = {};
    if (error) {
      result = getResult(error);
    } else if (data.length == 0) {
      result = getResult("Invalid posts id ");
    } else {
      result = getResult(error, data[0]);
    }
    res.send(result);
  });
};

const update_post = (req, res) => {
  const { description, header, tags, id } = req.body;
  const statement = `update posts set description ='${description}',
    header = '${header}',
    tags = '${tags}' where id = ${id}`;
  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};
module.exports = {
  add_post,
  get_post,
  get_post_by_id,
  get_recent_and_famous_posts,
  get_post_by_tags_search,
  get_posts_for_teacher,
  get_post_for_update,
  update_post,
};
