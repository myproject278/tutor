const db = require("../db/dbConnection");
const crypto = require("crypto-js");
const jwt = require("jsonwebtoken");
const { getResult, secret } = require("../utils/util");

const update_teacher_info = (req, res) => {
  const teacher_id = req.user.user_id;
  const {
    name,
    email,
    phone_no,
    secound_last_education,
    last_education,
    bio,
    available_slots,
    speaking_languaes,
    sunday,
    monday,
    tuesday,
    wednesday,
    thursday,
    friday,
    saturday,
  } = req.body;
  const statement = `UPDATE user
  SET name = '${name}',
      phone_no = '${phone_no}',
      email = '${email}',
      secound_last_education = '${secound_last_education}',
      last_education = '${last_education}',
      bio = '${bio}',
      available_slots = '${available_slots}',
      speaking_languaes = '${speaking_languaes}',
      sunday = ${sunday} ,
      monday =${monday},
      tuesday = ${tuesday},
      wednesday = ${wednesday},
      thursday = ${thursday},
      friday = ${friday},
      saturday = ${saturday}
  WHERE id = ${teacher_id}`;
  db.query(statement, (error, data) => {
    const result = {};
    if (error) {
      result.status = "error";
      result.error = error;
      res.send(result);
    } else {
      const query = `select * from user where id=${teacher_id}`;
      db.query(query, (error, data) => {
        let result = {};
        if (error) result = getResult(error);
        else if (data.length == 0) result = getResult("Invalid id");
        else result = getResult(error, data[0]);
        res.send(result);
      });
    }
  });
};

const get_teacher_list = (req, res) => {
  const statement = `select id , name , current_position,image_url from user where type = 0`;
  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};

const get_teacher_by_id = (req, response) => {
  const { teacher_id } = req.params;
  const statement = `select available_slots ,bio ,current_position,email ,id , image_url , last_education ,name
    , phone_no ,secound_last_education ,speaking_languaes from user where id = ${teacher_id}`;
  db.query(statement, (error, data) => {
    let res = {};
    if (error) res = getResult(error);
    else if (data.length == 0) res = getResult("Invalid id");
    else res = getResult(error, data[0]);
    response.send(res);
  });
};

const get_teacher_info = (req, response) => {
  const { teacher_id } = req.params;
  const statement = `select available_slots ,bio ,current_position,email ,id , image_url , last_education ,name
    , phone_no ,secound_last_education ,speaking_languaes from user where id = ${teacher_id}`;
  db.query(statement, (error, data) => {
    let res = {};
    if (error) res = getResult(error);
    else if (data.length == 0) res = getResult("Invalid id");
    else res = getResult(error, data[0]);
    response.send(res);
  });
};

const get_teacher_info_request_form = (req, response) => {
  const { teacher_id } = req.params;
  const statement = `select available_slots  ,email  , image_url  ,name
      from user where id = ${teacher_id}`;
  console.log(statement);
  db.query(statement, (error, data) => {
    let res = {};
    if (error) res = getResult(error);
    else if (data.length == 0) res = getResult("Invalid id");
    else res = getResult(error, data[0]);
    response.send(res);
  });
};

const get_top_teacher = (req, res) => {
  const statement = `select id , name , current_position,image_url,bio from user where type = 0 limit 3`;
  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};
module.exports = {
  get_teacher_list,
  get_teacher_by_id,
  get_teacher_info_request_form,
  get_top_teacher,
  update_teacher_info,
};
