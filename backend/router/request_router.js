const express= require('express');
const {
  addReqeustFromStudent,
  getRequestsByStudentId,
  get_request_by_id,
  getRequestsByTeacherId,
  get_request_by_id_with_student,
  handle_submit_slot,
  get_request_for_teacher_calender,
  get_request_for_student_calender,
  handle_request_state_change,
} = require("../controller/request_controller");
const { check_authentication } = require("../middleware/authentication");
const multer = require("multer");
const shortid = require("shortid");
const path = require("path");

const Router = express.Router();

Router.post("/", check_authentication, addReqeustFromStudent);
Router.get("/", check_authentication, getRequestsByStudentId);
Router.get("/teacher", check_authentication, getRequestsByTeacherId);
Router.get(
  "/teacher/calender",
  check_authentication,
  get_request_for_teacher_calender
);
Router.get(
  "/student/calender",
  check_authentication,
  get_request_for_student_calender
);

Router.get(
  "/student/:id",
  check_authentication,
  get_request_by_id_with_student
);
Router.put("/state_change", check_authentication, handle_request_state_change);

Router.put("/:request_id", check_authentication, handle_submit_slot);

Router.get('/:id', check_authentication, get_request_by_id)




module.exports=Router;
