const express = require("express");
const { check_authentication } = require("../middleware/authentication");
const {
  add_comments,
  get_comments_for_post,
} = require("../controller/comment_controller");

const Router = express.Router();

Router.post("/", check_authentication, add_comments);
Router.get("/:post_id", check_authentication, get_comments_for_post);

module.exports = Router;
