const express=require('express')
const {
  get_teacher_list,
  get_teacher_by_id,
  get_teacher_info_request_form,
  get_top_teacher,
  update_teacher_info,
} = require("../controller/teacher_controller");
const { signin, signup } = require("../controller/auth");
const multer = require("multer");
const shortid = require("shortid");
const path = require("path");
const { check_authentication } = require("../middleware/authentication");

const Router = express.Router();

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(path.dirname(__dirname), "uploads"));
  },
  filename: function (req, file, cb) {
    cb(null, shortid.generate() + "-" + file.originalname);
  },
});

const upload = multer({ storage: storage });

Router.post("/signup", upload.single("images"), signup);

Router.post("/signin", signin);

Router.get("/", check_authentication, get_teacher_list);
Router.get("/top3", check_authentication, get_top_teacher);
Router.put("/", check_authentication, update_teacher_info);
Router.get(
  "/request_form/:teacher_id",
  check_authentication,
  get_teacher_info_request_form
);
Router.get("/:teacher_id", check_authentication, get_teacher_by_id);



module.exports=Router