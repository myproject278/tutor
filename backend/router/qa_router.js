const express = require("express");
const { check_authentication } = require("../middleware/authentication");
const {
  add_question,
  add_answer,
  get_questions,
  get_answer_by_question_id,
  get_questions_by_tags_search,
} = require("../controller/qa_controller");

const Router = express.Router();

Router.post("/question", check_authentication, add_question);
Router.post("/answer", check_authentication, add_answer);
Router.get("/questions", check_authentication, get_questions);
Router.get(
  "/answer/:question_id",
  check_authentication,
  get_answer_by_question_id
);
Router.get("/search", check_authentication, get_questions_by_tags_search);


module.exports = Router;
