const express = require("express");
const { check_authentication } = require("../middleware/authentication");
const {
  get_question_by_subject_id,
  add_quiz_result,
  get_quiz_result_by_id,
  get_quiz_history_result,
  get_quiz_score_by_subject,
} = require("../controller/quiz_controller");

const Router = express.Router();

Router.get("/result/:quiz_id", check_authentication, get_quiz_result_by_id);
Router.get("/history", check_authentication, get_quiz_history_result);
Router.get("/quiz_scores", check_authentication, get_quiz_score_by_subject);

Router.get("/:subject_id", check_authentication, get_question_by_subject_id);
Router.post("/", check_authentication, add_quiz_result);

module.exports = Router;
