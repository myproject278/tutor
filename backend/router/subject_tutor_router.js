const express= require('express')
const multer=require('multer')
const shortid=require('shortid')
const path=require('path')
const {
  get_teacher_info_by_subject,
  get_top_teachers,
  get_subject_teacher_by_id,
  add_subject_tutor,
  get_subject_list_for_teacher,
  get_teacher_side_subject_list,
  get_subject_info_for_update,
  update_subject_tutor,
} = require("../controller/subject_tutor_controller");
const { check_authentication } = require("../middleware/authentication");

const Router = express.Router();

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(path.dirname(__dirname), "uploads"));
  },
  filename: function (req, file, cb) {
    cb(null, shortid.generate() + "-" + file.originalname);
  },
});
const upload = multer({ storage: storage });

Router.get("/top_teacher", get_top_teachers);
Router.put("/", check_authentication, update_subject_tutor);

Router.get(
  "/update/:subject_id",
  check_authentication,
  get_subject_info_for_update
);
Router.get(
  "/teacher_list/:teacher_id",
  check_authentication,
  get_subject_list_for_teacher
);
Router.get(
  "/subject_list",
  check_authentication,
  get_teacher_side_subject_list
);

Router.get(
  "/:subject_id/:sorting_column/:page_id",
  get_teacher_info_by_subject
);
Router.post("/", check_authentication, add_subject_tutor);
Router.get('/:subject_id/:teacher_id', get_subject_teacher_by_id);






module.exports=Router;
