const express= require('express')
const { check_authentication } = require('../middleware/authentication')
const { get_user_address } = require('../controller/address_controller')

const Router = express.Router()




Router.get('/user/:user_id',check_authentication,get_user_address)





module.exports=Router;
