const express= require('express')
const multer=require('multer')
const shortid=require('shortid')
const path=require('path')
const {
  add_subject_info,
  get_subject_info_by_parentId,
  get_parent_subjects,
  get_top_subjects,
  get_subject_for_teacher_dropdown,
} = require("../controller/subject_info_controller");
const { check_authentication } = require('../middleware/authentication')


const Router=express.Router()

const storage=multer.diskStorage({
    destination:function(req,file,cb){
        cb(null,path.join(path.dirname(__dirname),"uploads"));
    },
    filename:function(req,file,cb){
        cb(null,shortid.generate()+"-"+file.originalname)
    }
});

const upload=multer({storage:storage})


Router.get("/", check_authentication, get_subject_info_by_parentId);
Router.get("/top4", check_authentication, get_top_subjects);

Router.post('/',upload.single("images"),add_subject_info)
Router.get("/parent", check_authentication, get_parent_subjects);
Router.get("/dropdown", check_authentication, get_subject_for_teacher_dropdown);


Router.get('/:service_id',check_authentication,get_subject_info_by_parentId)


module.exports=Router;
