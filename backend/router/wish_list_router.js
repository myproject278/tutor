const express= require('express')
const {get_wishlist_by_student_id, add_wishlist, remove_wishlist} = require('../controller/wishlist_controller')
const { check_authentication } = require('../middleware/authentication')

const Router = express.Router()




Router.post('/',check_authentication,add_wishlist)
Router.delete('/:id',check_authentication,remove_wishlist)
Router.get('/',check_authentication,get_wishlist_by_student_id);




module.exports=Router;
