const express = require("express");
const multer = require("multer");
const shortid = require("shortid");
const path = require("path");

const { check_authentication } = require("../middleware/authentication");
const {
  add_post,
  get_post,
  get_post_by_id,
  get_recent_and_famous_posts,
  get_post_by_tags_search,
  get_posts_for_teacher,
  get_post_for_update,
  update_post,
} = require("../controller/post_controller");

const Router = express.Router();

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(path.dirname(__dirname), "uploads"));
  },
  filename: function (req, file, cb) {
    cb(null, shortid.generate() + "-" + file.originalname);
  },
});

const upload = multer({ storage: storage });

Router.post("/", check_authentication, upload.array("images"), add_post);
Router.get("/", check_authentication, get_post);
Router.get("/recent", check_authentication, get_recent_and_famous_posts);
Router.get("/search", check_authentication, get_post_by_tags_search);
Router.get("/teacher", check_authentication, get_posts_for_teacher);
Router.get("/update/:post_id", check_authentication, get_post_for_update);
Router.put("/", check_authentication, update_post);
Router.get("/:post_id", check_authentication, get_post_by_id);

module.exports = Router;
