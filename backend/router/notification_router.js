const express = require("express");
const { check_authentication } = require("../middleware/authentication");
const {
  add_notification,
  get_notifications,
  handle_notification_read,
  get_unread_msg_count,
} = require("../controller/notification_controller");

const Router = express.Router();

Router.post("/:type", check_authentication, add_notification);
Router.put("/", check_authentication, handle_notification_read);
Router.get("/count/:type", check_authentication, get_unread_msg_count);

Router.get("/:type", check_authentication, get_notifications);
module.exports = Router;
