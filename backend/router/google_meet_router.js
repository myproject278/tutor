const express = require("express");
const { check_authentication } = require("../middleware/authentication");
const {
  scheduled_meeting,
  s2,
} = require("../controller/google_meet_controller");

const Router = express.Router();

Router.post("/request", scheduled_meeting);
Router.get("/", s2);
module.exports = Router;
