const express= require('express')
const multer=require('multer')
const shortid=require('shortid')
const path=require('path')

const { check_authentication } = require('../middleware/authentication')
const { add_review, get_review, get_review_for_teacher } = require('../controller/review_controller')


const Router=express.Router()

const storage=multer.diskStorage({
    destination:function(req,file,cb){
        cb(null,path.join(path.dirname(__dirname),"uploads"));
    },
    filename:function(req,file,cb){
        cb(null,shortid.generate()+"-"+file.originalname)
    }
});

const upload=multer({storage:storage})

Router.post('/', check_authentication,upload.array("images"),add_review);
Router.get('/single/:tutor_subject_id',check_authentication,get_review)
Router.get('/:subject_id/:teacher_id', check_authentication, get_review_for_teacher)



module.exports=Router;

