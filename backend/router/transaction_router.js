const express= require('express');
const { check_authentication } = require('../middleware/authentication')
const {add_transaction_for_student,
    get_count_transcations_for_student,
    get_transcations_for_student, get_student_transactions_amt_for_dates,
    get_total_paid_amount_student
} = require('../controller/transaction_controller')
const Router = express.Router()



Router.get('/user/count',check_authentication,get_count_transcations_for_student)
Router.post('/',check_authentication,add_transaction_for_student)
Router.get('/user/date/:start_date/:end_date',check_authentication,get_student_transactions_amt_for_dates)
Router.get('/user/total_paid',check_authentication,get_total_paid_amount_student)
Router.get('/user/:page/:rowsPerPage',check_authentication,get_count_transcations_for_student)



module.exports=Router;
