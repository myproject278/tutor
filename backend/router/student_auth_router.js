const express=require('express')
const {
  signup,
  signin,
  confirm_user_email,
  get_user_info,
} = require("../controller/auth");
const multer = require("multer");
const shortid = require("shortid");
const path = require("path");
const { check_authentication } = require("../middleware/authentication");
const { send_sign_up_mail } = require("../controller/mailer_controller");

const Router = express.Router();

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(path.dirname(__dirname), "uploads"));
  },
  filename: function (req, file, cb) {
    cb(null, shortid.generate() + "-" + file.originalname);
  },
});

const upload = multer({ storage: storage });

Router.post("/signup", upload.single("images"), signup, send_sign_up_mail);

Router.post("/signin", signin);
Router.get("/confirm/:token", confirm_user_email);

Router.get("/info", check_authentication, get_user_info);



module.exports=Router