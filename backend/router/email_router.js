const express = require("express");
const { check_authentication } = require("../middleware/authentication");

const { send_mail } = require("../controller/mailer_controller");

const Router = express.Router();

Router.post("/", check_authentication, send_mail);
module.exports = Router;
