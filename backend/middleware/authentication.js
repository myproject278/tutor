const  jwt= require('jsonwebtoken')
const { getResult ,secret}= require('../utils/util');
 const check_authentication=(req,res,next)=>{
  try{
    const token=req.headers?.token ?? false;
    if(!token)
    res.status(401).json(getResult('Sign in plzz !!!'))
    else{
      const user = jwt.verify(token, secret);
      if (user.authenticated == 0)
        res.status(401).json(getResult("Please confirm email !!!"));
      else {
        req.user = user;
        next();
      }
    }
  }catch(err){
      res.status(401).json(getResult(err))
  }
}

module.exports ={
    check_authentication
}

