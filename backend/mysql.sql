-- User  -           [ id, name , mobile_no , email , password , type (User, owner),image  ]
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `mobile_no` char(12) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(1000) DEFAULT NULL,
  `type` tinyint DEFAULT NULL,
  `image_url` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) 

-- Services -         [ id , service_name  ,  image_url , parent_id ]
CREATE TABLE `services` (
  `id` int NOT NULL AUTO_INCREMENT,
  `service_name` varchar(1000) DEFAULT NULL,
  `image_url` varchar(1000) DEFAULT NULL,
  `parent_id` int DEFAULT NULL,
  `info` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`id`)
)


-- products and their rates [ id , vehicle info , images_urls , location , owner_id, service_id ]
 CREATE TABLE `products` (
  `id` int NOT NULL AUTO_INCREMENT,
  `product_name` varchar(50) DEFAULT NULL,
  `info` varchar(2000) DEFAULT NULL,
  `unit` varchar(20) DEFAULT NULL,
  `regular_rate` double DEFAULT NULL,
  `extra_rate` double DEFAULT NULL,
  `extra_rate_unit` varchar(20) DEFAULT NULL,
  `image_urls` varchar(1000) DEFAULT NULL,
  `location` varchar(1000) DEFAULT NULL,
  `owner_id` int DEFAULT NULL,
  `service_id` int DEFAULT NULL,
  `avg_rating` double DEFAULT NULL,
  `total_reviews` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) 

-- request_and_approval - [ id , vehicle_id , start_time , end_time , approval_status , user_id ]
 CREATE TABLE `requests` (
  `id` int NOT NULL AUTO_INCREMENT,
  `product_id` int DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `approval_status` int NOT NULL DEFAULT '0',
  `user_id` int DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `unit_of_work` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) 

-- Transactional_table -   [ user_id , vehicle_id , service_id , owner_id , total_amount, paid_amount , date ] 
CREATE TABLE `transactions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `owner_id` int DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `date` datetime DEFAULT CURRENT_TIMESTAMP,
  `description` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`id`)
)

-- users_owner_due=  [ id , user_id, owner_id , remaining amount  ] 
CREATE TABLE `user_owner_due` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `owner_id` int DEFAULT NULL,
  `remaining_amount` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_user_owner` (`user_id`,`owner_id`)
)



-- wishlists 
CREATE TABLE `wish_lists` (
  `user_id` int NOT NULL,
  `product_id` int NOT NULL,
  PRIMARY KEY (`user_id`,`product_id`)
)

-- review [ product_id , user_id , rating , title , desc , date, images_url]
CREATE TABLE reviews (
  product_id INT,
  user_id INT,
  rating INT,
  title VARCHAR(500),
  description VARCHAR(2000),
  date DATETIME DEFAULT CURRENT_TIMESTAMP,
  image_urls VARCHAR(1000),
  PRIMARY KEY (product_id, user_id)
);



-- address table 
CREATE TABLE `address` (
  `id` int NOT NULL AUTO_INCREMENT,
  `city` varchar(100) DEFAULT NULL,
  `pincode` char(10) DEFAULT NULL,
  `home_no` varchar(20) DEFAULT NULL,
  `road` varchar(100) DEFAULT NULL,
  `extra` varchar(1000) DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) 

-- trigger for changeing on transactions 
DELIMITER //
CREATE TRIGGER update_user_owner_due
AFTER INSERT ON transactions
FOR EACH ROW
BEGIN
    INSERT INTO user_owner_due (user_id, owner_id, remaining_amount)
    VALUES (NEW.user_id, NEW.owner_id, NEW.amount)
    ON DUPLICATE KEY UPDATE remaining_amount = remaining_amount + NEW.amount;
END //
DELIMITER ;





-- trigger for rating 


DROP TRIGGER IF EXISTS update_product_reviews;
DELIMITER //



CREATE TRIGGER update_product_reviews
AFTER INSERT ON reviews
FOR EACH ROW
BEGIN
  UPDATE products
SET total_reviews = total_reviews + 1,
    avg_rating = (
      (avg_rating * total_reviews + NEW.rating) / (total_reviews)
    )
WHERE id = NEW.product_id;

END //

DELIMITER ;
