const { google } = require("googleapis");

// Initialize the Google Calendar API client with your credentials
const calendar = google.calendar("v3");

// You need to authenticate your application before using the Google Calendar API.
// You can refer to Google's documentation on how to authenticate your app.

// Define the email addresses of attendees
const fun = async () => {
  const attendeesEmails = [
    { email: "karankadam540@example.com" },
    { email: "karanpatil951999@example.com" },
  ];

  // Define the event details
  const event = {
    summary: "Coding class",
    location: "Virtual / Google Meet",
    description: "Learn how to code with Javascript",
    start: {
      dateTime: "2023-09-10T09:00:00-07:00",
      timeZone: "America/Los_Angeles",
    },
    end: {
      dateTime: "2023-09-10T09:30:00-07:00",
      timeZone: "America/Los_Angeles",
    },
    attendees: attendeesEmails,
    reminders: {
      useDefault: false,
      overrides: [
        { method: "email", minutes: 24 * 60 },
        { method: "popup", minutes: 10 },
      ],
    },
    conferenceData: {
      createRequest: {
        conferenceSolutionKey: {
          type: "hangoutsMeet",
        },
        requestId: "coding-calendar-demo",
      },
    },
  };

  // Insert the event into the user's primary calendar
  const response = await calendar.events.insert({
    calendarId: "primary", // You can change 'primary' to the desired calendar ID
    resource: event,
    conferenceDataVersion: 1,
  });

  // Extract information about the created event
  const {
    config: {
      data: { summary, location, start, end, attendees },
    },
    data: { conferenceData },
  } = response;

  // Get the Google Meet conference URL in order to join the call
  const { uri } = conferenceData.entryPoints[0];

  // Print event details and conference URL
  console.log(
    `📅 Calendar event created: ${summary} at ${location}, from ${
      start.dateTime
    } to ${end.dateTime}, attendees:\n${attendees
      .map((person) => `🧍 ${person.email}`)
      .join("\n")} \n 💻 Join conference call link: ${uri}`
  );
};
fun();
