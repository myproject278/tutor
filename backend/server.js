const express = require("express");
const cors = require("cors");
const path=require('path')
const { Server } = require("socket.io"); // Add this
const http = require("http");

const subjectRouter = require("./router/subject_info_router");
const subjectTutorRouter = require("./router/subject_tutor_router");
const wishListRouter = require("./router/wish_list_router");
const studentRouter = require("./router/student_auth_router");
const requestRouter = require("./router/request_router");
const transactionRouter = require("./router/transaction_router");
const reviewRouter = require("./router/review_router");
const addressRouter = require("./router/address_router");
const teacherRouter = require("./router/teacher_auth_router");
const PostRouter = require("./router/post_router");
const questionRouter = require("./router/qa_router");
const commentRouter = require("./router/comment_router");
const googleRouter = require("./router/google_meet_router");
const mailerRouter = require("./router/email_router");
const notificationRouter = require("./router/notification_router");
const quizRouter = require("./router/quiz_router");
const sendMail = require("./utils/mailer");
const chatRouter = require("./chat_application/router/chat_router");
const messageRouter = require("./chat_application/router/message_router");
const groupRouter = require("./chat_application/router/group_router");
const {
  set_user_online,
  set_user_offline,
  get_socket_id,
} = require("./chat_application/controller.js/user_controller");
const {
  add_message,
  read_message_by_user,
} = require("./chat_application/controller.js/message_controller");
const {
  creat_user_group,
} = require("./chat_application/controller.js/group_controller");

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/public", express.static(path.join(__dirname, "uploads")));

const server = http.createServer(app);
const io = new Server(server, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"],
  },
});

app.use("/subject", subjectRouter);
app.use("/teacher", teacherRouter);
app.use("/subject/teacher", subjectTutorRouter);
app.use("/wishlist", wishListRouter);
app.use("/student", studentRouter);
app.use("/request", requestRouter);
app.use("/transaction", transactionRouter);
app.use("/review", reviewRouter);
app.use("/address", addressRouter);
app.use("/posts", PostRouter);
app.use("/qa", questionRouter);
app.use("/comments", commentRouter);
app.use("/google_meet", googleRouter);
app.use("/send_email", mailerRouter);
app.use("/notifications", notificationRouter);
app.use("/quiz", quizRouter);
app.use("/chat", chatRouter);
app.use("/messages", messageRouter);
app.use("/group", groupRouter);

const port = process.env.PORT || 3001;

server.listen(port, () => {
  console.log(`App running on port ${port} ...`);
});

io.on("connection", async (socket) => {
  socket.on("setUp", (user_id) => {
    set_user_online(user_id, socket.id);
    socket.emit("connected");
  });

  // We can write our socket event listeners in here...
  socket.on("friend_request", async (data) => {
    const to = await User.findById(data.to).select("socket_id");
    const from = await User.findById(data.from).select("socket_id");

    // create a friend request
    await FriendRequest.create({
      sender: data.from,
      recipient: data.to,
    });
    // emit event request received to recipient
    io.to(to?.socket_id).emit("new_friend_request", {
      message: "New friend request received",
    });
    io.to(from?.socket_id).emit("request_sent", {
      message: "Request Sent successfully!",
    });
  });

  socket.on("accept_request", async (data) => {
    // accept friend request => add ref of each other in friends array
    console.log(data);
    const request_doc = await FriendRequest.findById(data.request_id);

    console.log(request_doc);

    const sender = await User.findById(request_doc.sender);
    const receiver = await User.findById(request_doc.recipient);

    sender.friends.push(request_doc.recipient);
    receiver.friends.push(request_doc.sender);

    await receiver.save({ new: true, validateModifiedOnly: true });
    await sender.save({ new: true, validateModifiedOnly: true });

    await FriendRequest.findByIdAndDelete(data.request_id);

    // delete this request doc
    // emit event to both of them

    // emit event request accepted to both
    io.to(sender?.socket_id).emit("request_accepted", {
      message: "Friend Request Accepted",
    });
    io.to(receiver?.socket_id).emit("request_accepted", {
      message: "Friend Request Accepted",
    });
  });

  socket.on("get_direct_conversations", async ({ user_id }, callback) => {
    const existing_conversations = await OneToOneMessage.find({
      participants: { $all: [user_id] },
    }).populate("participants", "firstName lastName avatar _id email status");

    // db.books.find({ authors: { $elemMatch: { name: "John Smith" } } })

    console.log(existing_conversations);

    callback(existing_conversations);
  });

  // socket.on("start_conversation", async (data) => {
  //   // data: {to: from:}

  //   const { to, from } = data;

  //   console.log(existing_conversations[0], "Existing Conversation");

  //   // if no => create a new OneToOneMessage doc & emit event "start_chat" & send conversation details as payload
  //   if (existing_conversations.length === 0) {
  //     let new_chat = await OneToOneMessage.create({
  //       participants: [to, from],
  //     });

  //     new_chat = await OneToOneMessage.findById(new_chat).populate(
  //       "participants",
  //       "firstName lastName _id email status"
  //     );

  //     console.log(new_chat);

  //     socket.emit("start_chat", new_chat);
  //   }
  //   // if yes => just emit event "start_chat" & send conversation details as payload
  //   else {
  //     socket.emit("start_chat", existing_conversations[0]);
  //   }
  // });

  socket.on("get_messages", async (data, callback) => {
    try {
      const { messages } = await OneToOneMessage.findById(
        data.conversation_id
      ).select("messages");
      callback(messages);
    } catch (error) {
      console.log(error);
    }
  });

  // Handle incoming text/link messages
  socket.on("text_message", async (data) => {
    console.log("Received message:", data);

    // data: {to, from, text}

    const message = await add_message(data);
    const sender_socket_id = await get_socket_id(data?.from);
    const receiver_socket_id = await get_socket_id(data?.to);
    // emit incoming_message -> to user
    console.log(sender_socket_id);
    if (sender_socket_id) {
      io.to(sender_socket_id).emit("send_message_success", {
        message: message,
      });
    }

    // emit outgoing_message -> from user
    if (receiver_socket_id) {
      io.to(receiver_socket_id).emit("new_message", {
        message: message,
      });
    }
  });

  socket.on("isTyping", async ({ name, receiver_id, sender_id }) => {
    const receiver_socket_id = await get_socket_id(receiver_id);

    if (receiver_socket_id)
      io.to(receiver_socket_id).emit("userTyping", { name, sender_id });
  });

  socket.on("stopTyping", async ({ receiver_id, sender_id }) => {
    const receiver_socket_id = await get_socket_id(receiver_id);
    if (receiver_socket_id)
      io.to(receiver_socket_id).emit("userStopTyping", { sender_id });
  });

  socket.on("readMsg", async ({ sender_id, receiver_id }) => {
    const receiver_socket_id = await get_socket_id(receiver_id);
    read_message_by_user(sender_id, receiver_id);
    io.to(receiver_socket_id).emit("read_msg_by_receiver", sender_id);
  });

  // handle Media/Document Message
  socket.on("file_message", (data) => {
    console.log("Received message:", data);

    // data: {to, from, text, file}

    // Get the file extension
    const fileExtension = path.extname(data.file.name);

    // Generate a unique filename
    const filename = `${Date.now()}_${Math.floor(
      Math.random() * 10000
    )}${fileExtension}`;

    // upload file to AWS s3

    // create a new conversation if its dosent exists yet or add a new message to existing conversation

    // save to db

    // emit incoming_message -> to user

    // emit outgoing_message -> from user
  });

  // ----------------- handle group ------------------------

  socket.on("create_user_group", async (form) => {
    //const group_info = await creat_user_group(form);
    console.log(form.get("group_name"));
  });

  // -------------- HANDLE AUDIO CALL SOCKET EVENTS ----------------- //

  // handle start_audio_call event
  socket.on("start_audio_call", async (data) => {
    const { from, to, roomID } = data;

    const to_user = await User.findById(to);
    const from_user = await User.findById(from);

    console.log("to_user", to_user);

    // send notification to receiver of call
    io.to(to_user?.socket_id).emit("audio_call_notification", {
      from: from_user,
      roomID,
      streamID: from,
      userID: to,
      userName: to,
    });
  });

  // handle audio_call_not_picked
  socket.on("audio_call_not_picked", async (data) => {
    console.log(data);
    // find and update call record
    const { to, from } = data;

    const to_user = await User.findById(to);

    await AudioCall.findOneAndUpdate(
      {
        participants: { $size: 2, $all: [to, from] },
      },
      { verdict: "Missed", status: "Ended", endedAt: Date.now() }
    );

    // TODO => emit call_missed to receiver of call
    io.to(to_user?.socket_id).emit("audio_call_missed", {
      from,
      to,
    });
  });

  // handle audio_call_accepted
  socket.on("audio_call_accepted", async (data) => {
    const { to, from } = data;

    const from_user = await User.findById(from);

    // find and update call record
    await AudioCall.findOneAndUpdate(
      {
        participants: { $size: 2, $all: [to, from] },
      },
      { verdict: "Accepted" }
    );

    // TODO => emit call_accepted to sender of call
    io.to(from_user?.socket_id).emit("audio_call_accepted", {
      from,
      to,
    });
  });

  // handle audio_call_denied
  socket.on("audio_call_denied", async (data) => {
    // find and update call record
    const { to, from } = data;

    await AudioCall.findOneAndUpdate(
      {
        participants: { $size: 2, $all: [to, from] },
      },
      { verdict: "Denied", status: "Ended", endedAt: Date.now() }
    );

    const from_user = await User.findById(from);
    // TODO => emit call_denied to sender of call

    io.to(from_user?.socket_id).emit("audio_call_denied", {
      from,
      to,
    });
  });

  // handle user_is_busy_audio_call
  socket.on("user_is_busy_audio_call", async (data) => {
    const { to, from } = data;
    // find and update call record
    await AudioCall.findOneAndUpdate(
      {
        participants: { $size: 2, $all: [to, from] },
      },
      { verdict: "Busy", status: "Ended", endedAt: Date.now() }
    );

    const from_user = await User.findById(from);
    // TODO => emit on_another_audio_call to sender of call
    io.to(from_user?.socket_id).emit("on_another_audio_call", {
      from,
      to,
    });
  });

  // --------------------- HANDLE VIDEO CALL SOCKET EVENTS ---------------------- //

  // handle start_video_call event
  socket.on("start_video_call", async (data) => {
    const { from, to, roomID } = data;

    console.log(data);

    const to_user = await User.findById(to);
    const from_user = await User.findById(from);

    console.log("to_user", to_user);

    // send notification to receiver of call
    io.to(to_user?.socket_id).emit("video_call_notification", {
      from: from_user,
      roomID,
      streamID: from,
      userID: to,
      userName: to,
    });
  });

  // handle video_call_not_picked
  socket.on("video_call_not_picked", async (data) => {
    console.log(data);
    // find and update call record
    const { to, from } = data;

    const to_user = await User.findById(to);

    await VideoCall.findOneAndUpdate(
      {
        participants: { $size: 2, $all: [to, from] },
      },
      { verdict: "Missed", status: "Ended", endedAt: Date.now() }
    );

    // TODO => emit call_missed to receiver of call
    io.to(to_user?.socket_id).emit("video_call_missed", {
      from,
      to,
    });
  });

  // handle video_call_accepted
  socket.on("video_call_accepted", async (data) => {
    const { to, from } = data;

    const from_user = await User.findById(from);

    // find and update call record
    await VideoCall.findOneAndUpdate(
      {
        participants: { $size: 2, $all: [to, from] },
      },
      { verdict: "Accepted" }
    );

    // TODO => emit call_accepted to sender of call
    io.to(from_user?.socket_id).emit("video_call_accepted", {
      from,
      to,
    });
  });

  // handle video_call_denied
  socket.on("video_call_denied", async (data) => {
    // find and update call record
    const { to, from } = data;

    await VideoCall.findOneAndUpdate(
      {
        participants: { $size: 2, $all: [to, from] },
      },
      { verdict: "Denied", status: "Ended", endedAt: Date.now() }
    );

    const from_user = await User.findById(from);
    // TODO => emit call_denied to sender of call

    io.to(from_user?.socket_id).emit("video_call_denied", {
      from,
      to,
    });
  });

  // handle user_is_busy_video_call
  socket.on("user_is_busy_video_call", async (data) => {
    const { to, from } = data;
    // find and update call record
    await VideoCall.findOneAndUpdate(
      {
        participants: { $size: 2, $all: [to, from] },
      },
      { verdict: "Busy", status: "Ended", endedAt: Date.now() }
    );

    const from_user = await User.findById(from);
    // TODO => emit on_another_video_call to sender of call
    io.to(from_user?.socket_id).emit("on_another_video_call", {
      from,
      to,
    });
  });

  // -------------- HANDLE SOCKET DISCONNECTION ----------------- //

  socket.on("end", async (data) => {
    // Find user by ID and set status as offline

    if (data.user_id) {
      await set_user_offline(data.user_id);
    }

    // broadcast to all conversation rooms of this user that this user is offline (disconnected)

    console.log("closing connection");
    socket.disconnect(0);
  });
});