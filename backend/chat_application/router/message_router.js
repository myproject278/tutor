const express = require("express");
const { check_authentication } = require("../../middleware/authentication");
const {
  get_message_by_user_id,
} = require("../controller.js/message_controller");

const Router = express.Router();

Router.get(
  "/single_user/:other_user_id",
  check_authentication,
  get_message_by_user_id
);
module.exports = Router;
