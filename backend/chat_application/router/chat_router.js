const express = require("express");
const { check_authentication } = require("../../middleware/authentication");
const {
  get_friends,
  get_user_conversation,
} = require("../controller.js/chat_controller");


const Router = express.Router();

Router.get("/friends", check_authentication, get_friends);
Router.get("/conversations", check_authentication, get_user_conversation);

module.exports = Router;
