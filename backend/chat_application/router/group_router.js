const express = require("express");
const multer = require("multer");
const shortid = require("shortid");
const path = require("path");

const { creat_user_group } = require("../controller.js/group_controller");
const { check_authentication } = require("../../middleware/authentication");

const Router = express.Router();

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(path.dirname(__dirname), "../uploads"));
  },
  filename: function (req, file, cb) {
    cb(null, shortid.generate() + "-" + file.originalname);
  },
});

const upload = multer({ storage: storage });

Router.post(
  "/",
  upload.single("image"),
  check_authentication,
  creat_user_group
);

module.exports = Router;
