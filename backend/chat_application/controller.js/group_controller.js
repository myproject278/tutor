const db = require("../../db/dbConnection");
const util = require("util");
const { getResult } = require("../../utils/util");

const queryAsync = util.promisify(db.query).bind(db);

const creat_user_group = async (req, res) => {
  try {
    const { group_name, group_description } = req.body;
    const user_id = req.user.user_id;
    const insertStatement = `
    insert into tutor_groups (group_name, group_description, image_url, created_by)
    VALUES (?, ?, ?, ?)
  `;
    const image_url = req.file.filename;
    const insertData = [group_name, group_description, image_url, user_id];
    const insertResult = await queryAsync(insertStatement, insertData);

    const selectStatement = "select * from tutor_groups where group_id = ?";
    const [group_result] = await queryAsync(selectStatement, [
      insertResult.insertId,
    ]);
    res.send(getResult(false, group_result));
  } catch (err) {
    res.send(getResult(err));
  }
};

module.exports = {
  creat_user_group,
};
