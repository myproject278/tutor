const db = require("../../db/dbConnection");
const util = require("util");

const queryAsync = util.promisify(db.query).bind(db);

const set_user_online = async (user_id, socket_id) => {
  const statement = `update user set online = 1,socket_id= '${socket_id}' where id = ${user_id}`;
  db.query(statement, (error, data) => {});
};

const set_user_offline = async (user_id) => {
  const statement = `update user set online = 0 , socket_id= NULL where id = ${user_id}`;
  db.query(statement, (error, data) => {});
};

const get_socket_id = async (user_id) => {
  try {
    const statement = `select socket_id from user where id = ?`;
    const result = await queryAsync(statement, [user_id]);
    if (result.length > 0) return result[0]?.socket_id;
  } catch (error) {}
};

module.exports = {
  set_user_offline,
  set_user_online,
  get_socket_id,
};
