const db = require("../../db/dbConnection");
const util = require("util");
const { getResult } = require("../../utils/util");

// Promisify the query function
const queryAsync = util.promisify(db.query).bind(db);

const add_message = async ({ message, from, to, type, parent_id }) => {
  try {
    const insertStatement = `
      INSERT INTO messages (sender_user_id, receiver_user_id, content, type, parent_id)
      VALUES (?, ?, ?, ?, ?)
    `;

    const insertData = [from, to, message, type, parent_id];
    const insertResult = await queryAsync(insertStatement, insertData);

    const selectStatement =
      "SELECT M1.*, M2.content AS parent_message,M2.type as parent_type FROM messages  M1 LEFT JOIN messages AS M2 ON M1.parent_id = M2.message_id WHERE M1.message_id = ?";
    const [selectedMessage] = await queryAsync(selectStatement, [
      insertResult.insertId,
    ]);

    const updateStatement = `
    UPDATE friendships 
    SET  last_message_id = ? , unread_count = unread_count + 1
    WHERE (user1_id = ? AND user2_id = ?) OR (user1_id = ? AND user2_id = ?)
  `;

    await queryAsync(updateStatement, [
      insertResult.insertId,
      to,
      from,
      from,
      to,
    ]);

    return selectedMessage;
  } catch (error) {
    throw error;
  }
};

const get_message_by_user_id = (req, res) => {
  const user_id = req.user.user_id;
  const { other_user_id } = req.params;
  const statement = `SELECT M1.*, M2.content AS parent_message,M2.type as parent_type
  FROM messages AS M1
  LEFT JOIN messages AS M2 ON M1.parent_id = M2.message_id
  WHERE (
    (M1.sender_user_id = ${user_id} AND M1.receiver_user_id = ${other_user_id}) OR
    (M1.sender_user_id = ${other_user_id} AND M1.receiver_user_id = ${user_id})
  )
  ORDER BY M1.message_id;
  `;
  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};

const read_message_by_user = async (user1_id, user2_id) => {
  const updateStatement = `
  UPDATE friendships 
  SET   unread_count = 0
  WHERE (user1_id = ? AND user2_id = ?) OR (user1_id = ? AND user2_id = ?)
`;

  await queryAsync(updateStatement, [user1_id, user2_id, user2_id, user1_id]);
};

module.exports = {
  add_message,
  get_message_by_user_id,
  read_message_by_user,
};
