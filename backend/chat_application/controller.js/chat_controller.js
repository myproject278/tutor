const db = require("../../db/dbConnection");
const { getResult } = require("../../utils/util");


const get_friends = (req, res) => {
  const user_id = req.user.user_id;
  const statement = `SELECT f.user1_id AS id,u.name,image_url img,online,u.email FROM friendships  f inner join user u on 
  f.user1_id = u.id  WHERE user2_id = ${user_id} AND f.status = 'accepted'
  UNION
  SELECT f.user2_id AS id ,u.name, image_url img,online,u.email FROM friendships f inner join user u on 
  f.user2_id = u.id WHERE user1_id = ${user_id} AND f.status = 'accepted';
  `;
  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};

const get_user_conversation = (req, res) => {
  const user_id = req.user.user_id;
  const statement = `SELECT f.user1_id AS id,u.name,image_url img,online,u.email,m.content,m.receiver_user_id,m.created_at,m.type,
  f.unread_count 
  FROM friendships  f inner join user u on 
  f.user1_id = u.id  inner join messages m on f.last_message_id = m.message_id  WHERE user2_id = ${user_id}
  UNION
  SELECT f.user2_id AS id ,u.name, image_url img,online,u.email,m.content,m.receiver_user_id,m.created_at,m.type ,f.unread_count 
   FROM friendships f inner join user u on 
  f.user2_id = u.id inner join messages m on f.last_message_id = m.message_id
   WHERE user1_id = ${user_id} order by created_at desc
  `;
  db.query(statement, (error, data) => {
    res.send(getResult(error, data));
  });
};



module.exports = {
  get_friends,
  get_user_conversation,
};
