import { FETCH_SUBJECTS } from '../constant'
import authAxios from '../helper/authAxios';


export const getSubjects=(subject_id=0)=>async dispatch=>{

    try{
        const { data } = await authAxios().get(`/subject/${subject_id}`)
     if(data.status=='success')
        dispatch({type:FETCH_SUBJECTS , data : data.data})
    }  catch(err){
     console.log(err)
    }

}
export const getTopSubjects = () => async (dispatch) => {
  try {
    const { data } = await authAxios().get(`/subject/top4`);
    if (data.status == "success")
      dispatch({ type: FETCH_SUBJECTS, data: data.data });
  } catch (err) {
    console.log(err);
  }
};

// export const getParentServiceName = ()=> async dispatch =>{
//     try{
//         const {data}=await authAxios().get(`/service/parent`)
//         if(data.status=='success')
//            dispatch({type:FETCH_PARENT_SERVICES , data : data.data})
//        }  catch(err){
//         console.log(err)
//        }
// }
