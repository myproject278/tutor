import { GET_ALL_POST_SUCCESS, GET_POST_BY_ID } from "../constant";
import authAxios from "../helper/authAxios";

export const get_single_post = (post_id) => async (dispatch) => {
  try {
    const { data } = await authAxios().get(`/posts/${post_id}`);
    if (data.status == "success" && data.data.length > 0)
      dispatch({ type: GET_POST_BY_ID, data: data.data[0] });
  } catch (err) {
    console.log(err);
  }
};

export const get_posts = () => async (dispatch) => {
  try {
    const { data } = await authAxios().get(`/posts`);
    console.log("data");
    console.log(data);
    if (data.status == "success")
      dispatch({ type: GET_ALL_POST_SUCCESS, data: data.data });
  } catch (err) {
    console.log(err);
  }
};
export const get_recent_and_famous_posts = () => async (dispatch) => {
  try {
    const { data } = await authAxios().get(`/posts/recent`);
    if (data.status == "success")
      dispatch({ type: GET_ALL_POST_SUCCESS, data: data.data });
  } catch (err) {
    console.log(err);
  }
};
export const get_posts_by_search_tage =
  ({ search = "", tags = "" }) =>
  async (dispatch) => {
    try {
      const { data } = await authAxios().get(
        `/posts/search?search=${search}&tags=${tags}`
      );

      if (data.status == "success")
        dispatch({ type: GET_ALL_POST_SUCCESS, data: data.data });
    } catch (err) {
      console.log(err);
    }
  };
