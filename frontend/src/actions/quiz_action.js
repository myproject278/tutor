import {
  GET_ALL_POST_SUCCESS,
  GET_ANSWER_FOR_QUESTION,
  GET_POST_BY_ID,
  GET_QUESTIONS,
  GET_QUIZ_HISTORY_SUCCESS,
  GET_QUIZ_QUESTION_SUCCESS,
  GET_QUIZ_RESULT_SUCCESS,
  GET_QUIZ_SCORE_SUCCESS,
} from "../constant";
import authAxios from "../helper/authAxios";
import store from "../store/store";

export const get_questions_for_quiz = (subject_id) => async (dispatch) => {
  try {
    const { data } = await authAxios().get(`/quiz/${subject_id}`);
    console.log(data);
    if (data.status == "success")
      dispatch({ type: GET_QUIZ_QUESTION_SUCCESS, data: data.data });
  } catch (err) {
    console.log(err);
  }
};

export const add_quiz_result = async (obj) => {
  try {
    const { data } = await authAxios().post(`/quiz/`, obj);
    console.log(data);
    return data;
  } catch (err) {
    console.log(err);
  }
};

export const get_quiz_result_by_id = (quiz_id) => async (dispatch) => {
  try {
    const { data } = await authAxios().get(`/quiz/result/${quiz_id}`);
    if (data.status == "success")
      dispatch({ type: GET_QUIZ_RESULT_SUCCESS, data: data.data });
  } catch (err) {
    console.log(err);
  }
};

export const get_quiz_history = () => async (dispatch) => {
  try {
    const { data } = await authAxios().get(`/quiz/history`);
    if (data.status == "success")
      dispatch({ type: GET_QUIZ_HISTORY_SUCCESS, data: data.data });
  } catch (err) {
    console.log(err);
  }
};

export const get_quiz_score_for_subjects = () => async (dispatch) => {
  try {
    const { data } = await authAxios().get(`/quiz/quiz_scores`);
    if (data.status == "success")
      dispatch({ type: GET_QUIZ_SCORE_SUCCESS, data: data.data });
  } catch (err) {
    console.log(err);
  }
};


