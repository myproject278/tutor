import axios from "axios";
import {
  AUTH_LOADING,
  AUTH_LOADING_COMPLETE,
  GET_USER_INFO_FAILURE,
  GET_USER_INFO_SUCCESFULL,
  LOGOUT,
  SIGN_IN_UP,
  SIGN_UP_IN_FAILURE,
} from "../constant";
import { BASE_URL } from "../constant";
import requestReducer from "../reducers/request_reducer";
import authAxios from "../helper/authAxios";

export const signUp = async (formData) => {
  try {
    const { data } = await axios.post(`${BASE_URL}/student/signup`, formData);
    return data;
  } catch (err) {
    return { status: "error", error: err };
  }
};

export const signIn = (user_data) => async (dispatch) => {
  try {
    const { data } = await axios.post(`${BASE_URL}/student/signin`, user_data);
    saveData(data, dispatch);
  } catch (err) {
    dispatch({
      type: SIGN_UP_IN_FAILURE,
      payload: {
        error: err,
      },
    });
  }
};
const saveData = (data, dispatch) => {
  if (data.status == "success") {
    const user = data.data;

    localStorage.setItem("student", JSON.stringify(user));
    localStorage.setItem("student_token", data.token);
    console.log(data);
    dispatch({
      type: SIGN_IN_UP,
      payload: {
        user,
        token: data.token,
      },
    });
  } else {
    console.log(data);
    dispatch({ type: SIGN_UP_IN_FAILURE, error: data.error });
  }
};

export const isUserLoggedIn = () => (dispatch) => {
  dispatch({ type: AUTH_LOADING });
  const token = localStorage.getItem("student_token");
  if (token) {
    const user = JSON.parse(localStorage.getItem("student"));
    dispatch({
      type: SIGN_IN_UP,
      payload: {
        user,
        token,
      },
    });
  }
  dispatch({ type: AUTH_LOADING_COMPLETE });
};

export const get_user_info = () => async (dispatch) => {
  try {
    const { data } = await authAxios().get(`${BASE_URL}/student/info`);
    if (data.status == "success")
      dispatch({ type: GET_USER_INFO_SUCCESFULL, data: data.data });
  } catch (err) {
    dispatch({
      type: GET_USER_INFO_FAILURE,
      error: err,
    });
  }
};


export const logoutUser = () => async dispatch => {
    console.log('logout')
        localStorage.clear();
        dispatch({type:LOGOUT});
}