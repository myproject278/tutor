import {
  CHANGE_REQUEST_STATUS,
  GET_REQUEST_BY_ID_SUCESS,
  GET_REQUEST_BY_PRODUCT_ID,
  GET_REQUEST_BY_USER_ID,
  GET_REQUEST_FOR_CALENDER,
} from "../constant";
import authAxios from "../helper/authAxios";
export const add_request = async (request) => {
  try {
    const { data } = await authAxios().post(`/request`, request);
    return data;
  } catch (err) {
    console.log(err);
    return false;
  }
};
export const handle_request_state_change = (obj) => async (dispatch) => {
  try {
    const { data } = await authAxios().put(`/request/state_change`, obj);
    if (data.status == "success")
      dispatch({ type: CHANGE_REQUEST_STATUS, status: obj.status });
  } catch (err) {
    console.log(err);
  }
};
export const get_request_by_student_id =
  (searchQuery, checkedStatus) => async (dispatch) => {
    try {
      const { data } = await authAxios().get(
        `/request?search=${searchQuery}&status=${checkedStatus}`
      );
      if (data.status == "success")
        dispatch({ type: GET_REQUEST_BY_USER_ID, data: data.data });
    } catch (err) {
      console.log(err);
    }
  };

export const get_request_by_id = (id) => async (dispatch) => {
  try {
    const { data } = await authAxios().get(`/request/${id}`);
    if ((data.status = "success"))
      dispatch({ type: GET_REQUEST_BY_ID_SUCESS, data: data.data });
  } catch (err) {
    console.log(err);
  }
};

export const get_request_by_product_id = (id) => async (dispatch) => {
  try {
    const { data } = await authAxios().get(`/request/product/${id}`);
    if ((data.status = "success"))
      dispatch({ type: GET_REQUEST_BY_PRODUCT_ID, data: data.data });
  } catch (err) {
    console.log(err);
  }
};

export const get_request_for_student_calender = () => async (dispatch) => {
  try {
    const { data } = await authAxios().get(`/request/student/calender`);
    if (data.status == "success") {
      dispatch({
        type: GET_REQUEST_FOR_CALENDER,
        data: data.data,
      });
    }
  } catch (err) {
    console.log(err);
  }
};
