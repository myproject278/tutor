import { GET_SUBJECTS_FOR_TEACHER, GET_SUBJECT_TEACHER_BY_ID, GET_TEACHERS_BY_SUBJECT} from "../constant";
import authAxios from "../helper/authAxios";
import store from "../store/store";
export const get_teachers_by_subject =
  (
    subject_id,
    sorting_column = "id",
    page_id,
    daysAvailability,
    expertiseLevel,
    searchText
  ) =>
  async (dispatch) => {
    try {
      const { data } = await authAxios().get(
        `/subject/teacher/${subject_id}/${sorting_column}/${page_id}?days_of_availability=${daysAvailability}&expertise_level=${expertiseLevel}&teacher_name=${searchText}`
      );
      if (data.status == "success")
        dispatch({ type: GET_TEACHERS_BY_SUBJECT, data: data.data });
    } catch (err) {
      console.log(err);
    }
  };

export const get_subject_teacher_details_by_id = ({subject_id, teacher_id }) => async dispatch => {
    try {
        
        const { data } = await authAxios().get(`/subject/teacher/${subject_id}/${teacher_id}`)
        console.log(data)
        if(data.status=='success')
           dispatch({type:GET_SUBJECT_TEACHER_BY_ID , data : data.data})
    }catch(err){
        console.log(err)
    }
}


export const get_subjects_for_teacher = ( teacher_id ) => async dispatch => {
    try {
        
        const { data } = await authAxios().get(`/subject/teacher/teacher_list/${teacher_id}`)
        console.log(data)
        if(data.status=='success')
           dispatch({type:GET_SUBJECTS_FOR_TEACHER , data : data.data})
    }catch(err){
        console.log(err)
    }
}
// export const add_product_to_recently_viewed = (product_id) =>async dispatch=> {
//     dispatch({type:ADD_PRODUCT_IN_RECENTLY_VIEWED, data:product_id})
// }

// export const get_recenltly_view_product = () => async dispatch=>{
//     const recently_viwed_products = localStorage.getItem('recently_viwed_products')
//     if (recently_viwed_products) {
//         dispatch({type:ADD_RECENTLY_VIEWD_LIST, data:JSON.parse(recently_viwed_products)})
//     }
// }

// export const get_recent_product = () => async dispatch => {
//     try {
//         const recently_viwed_products_id = store.getState().productReducer.recently_viwed_products_id
//         const { data } = await authAxios().post(`/product/recent`, { product_list: recently_viwed_products_id })
//         if(data.status=='success')
//             dispatch({ type: GET_RECENTLY_VIEWED_LIST_SUCCESSFULL, data: data.data })
//      } catch (e) {
//         console.log(e)
//       }
// }

// export const get_most_viwed_services = () => async dispatch => {
//     try {
//         const { data } = await authAxios().get(`/product/most_visited`)
//         if(data.status=='success')
//         dispatch({ type: GET_MOST_VIEWED_PRODUCT, data: data.data })
//      } catch (e) {
//         console.log(e)
//       }
// }


// export const get_product_count = () => async dispatch => {
//     try {
//         const { data } = await authAxios().get(`/product/count`)
//         if(data.status=='success')
//         dispatch({ type: GET_PRODUCT_COUNT, data: data.data })
//      } catch (e) {
//         console.log(e)
//       }
// }