import authAxios from "../helper/authAxios";

export const send_email = async (obj) => {
  try {
    console.log("in send email");
    const { data } = await authAxios().post(`/send_email`, obj);
    console.log(data);
  } catch (err) {
    console.log(err);
  }
};
