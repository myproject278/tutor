import { DELETE_PRODUCT_FROM_WISHLIST, GET_REQUEST_BY_USER_ID, GET_WISH_LIST_FOR_USER } from "../constant";
import authAxios from "../helper/authAxios";
export const add_product_to_wishlist = async(product_id,navigate) =>{
    try{

        const {data}= await authAxios().post(`/wishlist`,{product_id})
      
    
          navigate('/cart')
       
       }  catch(err){
        console.log(err)
       }
}

export const get_wishlist_by_user_id =  () => async dispatch =>{
  try{
      
      const {data}= await authAxios().get(`/wishlist`)
      if(data.status=='success')
      dispatch({type:GET_WISH_LIST_FOR_USER , data : data.data})
     
     }  catch(err){
      console.log(err)
     }
}

export const delete_product_from_wishlist = product_id => async dispatch =>{
  try{
      
    const {data}= await authAxios().delete(`/wishlist/${product_id}`)
    if(data.status=='success')
    dispatch({type:DELETE_PRODUCT_FROM_WISHLIST , product_id : product_id})
   
   }  catch(err){
    console.log(err)
   }
}