import {
  GET_ALL_POST_SUCCESS,
  GET_ANSWER_FOR_QUESTION,
  GET_POST_BY_ID,
  GET_QUESTIONS,
} from "../constant";
import authAxios from "../helper/authAxios";
import store from "../store/store";

export const add_question = async (question) => {
  try {
    const { data } = await authAxios().post(`/qa/question`, question);
    return data;
  } catch (err) {
    return { status: "error", error: err };
  }
};

export const add_answer_for_question = async (obj) => {
  try {
    const { data } = await authAxios().post(`/qa/answer`, obj);
    return data;
  } catch (err) {
    return { status: "error", error: err };
  }
};

export const get_questions = () => async (dispatch) => {
  try {
    const { data } = await authAxios().get(`/qa/questions`);
    if (data.status == "success")
      dispatch({ type: GET_QUESTIONS, data: data.data });
  } catch (err) {
    console.log(err);
  }
};

export const get_answers_by_questions = (question_id) => async (dispatch) => {
  try {
    const { data } = await authAxios().get(`/qa/answer/${question_id}`);
    console.log(data);
    if (data.status == "success")
      dispatch({ type: GET_ANSWER_FOR_QUESTION, data: data.data });
  } catch (err) {
    console.log(err);
  }
};
export const get_questions_by_search_tage =
  ({ search = "", tags = "" }) =>
  async (dispatch) => {
    try {
      const { data } = await authAxios().get(
        `/qa/search?search=${search}&tags=${tags}`
      );

      dispatch({ type: GET_QUESTIONS, data: data.data });
    } catch (err) {
      console.log(err);
    }
  };
