import {
  GET_ALL_POST_SUCCESS,
  GET_ANSWER_FOR_QUESTION,
  GET_POST_BY_ID,
  GET_QUESTIONS,
} from "../constant";
import authAxios from "../helper/authAxios";
import store from "../store/store";

export const add_meeting = async (obj) => {
  try {
    const { data } = await authAxios().post(`/google_meet/request`, obj);
    return data;
  } catch (err) {
    console.log(err);
  }
};
