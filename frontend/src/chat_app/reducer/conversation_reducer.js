import {
  GET_CONVERSATION_SUCCESS,
  GET_MESSAGE_SUCESS,
  GET_NEW_MESSAGE,
  MESSAGE_SEEN_FOR_USER,
  SELECT_CHATTING_USER,
  SELECT_CONVERSATION_USER,
  SELECT_MESSAGE_FOR_REPLY,
} from "../../constant";

const intial_state = {
  direct_chat: {
    conversations: [],
    current_conversation: null,
    current_messages: [],
    message_for_reply: null,
  },
  group_chat: {},
};

const conversationReducer = (state = intial_state, action) => {
  switch (action.type) {
    case SELECT_CHATTING_USER:
      const filter_conversion = state.direct_chat.conversations.filter(
        (u) => u.id != action.user.id
      );
      state.direct_chat.conversations = [action.user, ...filter_conversion];
      state.direct_chat.current_conversation = action.user;

      break;
    case SELECT_CONVERSATION_USER:
      state.direct_chat.current_conversation = action.user;
      break;
    case GET_CONVERSATION_SUCCESS:
      state.direct_chat.conversations = action.data;
      break;
    case GET_MESSAGE_SUCESS:
      state.direct_chat.current_messages = action.data;
      break;
    case GET_NEW_MESSAGE:
      const len = state.direct_chat.current_messages.length;
      if (
        len > 0 &&
        state.direct_chat.current_messages[len - 1].message_id !=
          action.message.id &&
        (state.direct_chat.current_conversation.id ==
          action.message.sender_user_id ||
          state.direct_chat.current_conversation.id ==
            action.message.receiver_user_id)
      )
        state.direct_chat.current_messages = [
          ...state.direct_chat.current_messages,
          action.message,
        ];
      if (
        state.direct_chat.current_conversation?.id !=
        action.message.sender_user_id
      ) {
        state.direct_chat.conversations = state.direct_chat.conversations.map(
          (con) =>
            con.id == action.message.sender_user_id
              ? {
                  ...con,
                  unread_count: con.unread_count + 1,
                  content: action.message.content,
                }
              : con
        );
      }

      break;
    case MESSAGE_SEEN_FOR_USER:
      state.direct_chat.conversations = state.direct_chat.conversations.map(
        (con) =>
          con.id == action.current_user_id ? { ...con, unread_count: 0 } : con
      );
      break;
    case SELECT_MESSAGE_FOR_REPLY:
      state.direct_chat.message_for_reply = action.message;
      break;
    default:
      return state;
  }
  return {
    ...state,
  };
};
export default conversationReducer;
