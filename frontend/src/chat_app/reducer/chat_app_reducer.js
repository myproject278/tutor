import {
  ADD_SOCKET_REQUEST,
  GET_FRIENDS_SUCESS,
  UPDATE_CHAT_TAB,
  ADD_SOCKET_SUCCESS,
  REMOVE_SOCKET,
  TOGGLE_SIDE_BAR,
  UPDATE_SIDE_BAR_TYPE,
} from "../../constant";

const initialState = {
  sideBar: {
    open: false,
    type: "CONTACT", // can be CONTACT, STARRED, SHARED
  },
  tab: 0, // [0, 1, 2, 3]
  snackbar: {
    open: null,
    severity: null,
    message: null,
  },
  users: [], // all users of app who are not friends and not requested yet
  all_users: [],
  friends: [], // all friends
  friendRequests: [], // all friend requests
  chat_type: "individual",
  call_logs: [],
  socket: {
    loading: false,
    socket: null,
  },
};

const chatAppReducer = (state = initialState, action) => {
 
  switch (action.type) {
    case GET_FRIENDS_SUCESS:
      state.friends = action.data;
      break;
    case UPDATE_CHAT_TAB:
      state.tab = action.tab;
      break;
    case TOGGLE_SIDE_BAR:
      state.sideBar.open = !state.sideBar.open;
      break;
    case UPDATE_SIDE_BAR_TYPE:
      state.sideBar.type = action.data;
      break;
    case ADD_SOCKET_REQUEST:
      state.socket.loading = true;
      break;
    case ADD_SOCKET_SUCCESS:
      state.socket.loading = false;
      state.socket.socket = action.payload;
      break;
    case REMOVE_SOCKET:
      return (state.socket = {
        loading: false,
        socket: null,
      });
      break;
    default:
      return state;
  }
  return {
    ...state,
  };
};
export default chatAppReducer;
