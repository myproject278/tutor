import React from "react";
import { Outlet } from "react-router-dom";
import SideBar from "./SideNav";

const DashboardLayout = () => {
 console.log('in ')
  return (
    <>
      <SideBar />
       
      <Outlet />
    </>
  );
};

export default DashboardLayout;
