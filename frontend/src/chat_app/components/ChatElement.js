import React from "react";
import { Box, Badge, Stack, Avatar, Typography } from "@mui/material";
import { styled, useTheme, alpha } from "@mui/material/styles";
import { useSearchParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { get_image_url } from "../../helper/util";
import { SELECT_CONVERSATION_USER } from "../../constant";
import { formatDate } from "../../utils/functions";
//import { SelectConversation } from "../redux/slices/app";

const truncateText = (string, n) => {
  return string?.length > n ? `${string?.slice(0, n)}...` : string;
};

const StyledChatBox = styled(Box)(({ theme }) => ({
  "&:hover": {
    cursor: "pointer",
  },
}));

const StyledBadge = styled(Badge)(({ theme }) => ({
  "& .MuiBadge-badge": {
    backgroundColor: "#44b700",
    color: "#44b700",
    boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
    "&::after": {
      position: "absolute",
      top: 0,
      left: 0,
      width: "100%",
      height: "100%",
      borderRadius: "50%",
      animation: "ripple 1.2s infinite ease-in-out",
      border: "1px solid currentColor",
      content: '""',
    },
  },
  "@keyframes ripple": {
    "0%": {
      transform: "scale(.8)",
      opacity: 1,
    },
    "100%": {
      transform: "scale(2.4)",
      opacity: 0,
    },
  },
}));

const ChatElement = ({
  img,
  name,
  msg,
  time,
  unread,
  online,
  id,
  created_at,
  content,
  receiver_user_id,
  type,
  unread_count,
}) => {
  const dispatch = useDispatch();
  const room_id = id;

  let isSelected = true;

  const theme = useTheme();

  return (
    <StyledChatBox
      onClick={() => {
        dispatch({
          type: SELECT_CONVERSATION_USER,
          user: { id, name, img, online, receiver_user_id, unread_count },
        });
        
      }}
      sx={{
        width: "100%",

        borderRadius: 1,

        backgroundColor: isSelected
          ? theme.palette.mode === "light"
            ? alpha(theme.palette.primary.main, 0.5)
            : theme.palette.primary.main
          : theme.palette.mode === "light"
          ? "#fff"
          : theme.palette.background.paper,
      }}
      p={2}
    >
      <Stack
        direction="row"
        alignItems={"center"}
        justifyContent="space-between"
      >
        <Stack direction="row" spacing={2}>
          {" "}
          {online ? (
            <StyledBadge
              overlap="circular"
              anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
              variant="dot"
            >
              <Avatar alt={name} src={get_image_url(img)} />
            </StyledBadge>
          ) : (
            <Avatar alt={name} src={get_image_url(img)} />
          )}
          <Stack spacing={0.3}>
            <Typography variant="subtitle2">{name}</Typography>
            <Typography variant="caption">
              {truncateText(content, 20)}
            </Typography>
          </Stack>
        </Stack>
        <Stack spacing={2} alignItems={"center"}>
          <Badge
            className="unread-count"
            color="primary"
            badgeContent={receiver_user_id == id ? 0 : unread_count}
          />
        </Stack>
      </Stack>
    </StyledChatBox>
  );
};

export default ChatElement;
