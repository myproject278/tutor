import { Stack, Box, Typography } from "@mui/material";
import React, { useEffect, useRef, useState } from "react";
import { useTheme } from "@mui/material/styles";
import { SimpleBarStyle } from "../../components/Scrollbar";

import { ChatHeader, ChatFooter } from "../../components/Chat";
import useResponsive from "../../hooks/useResponsive";
import { Chat_History } from "../../data";
import animationData from "../../assets/typing.json";
import Lottie from "react-lottie";
import {
  DocMsg,
  LinkMsg,
  MediaMsg,
  ReplyMsg,
  TextMsg,
  Timeline,
} from "../../sections/Dashboard/Conversation";
import { useDispatch, useSelector } from "react-redux";

import { get_message_for_single_user } from "../../action/message_action";
import { MESSAGE_SEEN_FOR_USER } from "../../../constant";

const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: animationData,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};
const Conversation = ({ isMobile, menu }) => {
  const dispatch = useDispatch();
  const { socket } = useSelector((state) => state.chatAppReducer);
  const { conversations, current_messages, current_conversation } = useSelector(
    (state) => state.conversationReducer.direct_chat
  );
  const user_id = useSelector((state) => state.authReducer.user.id);

  const [userTyping, setUserTyping] = useState({
    state: false,
    name: "",
    id: "",
  });
  useEffect(() => {
    if (socket?.socket) {
      socket.socket.on("userTyping", ({ name, sender_id }) =>
        setUserTyping({ name, state: true, id: sender_id })
      );
      socket.socket.on("userStopTyping", ({ sender_id }) =>
        setUserTyping({ ...userTyping, state: false })
      );
    }
  }, [socket]);
  useEffect(() => {
    if (current_conversation?.id) {
      dispatch(get_message_for_single_user(current_conversation.id));
      if (
        socket?.socket &&
        current_conversation.receiver_user_id == user_id &&
        current_conversation.unread_count > 0
      ) {
        socket.socket.emit("readMsg", {
          sender_id: user_id,
          receiver_id: current_conversation.id,
        });
        dispatch({
          type: MESSAGE_SEEN_FOR_USER,
          current_user_id: current_conversation.id,
        });
      }
    }
  }, [current_conversation?.id]);
  return (
    <Box p={isMobile ? 1 : 3}>
      <Stack spacing={3}>
        {current_messages.map((el, idx) => {
          // switch (el.type) {
          //   case "divider":
          //     return (
          //       // Timeline
          //       <Timeline el={el} />
          //     );

          //   case "msg":
          switch (el.type) {
            case "img":
              return (
                // Media Message
                <MediaMsg el={el} menu={menu} />
              );

            case "doc":
              return (
                // Doc Message
                <DocMsg el={el} menu={menu} />
              );
            case "Link":
              return (
                //  Link Message
                <LinkMsg el={el} menu={menu} />
              );

            default:
              if (el.parent_id != null)
                return (
                  <ReplyMsg
                    el={el}
                    menu={menu}
                    current_user_id={current_conversation.id}
                  />
                );
              else
                return (
                  // Text Message
                  <TextMsg
                    el={el}
                    menu={menu}
                    current_user_id={current_conversation.id}
                  />
                );
          }
        })}
      </Stack>
      <div style={{ width: "100%" }}>
        {userTyping.state && userTyping.id == current_conversation.id && (
          <Lottie
            options={defaultOptions}
            width={70}
            style={{ marginLeft: 1, marginTop: 10 }}
          />
        )}
      </div>
    </Box>
  );
};

const ChatComponent = () => {
  const isMobile = useResponsive("between", "md", "xs", "sm");
  const theme = useTheme();

  const messageListRef = useRef(null);

  const { current_messages } = useSelector(
    (state) => state.conversationReducer.direct_chat
  );

  useEffect(() => {
    // Scroll to the bottom of the message list when new messages are added
    messageListRef.current.scrollTop = messageListRef.current.scrollHeight;
  }, [current_messages]);

  return (
    <Stack
      height={"100%"}
      maxHeight={"100vh"}
      width={isMobile ? "100vw" : "auto"}
    >
      {/*  */}
      <ChatHeader />
      <Box
        ref={messageListRef}
        width={"100%"}
        sx={{
          position: "relative",
          flexGrow: 1,
          overflow: "scroll",

          backgroundColor:
            theme.palette.mode === "light"
              ? "#F0F4FA"
              : theme.palette.background,

          boxShadow: "0px 0px 2px rgba(0, 0, 0, 0.25)",
        }}
      >
        <SimpleBarStyle timeout={500} clickOnTrack={false}>
          <Conversation menu={true} isMobile={isMobile} />
        </SimpleBarStyle>
      </Box>

      {/*  */}
      <ChatFooter />
    </Stack>
  );
};

export default ChatComponent;

export { Conversation };
