import { GET_CONVERSATION_SUCCESS, GET_FRIENDS_SUCESS } from "../../constant";
import authAxios from "../../helper/authAxios";

export const get_friends = () => async (dispatch) => {
  try {
    const { data } = await authAxios().get(`/chat/friends`);
    if (data.status == "success")
      dispatch({ type: GET_FRIENDS_SUCESS, data: data.data });
  } catch (err) {
    console.log(err);
  }
};

export const get_conversations = () => async (dispatch) => {
  try {
    const { data } = await authAxios().get(`/chat/conversations`);
    if (data.status == "success")
      dispatch({ type: GET_CONVERSATION_SUCCESS, data: data.data });
  } catch (err) {
    console.log(err);
  }
};
