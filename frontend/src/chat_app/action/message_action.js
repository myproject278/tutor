import { GET_FRIENDS_SUCESS, GET_MESSAGE_SUCESS } from "../../constant";
import authAxios from "../../helper/authAxios";

export const get_friends = () => async (dispatch) => {
  try {
    const { data } = await authAxios().get(`/chat/friends`);
    if (data.status == "success")
      dispatch({ type: GET_FRIENDS_SUCESS, data: data.data });
  } catch (err) {
    console.log(err);
  }
};

export const get_message_for_single_user =
  (other_user_id) => async (dispatch) => {
    try {
      const { data } = await authAxios().get(
        `/messages/single_user/${other_user_id}`
      );
      if (data.status == "success")
        dispatch({ type: GET_MESSAGE_SUCESS, data: data.data });
    } catch (err) {
      console.log(err);
    }
  };
