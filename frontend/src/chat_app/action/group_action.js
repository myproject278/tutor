import authAxios from "../../helper/authAxios";

export const create_group = (form) => async (dispatch) => {
  try {
    const { data } = await authAxios().post(`/group`, form);
    console.log(data);
    //   if (data.status == "success")
    //     dispatch({ type: GET_MESSAGE_SUCESS, data: data.data });
  } catch (err) {
    console.log(err);
  }
};
