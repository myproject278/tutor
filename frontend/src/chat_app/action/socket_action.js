import { io } from "socket.io-client";
import {
  ADD_SOCKET_REQUEST,
  ADD_SOCKET_SUCCESS,
  DELETE_CHAT_SUCCESS,
  DELETE_CHAT_MESSAGE_SUCCESS,
  SET_CHAT_COUNT_ZERO,
  DELETE_MESSAGE_SUCCESS,
  BASE_URL,
  GET_NEW_MESSAGE,
} from "../../constant";

export const addSocket = (user) => (dispatch) => {
  dispatch({ type: ADD_SOCKET_REQUEST });
  const socket = io(BASE_URL);
  socket.emit("setUp", user.id);
  socket.on("connected", () => {
    dispatch({ type: ADD_SOCKET_SUCCESS, payload: socket });

    socket.on("send_message_success", (data) => {
      dispatch({ type: GET_NEW_MESSAGE, message: data.message });
    });
    socket.on("new_message", (data) => {
      dispatch({ type: GET_NEW_MESSAGE, message: data.message });
    });
    socket.on("read_msg_by_receiver", (sender_id) => {
      console.log("new  sender _id data");
      console.log(sender_id);
    });

  });
};
