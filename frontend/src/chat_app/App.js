// routes
import Router from "./routes";
// theme
import ThemeProvider from './theme';
// components
import ThemeSettings from './components/settings';
import DashboardLayout from "./layouts/dashboard";
import GeneralApp from "./pages/dashboard/GeneralApp";
import Group from "./pages/dashboard/Group";


function ChatApp() {
  return (
    <ThemeProvider>
      <ThemeSettings>
        <div style={{ display: "flex", padding: "10px", height: "70vh" }}>
          <DashboardLayout />
          <Group />
        </div>
      </ThemeSettings>
    </ThemeProvider>
  );
}

export default ChatApp;
