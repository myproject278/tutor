import { useEffect } from 'react';
import { useSelector,useDispatch } from 'react-redux';
import {  useNavigate } from 'react-router-dom';
import { useLocation } from "react-router-dom";
import { SET_AUTH_PATH } from '../constant';


const ProtectedRoute = ({ children }) => {
    const dispatch =useDispatch()
    const { authenticate, loading } = useSelector(state => state.authReducer);
    const navigate = useNavigate()
    const location = useLocation()
    useEffect(() => {
        if (!authenticate)
            navigate('/auth')
        else {
           dispatch({type:SET_AUTH_PATH,data:location.pathname+location.search})
        }
    },[authenticate])
   
  
   
    return (
        <>
            { (children
            )}
        </>
    );
};

export default ProtectedRoute;