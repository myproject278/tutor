import React, { useEffect, useState } from "react";
import TransactionTable from "../components/Transaction/TransactionTable";
import { useDispatch,useSelector } from "react-redux";
import { get_total_paid_amount, get_transactions_on_date, get_user_owner_due } from "../actions/transaction_action";
import {TextField, Typography,Button} from '@material-ui/core'
import { Link } from "react-router-dom";
import UserOwnerDue from "../components/Transaction/UserOwnerDueTable";

const Transaction = ()=>{
  
  const other = useSelector(state=>state.transactionReducer.other)
  const remaining = other.paid - other.expenditure 
  const today = new Date();
  const [end_date ,setEndDate] = useState(today.toISOString().split('T')[0])
  today.setMonth(today.getMonth() - 1);
  const [start_date ,setStartDate]=useState(today.toISOString().split('T')[0]);
  const remaining_amount = useSelector(state=>state.transactionReducer.total_paid)
  useEffect(() => {
    dispatch(get_transactions_on_date({start_date , end_date}))
    dispatch(get_total_paid_amount())
  }, []);
  const [ flag, setFlag] = useState(true)
  const dispatch =useDispatch()
  
  
  useEffect(()=>{
    
  },[])
 

  const search_date_expenditure = ()=>{
    setFlag(false)
      dispatch(get_transactions_on_date({start_date , end_date}))
  }
  return (
    <div>
    <div style={{padding:'2%',display: 'flex',flexDirection: 'row',justifyContent: 'space-around', alignItems: 'center'}}>
      <div style={{width: '37%',
    display: 'flex',
    flexDirection: 'column',
    height: '14rem',
    justifyContent: 'space-between'}}>
     { remaining_amount<0?<h1  className="text-stone-950 font-semibold text-xl capitalize" style={{color:'red'}}>Total Unpaid Amount : {remaining_amount}</h1>:
     <h1  className="text-stone-950 font-semibold text-xl capitalize" style={{color:'green'}}>Extra Paid Amount : {remaining_amount}</h1>
     }
     <div style={{    display: 'flex',
    justifyContent: 'space-between'}}>
      <Link
        to={`/transactions/create`}
       
        className="hover:text-rose-500 duration-300 flex justify-between items-center"
      >
        <Button variant="outlined" color="primary">Create Transaction</Button>
      </Link>
      <Link
        to={`/transactions/history`}
        className=" hover:text-rose-500 duration-300 flex justify-between items-center"
      >
        <Button variant="outlined" color="primary">Transaction History</Button>
         
       
      </Link>
      </div>
      </div>
     <div  style={{width:'24%'}}> 
     <div className="single-product flex flex-col bg-gray-50 gap-3 shadow-md hover:shadow-xl hover:scale-105 duration-300 px-4 py-7 rounded-sm overflow-hidden"
      >
      <div className="flex justify-center">
        { flag?'Expenditure Last 31 days':`Enpenditure From ${start_date} To ${end_date}` }
      </div>
     
       <div>
       <h2 className="text-stone-950  font-semibold text-xl capitalize" style={{color:'red'}}>
          Expenditure &nbsp;: {other.expenditure}
        </h2>
     
        <h2 className="text-stone-950 font-semibold text-xl capitalize" style={{color:'green'}}>
          Paid Amount : {other.paid}
        </h2>
        <h2 className="text-stone-950 font-semibold text-xl capitalize" style={{color:`${(other.paid-other.expenditure)<0 ?'red':'green'}`}}>
          Ramaining &nbsp;&nbsp;&nbsp;&nbsp;: {other.paid-other.expenditure}
        </h2>
       </div>
      <div className="flex justify-between items-center" style={{  flexDirection: 'column',height: '9rem'}}>
          <h1> Search Expenditure Between Dates - </h1>
          <div  style={{display :'flex' ,  
    alignItems: 'center',
    justifyContent: 'space-around'}} >
                <Typography>Start Time : </Typography>
                <TextField name='Start Time' type='date' value={start_date}   onChange={e=>setStartDate(e.target.value)}/>
              </div>
              <div  style={{display :'flex' ,  
    alignItems: 'center',
    justifyContent: 'space-around'}}>
                <Typography>End Time : </Typography>
                <TextField name='End Time' type='date' value={end_date}   onChange={e=>setEndDate(e.target.value)}/>
              </div>
        <button
          onClick={search_date_expenditure}
          className="bg-sky-400 text-sky-50 hover:bg-sky-50 hover:text-sky-400 duration-300 border border-sky-400 px-2 py-1 rounded-md"
        >
         Search
        </button>
      </div>
      
          
    </div>
     </div>
     
    </div>
    <UserOwnerDue />
    </div>
  )
}

export default Transaction