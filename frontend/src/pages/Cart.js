import { Link } from "react-router-dom";
import {  get_wishlist_by_user_id } from "../actions/wishlist_action";
import { useEffect } from "react";
import { useDispatch,useSelector} from 'react-redux'
import WishListCard from "../components/WishList/WishListCard";

const Cart = () => {
  const dispatch =useDispatch();
  useEffect(()=>{
      dispatch(get_wishlist_by_user_id())
  },[])
  const wishlists = useSelector(state=>state.wishlistReducer.wishlists)
  console.log(wishlists)
  console.log('wish')
  return  (
    <div className="flex flex-col gap-5  items-center justify-center">
      <div  className="flex flex-col gap-7  items-center justify-center" style={{    width: '100%', padding: '2% 10%',marginTop:'15px'}}>
        {
          wishlists.map(w=>
            <WishListCard product={w}/>)
        }
      </div>
      <Link
        to="/product"
        className="text-xl py-1 text-center hover:text-cyan-500 duration-300 select-none"
      >
        
        &larr; Go to Product
      </Link>
    </div>
  );
};

export default Cart;
