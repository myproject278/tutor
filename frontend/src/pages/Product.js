import { useState, useEffect, useRef } from "react";
import SingleProduct from "../components/SingleProduct";
import { Link } from "react-router-dom";
import { useParams ,useNavigate} from 'react-router-dom';
import { useSelector , useDispatch } from 'react-redux';
import { get_product_by_service, get_product_count } from '../actions/product_action';
import { getParentServiceName } from "../actions/service_action";
import QueryString from 'query-string';
import { useLocation } from 'react-router-dom';
import { Pagination } from "@mui/material";


const Products = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [page,setPage]=useState(1)
  const [err, setErr] = useState(null);
  const { id } = useParams();
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const sort = searchParams.get("sort");
  const service_name = searchParams.get("name")
  const dispatch = useDispatch();
  const navigate = useNavigate()
  const { products, products_count } = useSelector(state => state.productReducer)
  const parent_services = useSelector(state=> state.serviceReducer.parent_services)
  useEffect(()=>{
    if (id) {
      dispatch(getParentServiceName())
      dispatch(get_product_count())
    }
  }, [id])
  useEffect(() => {
    if (id) {
      dispatch(get_product_by_service(id,sort,page-1))
    }
  },[id,page])
  const vehilce_click_handler = id =>{
      if(id)
       navigate(`/product/${id}`)
  }

  const handlePaginationChange =  (event, value) => {
    setPage(value)
  };
 

  return (
    <div className="container mx-auto pb-20">
      <h2 className="text-center text-3xl py-10">{service_name}</h2>
      <div className="flex justify-between gap-10">
        <div className="w-[20%] bg-gray-50 flex flex-col gap-3 px-3 pt-2">
          <h3
            className="select-none cursor-pointer flex justify-between"
            onClick={() => {
              
            }}
          >
            <span className="font-semibold">Services</span>
            <span>{`(${parent_services.length})`}</span>
          </h3>
          {parent_services.map(({id,service_name}, i) => (
            <p
              className="select-none cursor-pointer capitalize font-semibold"
              key={i}
              onClick={() => {
               navigate(`/service/${id}`)
               
              }}
            >
              <span>{service_name}</span>
            </p>
          ))}
        </div>
        <div>
          <p className="text-gray-500 pb-4">
            {<Link to="/">Home </Link>}/
            <span className="text-sky-400 px-1">products</span>
          </p>
          <div className="grid grid-cols-3 gap-10 ">
            {products &&
              products.map((product) => (
                <SingleProduct key={product.id} product={product} />
              ))}
          </div>
          <div style={{
            marginTop: '50px'
          }}> <Pagination count={Math.ceil(products_count/9)} page={page} onChange={handlePaginationChange}  /></div>

        </div>
      </div>
     
    </div>
  );
};

export default Products;
