import { useDispatch, useSelector } from "react-redux";
import { get_recenltly_view_product } from "../actions/product_action";
import BestSelling from "../components/BestSelling";
import Consultation from "../components/Consultation";
import FeatureProducts from "../components/FeatureProducts";
import Slider from "../components/Slider";

import Whyme from "../components/Whyme";
import { useEffect } from "react";

const Home = () => {
  const { authenticate, loading } = useSelector(state => state.authReducer);
  console.log('in homw')
  const dispatch =useDispatch()
  useEffect(() => {
    if(authenticate)
    dispatch(get_recenltly_view_product())
  },[authenticate])
  return (
    <div className="home">
      <Slider />
      <FeatureProducts />
      <Consultation />
      <BestSelling />
    
      <Whyme />
    </div>
  );
};

export default Home;
