import React from "react";
import { TextField, InputAdornment, IconButton } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import "./teacher.css";
const SearchBar = ({ value, onChange }) => {
    return (
      <div className="teacher-search-bar">
        <TextField
          variant="outlined"
          fullWidth
          placeholder="Search Teacher Name..."
          value={value}
          onChange={onChange}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <IconButton disabled>
                  <SearchIcon />
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
      </div>
    );
};

export default SearchBar;
