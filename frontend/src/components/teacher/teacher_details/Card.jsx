import React from "react";
import Heading from "../../common/heading/Heading";
import "./details.css";
import { get_image_url } from "../../../helper/util";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { Button, CardActionArea } from "@mui/material";
import ReviewPage from "./review_page";
import { useNavigate } from "react-router-dom";
const expertiese_level_arr = ["Basic", "Intermediate", "Expert"];
const get_time_am_pm = (i) => {
  if ((i + 1) % 12 == 0) return `12 ${i == 0 ? "Pm" : "Am"}`;
  else return `${(i + 1) % 12} ${i / 12 > 0 ? "Pm" : "Am"}`;
};
const time_from_arr = (arr = []) => {
  const res = [];
  let flag = false;
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] == 1 && flag == false) {
      flag = true;
      res.push(get_time_am_pm(i));
    } else if (arr[i] == 0 && flag == true) {
      res[res.length - 1] = res[res.length - 1] + " - " + get_time_am_pm(i);
      flag = false;
    }
  }
  if (flag) res[res.length - 1] = res[res.length - 1] + " - 12 Am";
  return res;
};
const CardOuter = ({ teacher, subject_name }) => {
  let available_days = [];
  if (teacher && teacher.available_slots)
    available_days = JSON.parse(teacher.available_slots);
  const navigate = useNavigate();
  console.log(teacher);
  return (
    <>
      <section className="teacherHome">
        <div className="container flexSB">
          <div className="left">
            <Card>
              <CardActionArea>
                <div style={{ height: "280px" }}>
                  <CardMedia
                    component="img"
                    image={get_image_url(teacher?.image_url)}
                    alt="green iguana"
                    style={{ objectFit: "cover" }}
                  />
                </div>
                <CardContent>
                  <Typography
                    gutterBottom
                    variant="h5"
                    component="div"
                    align="center"
                  >
                    {teacher?.name}
                  </Typography>
                  <Typography gutterBottom component="div" align="center">
                    {teacher?.email}
                  </Typography>
                  <hr />
                  <Typography variant="h6" align="center">
                    Hourly Rate : {teacher.hourly_rate}
                  </Typography>
                  <Typography variant="h6" align="center">
                    Experties Level :{" "}
                    {expertiese_level_arr[teacher.level_of_knowledge - 1]}
                  </Typography>
                </CardContent>

                <Button
                  variant="contained"
                  fullWidth
                  onClick={() =>
                    navigate(
                      `/add_request/${teacher?.subject_id}/${teacher?.teacher_id}?name=${teacher?.name}&subject_name=${subject_name}&hourly_rate=${teacher?.hourly_rate}&level_of_knowledge=${teacher?.level_of_knowledge}`
                    )
                  }
                >
                  Contact {teacher?.name?.split(" ")[0]}
                </Button>
              </CardActionArea>
            </Card>
          </div>
          <div className="right row">
            <div className="items">
              <div className="item" style={{ display: "flex" }}>
                <div className="img">
                  <img
                    src={
                      "https://img.icons8.com/dotty/80/000000/storytelling.png"
                    }
                    alt=""
                  />
                </div>
                <div className="text">
                  <h2>{teacher?.header_info}</h2>
                  <p>{teacher?.other_info}</p>
                </div>
              </div>
              <hr />
              <div className="item " style={{ display: "flex" }}>
                <div className="img">
                  <img
                    src="https://img.icons8.com/ios/80/000000/diploma.png"
                    alt=""
                  />
                </div>
                <div className="text">
                  <h2> Education : </h2>
                  <p>{teacher?.secound_last_education}</p>
                </div>
              </div>
              <hr />
              <div className="item" style={{ display: "flex" }}>
                <div className="img">
                  <img
                    src="https://img.icons8.com/ios/80/000000/athlete.png"
                    alt=""
                  />
                </div>
                <div className="text">
                  <h2>Education :</h2>
                  <p>{teacher?.last_education}</p>
                </div>
              </div>
            </div>
            <hr />
            <div className="item " style={{ display: "flex" }}>
              <div className="img">
                <img
                  src="https://img.icons8.com/ios/80/000000/diploma.png"
                  alt=""
                />
              </div>
              <div className="text">
                <h2> Speaking Language : </h2>
                <p>
                  {JSON.parse(
                    teacher?.speaking_languaes
                      ? teacher?.speaking_languaes
                      : null
                  )?.join(" , ")}
                </p>
              </div>
            </div>
            <hr />
            <div className="item " style={{ display: "flex" }}>
              <div className="text" style={{ padding: "18px" }}>
                <h2> Bio : </h2>
                <p>{teacher?.bio}</p>
              </div>
            </div>

            <div style={{ marginTop: "40px" }}>
              <hr />
              <div className="text" style={{ padding: "18px" }}>
                <h3> Available Time On Hour Basis : </h3>
                <div>
                  <div
                    className="availabilityslotdiv"
                    style={{ color: "#6a6969" }}
                  >
                    <p>
                      Sunday :{" "}
                      <Typography color="black" style={{ display: "inline" }}>
                        {time_from_arr(available_days?.sunday)?.join(" , ")}
                      </Typography>{" "}
                    </p>
                    <p>
                      Monday :{" "}
                      <Typography color="black" style={{ display: "inline" }}>
                        {" "}
                        {time_from_arr(available_days?.monday)?.join(" , ")}
                      </Typography>
                    </p>
                    <p>
                      Tuesday :
                      <Typography color="black" style={{ display: "inline" }}>
                        {time_from_arr(available_days?.tuesday)?.join(" , ")}
                      </Typography>{" "}
                    </p>
                    <p>
                      Wednesday :{" "}
                      <Typography color="black" style={{ display: "inline" }}>
                        {time_from_arr(available_days?.wednesday)?.join(" , ")}
                      </Typography>{" "}
                    </p>
                    <p>
                      Thursday :{" "}
                      <Typography color="black" style={{ display: "inline" }}>
                        {time_from_arr(available_days?.thursday)?.join(" , ")}
                      </Typography>{" "}
                    </p>
                    <p>
                      Friday :{" "}
                      <Typography color="black" style={{ display: "inline" }}>
                        {time_from_arr(available_days?.friday)?.join(" , ")}
                      </Typography>{" "}
                    </p>
                    <p>
                      Saturday :{" "}
                      <Typography color="black" style={{ display: "inline" }}>
                        {time_from_arr(available_days?.saturday)?.join(" , ")}
                      </Typography>{" "}
                    </p>
                  </div>
                </div>
                <hr />
                <ReviewPage
                  teacher_id={teacher.teacher_id}
                  subject_id={teacher.subject_id}
                />
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default CardOuter;
