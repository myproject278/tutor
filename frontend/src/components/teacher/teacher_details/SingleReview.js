
import React from "react";
import { Avatar, Typography, Rating } from "@mui/material";
import { get_image_url } from "../../../helper/util";

const TeacherReview = ({ review, key }) => {
    const { name, date, rating, description, studentImage,image_urls }=review
  // Format the date
    const formattedDate = new Date(date).toLocaleDateString();
    let images= []
  if(image_urls != null){
    images=JSON.parse(image_urls)
   }
  return (
    <div className="flex items-center border-b p-4">
      <Avatar src={get_image_url( studentImage)} alt={name} sx={{ mr: 3 }} />
      <div>
        <Typography variant="h6" fontWeight="bold">
          {name}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {formattedDate}
        </Typography>
        <Rating value={rating} precision={0.5} readOnly />
              <Typography variant="body1">{description}</Typography>
              <div style={{
            display:'flex'}}>
        {images.map((image, index) => (
            <img key={index} src={image.img} alt={`Reviewer ${index}`} style={{
                width: '50px', height: '50px', objectFit: 'contain', marginRight: '10px'
            }} />
        ))}
        </div>
      </div>
    </div>
  );
};

export default TeacherReview;
