import React, { useEffect, useState } from "react"
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import Rating from '@mui/material/Rating';
import TextField from '@mui/material/TextField';
import StarIcon from '@mui/icons-material/Star';
import ImageUpload from "../../ImageUpload/ImageUpload";
import { useDispatch, useSelector } from "react-redux";
import SingleReview from "./SingleReview";
import styled from '@emotion/styled'
import { get_review_for_teacher, add_review } from "../../../actions/review_action";
import './review.css'
const ReviewPage = ({ teacher_id,subject_id ,avg_rating=0, total_reviews=0 }) => {
    const product_id = 1;
  
    const [open, setOpen] = useState(false);
    const [viewAll, setViewAll] = useState(false);
    const [rating, setRating] = useState(0);
    const [comment, setComment] = useState("");
    const [images, setImages] = useState([]);
    const reviews = useSelector(state => state.reviewReducer.reviews)
    const dispatch= useDispatch()

    useEffect(() => {
        if(teacher_id && subject_id)
        dispatch(get_review_for_teacher({ teacher_id ,subject_id}))
    },[teacher_id,subject_id])

    const handleDialogClose = () => {
        setOpen(!open);
    }
    const reviewSubmitHandler = () => {
        const form = new FormData()
        form.set('rating', rating)
        form.set('teacher_id', teacher_id)
        form.set('subject_id', subject_id)
        form.set('description',comment)
        images.forEach((file, index) => {
            form.append('images', file);
        });
        dispatch(add_review(form,setOpen))
      
    }

    return (
      <div>
        <div className="w-full mt-4 rounded-sm flex flex-col">
          <div className="flex justify-between items-center border-b px-6 py-4 heading-review">
            <h1 className="text-2xl font-medium heading-header">
              Ratings & Reviews
            </h1>
            <button
              onClick={handleDialogClose}
              className="px-4 py-2 rounded-sm hover:shadow-lg heading-rate-now-button"
            >
              Rate Now
            </button>
          </div>

          <Dialog
            aria-labelledby="review-dialog"
            open={open}
            className="add-review-container"
            onClose={handleDialogClose}
          >
            <DialogTitle className="border-b">Submit Review</DialogTitle>
            <DialogContent className="flex flex-col m-1 gap-4">
              <Rating
                onChange={(e) => setRating(e.target.value)}
                value={rating}
                size="large"
                precision={0.5}
              />

              <TextField
                label="Review"
                multiline
                rows={3}
                size="small"
                variant="outlined"
                className="add-review-text-field"
                value={comment}
                onChange={(e) => setComment(e.target.value)}
              />
              <ImageUpload images={images} setImages={setImages} />
            </DialogContent>
            <DialogActions>
              <button
                onClick={handleDialogClose}
                className="py-2 px-6 rounded shadow bg-white border border-red-500 hover:bg-red-100 text-red-600 uppercase"
              >
                Cancel
              </button>
              <button
                onClick={reviewSubmitHandler}
                className="py-2 px-6 rounded bg-blue-600 hover:bg-green-700 text-white shadow uppercase"
              >
                Submit
              </button>
            </DialogActions>
          </Dialog>
          <div
            className="flex items-center border-b"
            style={{ alignItems: "center" }}
          >
            <h1
              className="px-6 py-3 text-3xl font-semibold"
              style={{ display: "flex", alignItems: "center" }}
            >
              <p className="text-lg text-gray-500">Avg Rating - </p> &nbsp;{" "}
              {avg_rating}
              <StarIcon />
            </h1>
            <p className="text-lg text-gray-500">{total_reviews} Reviews</p>
          </div>

          {viewAll
            ? reviews
                ?.map((rev, i) => <SingleReview review={rev} key={i} />)
                .reverse()
            : reviews
                ?.slice(-3)
                .map((rev, i) => <SingleReview review={rev} key={i} />)
                .reverse()}
          {reviews?.length > 3 && (
            <button
              onClick={() => setViewAll(!viewAll)}
              className="w-1/3 m-2 rounded-sm shadow hover:shadow-lg py-2 bg-primary-blue text-white"
            >
              {viewAll ? "View Less" : "View All"}
            </button>
          )}
        </div>
      </div>
    );
}

export default ReviewPage