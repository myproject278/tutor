import React, { useEffect } from "react";
import "./details.css";
import Back from "../../common/back/Back";
import Card from "./Card";
import { useDispatch, useSelector } from "react-redux";
import { get_subject_teacher_details_by_id } from "../../../actions/subject_teacher_action";
import { useLocation, useParams } from "react-router-dom";

const TeacherDetails = () => {
  const { subject_id, teacher_id } = useParams();
  const dispatch = useDispatch();
  const teacher = useSelector(
    (state) => state.subjectTeacherReducer.teacher_details
  );
  const location = useLocation();

  const searchParams = new URLSearchParams(location.search);
  const subject_name = searchParams.get("subject_name");
  useEffect(() => {
    if (subject_id && teacher_id)
      dispatch(get_subject_teacher_details_by_id({ subject_id, teacher_id }));
  }, [subject_id, teacher_id]);
  return (
    <>
      <Back
        title={teacher.name}
        style={{ height: "30vh", paddingBottom: "12%" }}
      />
      <div style={{ marginTop: "10%" }}></div>
      <Card teacher={teacher} subject_name={subject_name} />
    </>
  );
};

export default TeacherDetails;
