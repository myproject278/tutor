import React, { useEffect, useState } from "react";
import "./teacher.css";
import SearchBar from "./SearchBar";
import HourlyRateFilter from "./TeacherFilters";
import TeacherFilter from "./TeacherFilters";
import _ from "lodash";
import {
  Slider,
  Typography,
  Checkbox,
  FormGroup,
  FormControlLabel,
  Select,
  MenuItem,
  FormControl,
} from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import { get_teachers_by_subject } from "../../actions/subject_teacher_action";
import { useLocation, useParams } from "react-router-dom";
import OneTeacherInfo from "./OneTeacherInfo";

const TeacherList = () => {
  const [searchText, setSearchText] = useState("");
  const [sort_column, setSortColumn] = useState("id");
  const { id } = useParams();
  const location = useLocation();
  const [expertiseLevel, setExpertiseLevel] = useState(0);

  const [daysAvailability, setDaysAvailability] = useState({
    sunday: true,
    monday: true,
    tuesday: true,
    wednesday: true,
    thursday: true,
    friday: true,
    saturday: true,
  });

  const searchParams = new URLSearchParams(location.search);
  const sort = searchParams.get("sort");
  const subject_name = searchParams.get("name");

  const dispatch = useDispatch();
  useEffect(() => {
    const trueDays = Object.keys(daysAvailability).filter(
      (day) => daysAvailability[day]
    );
    dispatch(
      get_teachers_by_subject(
        id,
        sort_column,
        0,
        trueDays.join(","),
        expertiseLevel,
        searchText
      )
    );
  }, [sort_column, daysAvailability, expertiseLevel, searchText]);
  const teacher_list = useSelector(
    (state) => state.subjectTeacherReducer.teacher_list
  );

  const handle_sort_column = (e) => {
    setSortColumn(e.target.value);
  };
  return (
    <div className="outer-teacher-div">
      <div style={{ display: "flex" }} className="outer-teacher-1">
        <TeacherFilter
          expertiseLevel={expertiseLevel}
          setExpertiseLevel={setExpertiseLevel}
          daysAvailability={daysAvailability}
          setDaysAvailability={setDaysAvailability}
        />

        <div className="right-side-teacher-div">
          <SearchBar
            value={searchText}
            onChange={(e) => setSearchText(e.target.value)}
          />
          <div>
            <div className="below-search-teacher-list">
              <Typography> Tutor fit your choices</Typography>
              <div className="sort-div">
                <Typography gutterBottom style={{ marginRight: "20px" }}>
                  Sort :
                </Typography>
                <FormControl>
                  <Select value={sort_column} onChange={handle_sort_column}>
                    <MenuItem value="id">id</MenuItem>
                    <MenuItem value="level_of_knowledge">Exporties</MenuItem>
                    <MenuItem value="hourly_rate">Hourly Rate</MenuItem>
                  </Select>
                </FormControl>
              </div>
            </div>
            <div>
              {teacher_list.map((teacher) => (
                <OneTeacherInfo
                  teacher={teacher}
                  subject_id={id}
                  subject_name={subject_name}
                />
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TeacherList;
