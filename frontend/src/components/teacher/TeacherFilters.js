import React, { useState } from "react";
import { Slider, Typography, Checkbox, FormGroup, FormControlLabel, Select, MenuItem, FormControl } from "@material-ui/core";
import "./teacher.css";
const TeacherFilter = ({
  expertiseLevel,
  setExpertiseLevel,
  daysAvailability,
  setDaysAvailability,
}) => {
  const handleDayAvailabilityChange = (event) => {
    setDaysAvailability({
      ...daysAvailability,
      [event.target.name]: event.target.checked,
    });
  };
  const [click, setClick] = useState(false);

  const handleExpertiseChange = (event) => {
    setExpertiseLevel(event.target.value);
  };
  return (
    <div className="fileter-outer">
      <div style={{ display: "flex", justifyContent: "flex-end" }}>
        <button className="toggle-filter" onClick={() => setClick(!click)}>
          {click ? (
            <i className="fa fa-times"> </i>
          ) : (
            <i className="fa fa-bars"></i>
          )}
        </button>
      </div>
      <div className={!click && "filter-click-div"}>
        <Typography variant="h4">Filters</Typography>
        <div className="filter-expertise">
          <Typography gutterBottom>Level Of Expertise</Typography>
          <FormControl>
            <Select value={expertiseLevel} onChange={handleExpertiseChange}>
              <MenuItem value="0" selected>
                Select
              </MenuItem>
              <MenuItem value="1">Basic</MenuItem>
              <MenuItem value="2">Medium</MenuItem>
              <MenuItem value="3">Expert</MenuItem>
            </Select>
          </FormControl>
        </div>
        <div className="teacher-filter-availability">
          <Typography gutterBottom>Availability by Day</Typography>
          <FormGroup>
            <FormControlLabel
              control={
                <Checkbox
                  checked={daysAvailability.sunday}
                  onChange={handleDayAvailabilityChange}
                  name="sunday"
                />
              }
              label="Sunday"
            />
            <FormControlLabel
              control={
                <Checkbox
                  checked={daysAvailability.monday}
                  onChange={handleDayAvailabilityChange}
                  name="monday"
                />
              }
              label="Monday"
            />
            <FormControlLabel
              control={
                <Checkbox
                  checked={daysAvailability.tuesday}
                  onChange={handleDayAvailabilityChange}
                  name="tuesday"
                />
              }
              label="Tuesday"
            />
            <FormControlLabel
              control={
                <Checkbox
                  checked={daysAvailability.wednesday}
                  onChange={handleDayAvailabilityChange}
                  name="wednesday"
                />
              }
              label="Wednesday"
            />
            <FormControlLabel
              control={
                <Checkbox
                  checked={daysAvailability.thursday}
                  onChange={handleDayAvailabilityChange}
                  name="thursday"
                />
              }
              label="Thursday"
            />
            <FormControlLabel
              control={
                <Checkbox
                  checked={daysAvailability.friday}
                  onChange={handleDayAvailabilityChange}
                  name="friday"
                />
              }
              label="Friday"
            />
            <FormControlLabel
              control={
                <Checkbox
                  checked={daysAvailability.saturday}
                  onChange={handleDayAvailabilityChange}
                  name="saturday"
                />
              }
              label="Saturday"
            />
          </FormGroup>
        </div>
      </div>
    </div>
  );
};

export default TeacherFilter;
