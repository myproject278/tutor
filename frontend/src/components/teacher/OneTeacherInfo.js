import { Link } from "react-router-dom";
import "./searchTeacher.css";
import { get_image_url } from "../../helper/util";
import { Typography } from "@material-ui/core";

const expertiese_level_arr = ["Basic", "Intermediate", "Expert"];
const OneTeacherInfo = ({ teacher, subject_id, subject_name }) => {
  return (
    <div className="searchItem">
      <img src={get_image_url(teacher.image_url)} alt="" className="siImg" />
      <div className="siDesc">
        <h1 className="siTitle">{teacher.name}</h1>
        <Typography variant="h6">{teacher.header_info}</Typography>
        <span className="siFeatures">
          {teacher.other_info && teacher.other_info.substring(0, 90)}
        </span>
      </div>
      <div className="siDetails">
        <div className="siRating">
          <div style={{ display: "flex", alignItems: "center" }}>
            <div> Experties Level : &nbsp; &nbsp;</div>
            <Typography variant="h6">
              {expertiese_level_arr[teacher.level_of_knowledge - 1]}
            </Typography>
          </div>
        </div>
        <div className="siDetailTexts">
          <span className="siPrice">{teacher.hourly_rate}&nbsp; ₹</span>
          <span className="siTaxOp">Hourly Rate </span>
          <Link
            to={`/subject/teacher_details/${subject_id}/${teacher.id}?subject_name=${subject_name}`}
          >
            <button className="siCheckButton">
              View {teacher.name.split(" ")[0]} Profile
            </button>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default OneTeacherInfo;
