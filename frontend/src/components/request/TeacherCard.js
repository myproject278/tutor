import React from "react";
import { get_image_url } from "../../helper/util";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { Button, CardActionArea } from "@mui/material";
import "./style.css";

const expertiese_level_arr = ["Basic", "Intermediate", "Expert"];

const TeacherCard = ({
  image_url,
  name,
  level_of_knowledge,
  hourly_rate,
  email,
}) => {
  return (
    <Card className="teacher_card_outer">
      <CardActionArea>
        <div style={{ height: "280px", width: "339px" }}>
          <CardMedia
            component="img"
            image={get_image_url(image_url)}
            alt="green iguana"
            style={{ objectFit: "cover", width: "100%", height: "100%" }}
          />
        </div>
        <CardContent>
          <Typography gutterBottom variant="h5" component="div" align="center">
            {name}
          </Typography>
          <Typography gutterBottom component="div" align="center">
            {email}
          </Typography>
          <hr />
          <Typography variant="h6" align="center">
            Hourly Rate : {hourly_rate}
          </Typography>
          <Typography variant="h6" align="center">
            Experties Level : {expertiese_level_arr[level_of_knowledge - 1]}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
};

export default TeacherCard;
