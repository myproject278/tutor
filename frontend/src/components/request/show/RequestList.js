import React, { useEffect, useState } from "react";
import TextField from "@mui/material/TextField";
import Box from "@mui/material/Box";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import Checkbox from "@mui/material/Checkbox";
import { useDispatch, useSelector } from "react-redux";
import { get_request_by_student_id } from "../../../actions/request_action";
import RequestItem from "./RequestItem";
import "./request.css";

const RequestList = () => {
  const [searchQuery, setSearchQuery] = useState("");
  const [checkedStatus, setCheckedStatus] = useState("");
  const requests = useSelector((state) => state.requestReducer.requests);
  const handleSearchChange = (event) => {
    setSearchQuery(event.target.value);
  };
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(get_request_by_student_id(searchQuery, checkedStatus));
  }, [searchQuery, checkedStatus]);
  const statusOptions = [
    { label: "Requested", value: "0" },
    { label: "Approved", value: "1" },
    { label: "Payment", value: "2" },
    { label: "Completed", value: "3" },
    { label: "Closed", value: "4" },
    { label: "Canceled", value: "5" },
  ];

  const handleStatusCheckboxChange = (value) => () => {
    setCheckedStatus(checkedStatus === value ? "" : value);
  };

  return (
    <Box className="outer-dive-request">
      <div style={{ display: "flex" }} className="main-request-div">
        <Box className="request-left-div">
          {statusOptions.map(({ label, value }) => (
            <ListItem
              key={value}
              button
              onClick={handleStatusCheckboxChange(value)}
            >
              <Checkbox checked={checkedStatus === value} />
              <ListItemText primary={label} />
            </ListItem>
          ))}
        </Box>
        <div className="request-right-div">
          <TextField
            label="Search by subject or teacher"
            variant="outlined"
            value={searchQuery}
            onChange={(e) => {
              setSearchQuery(e.target.value);
            }}
            fullWidth
            className="text-input-request"
            margin="normal"
          />

          <List className="list-reqeust">
            {requests.map((request) => (
              <RequestItem request={request} />
            ))}
          </List>
        </div>
      </div>
      {/* Search bar */}

      {/* Status filter */}

      {/* Display the request list */}
    </Box>
  );
};

export default RequestList;
