import React from "react";
import RadioButtonUncheckedIcon from "@mui/icons-material/RadioButtonUnchecked";
import CircleIcon from "@mui/icons-material/Circle";
import { Link } from "react-router-dom";
import { formatDate } from "../../../utils/functions";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import EventIcon from "@mui/icons-material/Event";
import TimerIcon from "@mui/icons-material/Timer";
import AccessTimeIcon from "@mui/icons-material/AccessTime";
import Box from "@mui/material/Box";
import Avatar from "@mui/material/Avatar";
import { get_image_url } from "../../../helper/util";
import "./request.css";
const statusOptions = [
  "Requested",
  "Approved",
  "Payment",
  "Completed",
  "Closed",
  "Canceled",
];
const RequestItem = ({ request }) => {
  const {
    id,
    goal_of_tutoring,
    request_time,

    status,
    subject_image,
    subject_name,
    teacher_image,
    teacher_name,
    total_hours,
  } = request;

  const getStatusIcon = () => {
    if (status === 0) {
      return <RadioButtonUncheckedIcon fontSize="small" color="primary" />;
    } else {
      return <CircleIcon fontSize="small" color="secondary" />;
    }
  };

  return (
    <Card className="request-card">
      <CardContent>
        <Grid container spacing={2} alignItems="center">
          <Grid item xs={12}>
            <Box display="flex" alignItems="center" mb={2}>
              <Avatar
                alt={teacher_name}
                src={get_image_url(teacher_image)}
                sx={{ width: 70, height: 70, marginRight: 5 }}
              />
              <Typography variant="h6">{teacher_name}</Typography>
            </Box>
          </Grid>
          <Grid item xs={12} className="subject-image-request">
            <img
              src={get_image_url(subject_image)}
              alt="Subject"
              style={{
                width: "100%",
                height: "100%",
                objectFit: "fill",
                borderRadius: 8,
                boxShadow: "0px 4px 6px rgba(0, 0, 0, 0.1)",
              }}
            />
          </Grid>
          <Grid item xs={12}>
            <Typography variant="h6" style={{ marginTop: 10 }}>
              {subject_name}
            </Typography>
            <div className="info-request-1">
              <Typography
                variant="body2"
                color="textSecondary"
                display="flex"
                className="request-time-typo"
              >
                <EventIcon fontSize="small" />
                <div>Requested on: {formatDate(request_time)}</div>
              </Typography>
              <Typography
                display="flex"
                variant="body2"
                color="textSecondary"
                className="request-goal"
              >
                <TimerIcon fontSize="small" />
                <div>Goal of tutoring: {goal_of_tutoring}</div>
              </Typography>
              <Typography
                display="flex"
                variant="body2"
                color="textSecondary"
                className="request-goal"
              >
                <AccessTimeIcon fontSize="small" />{" "}
                <div>Total hours: {total_hours}</div>
              </Typography>
            </div>
            <div className="request-goal">
              <Box display="flex" alignItems="center" marginTop={1}>
                <Typography variant="body2" sx={{ marginRight: 1 }}>
                  {getStatusIcon()}
                </Typography>
                <Typography variant="body2" color="textSecondary">
                  Status: {statusOptions[status]}
                </Typography>
              </Box>
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className="button-request-d">
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "flex-end",
                }}
                className="box-button-grid"
              >
                <Link color="primary" component={Link} to={`/requests/${id}`}>
                  View Details
                </Link>
              </Box>
            </div>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

export default RequestItem;
