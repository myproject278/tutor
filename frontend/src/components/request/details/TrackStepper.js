import { Step, StepLabel, Stepper } from "@mui/material";
import CircleIcon from "@mui/icons-material/Circle";
import "./style.css";
const TrackStepper = ({ activeStep }) => {
  const steps = ["Requested", "Approved", "Payment", "Completed", "Closed"];
  const completedIcon = (
    <span className="text-primary-green animate-pulse">
      <CircleIcon sx={{ fontSize: "16px" }} />
    </span>
  );
  const pendingIcon = (
    <span className="text-gray-400">
      <CircleIcon sx={{ fontSize: "16px" }} />
    </span>
  );

  return (
    <Stepper activeStep={activeStep} alternativeLabel>
      {steps.map((item, index) => (
        <Step
          key={index}
          active={activeStep === index - 1 ? true : false}
          completed={activeStep >= index - 1 ? true : false}
        >
          <StepLabel
            icon={activeStep >= index - 1 ? completedIcon : pendingIcon}
          >
            {activeStep >= index ? (
              <div className="flex flex-col">
                <span className="text-primary-green font-medium">{item}</span>
              </div>
            ) : (
              <span className="text-gray-400 font-medium"></span>
            )}
          </StepLabel>
        </Step>
      ))}
    </Stepper>
  );
};

export default TrackStepper;
