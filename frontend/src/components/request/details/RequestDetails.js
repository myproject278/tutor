// RequestDetails.js
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import {
  get_request_by_id,
  handle_request_state_change,
} from "../../../actions/request_action";
import { Typography, Button } from "@mui/material";
import { get_image_url } from "../../../helper/util";
import "./style.css";
import Table from "@mui/material/Table";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableRow from "@mui/material/TableRow";
import TrackStepper from "./TrackStepper";
import { add_meeting } from "../../../actions/google_meet_action";
import { formatDate } from "../../../utils/functions";
import { add_notification } from "../../../actions/notification_action";

const RequestDetails = () => {
  const dispatch = useDispatch();
  const { request_id } = useParams();
  const {
    approve_slot,
    bio,
    current_position,
    phone_no,
    email,
    goal_of_tutoring,
    id,
    request_time,
    slot_1,
    slot_2,
    slot_3,
    status,
    subject_image,
    teacher_id,
    subject_name,
    teacher_image,
    teacher_name,
    meet_link,
    total_hours,
  } = useSelector((state) => state.requestReducer.requestDetails);
  console.log(status);
  console.log(meet_link);
  const student = useSelector((state) => state.authReducer.user);
  useEffect(() => {
    if (request_id) dispatch(get_request_by_id(request_id));
  }, [request_id]);
  const navigate = useNavigate();
  const handleAddMeeting = async () => {
    try {
      const data = await add_meeting({
        teacher_email: email,
        student_email: student.email,
        request_id: id,
      });
      const newWindow = window.open(data);
    } catch (err) {
      console.log("An error occurred while adding the meeting.");
    }
  };
  const openGoogleMeet = () => {
    window.open(meet_link, "_blank");
  };
  const handle_payment_success = () => {
    handleAddMeeting();

    dispatch(
      add_notification({
        message: `${student.name} are payment for teaching the ${subject_name}`,
        redirect_link: `/requests/${id}`,
        user_id: teacher_id,
      })
    );
    dispatch(handle_request_state_change({ request_id: id, status: 2 }));
  };
  const handle_cancel_request = () => {
    dispatch(handle_request_state_change({ request_id: id, status: 5 }));
    dispatch(
      add_notification({
        message: `${student.name} are cancel request for ${subject_name}`,
        redirect_link: `/requests/${id}`,
        user_id: teacher_id,
      })
    );
  };
  return (
    <div className="request-details-container">
      <div elevation={3} className="request-details">
        {status == 5 && (
          <Typography variant="h6" color="red">
            This Request Is Canceled
          </Typography>
        )}
        <div className="teacher-info">
          {teacher_image && (
            <div className="info-image-div">
              <img src={get_image_url(teacher_image)} alt={teacher_name} />
            </div>
          )}
          <div className="teacher_info-text">
            <Typography variant="h3">{teacher_name}</Typography>
            <Typography variant="body1">{current_position}</Typography>
            <Typography variant="body1">{email}</Typography>
          </div>
        </div>
        <div className="bio-info">
          <Typography variant="h6">Bio:</Typography>
          <Typography variant="body1">{bio}</Typography>
        </div>
        <hr />
        <Typography variant="h6" className="request_info-header">
          Request Info
        </Typography>
        <div className="subject-info">
          {subject_image && (
            <div className="info-image-div">
              <img src={get_image_url(subject_image)} alt={subject_name} />
            </div>
          )}
          <TableContainer className="request-info-subect">
            <Table sx={{ width: 450 }} aria-label="simple table">
              <TableRow
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  Subject Name
                </TableCell>
                <TableCell align="left" className="request-columne">
                  :
                </TableCell>
                <TableCell align="left" className="subject-name-request">
                  {subject_name}
                </TableCell>
              </TableRow>
              <TableRow
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  Requested Time
                </TableCell>
                <TableCell align="left" className="request-columne">
                  :
                </TableCell>
                <TableCell align="left" className="subject-name-request">
                  {formatDate(request_time)}
                </TableCell>
              </TableRow>
              <TableRow
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  Total Hours
                </TableCell>
                <TableCell align="left" className="request-columne">
                  :
                </TableCell>
                <TableCell align="left" className="subject-name-request">
                  {total_hours} Hour
                </TableCell>
              </TableRow>
              {meet_link && (
                <TableRow
                  sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                >
                  <TableCell component="th" scope="row">
                    Meet Link
                  </TableCell>
                  <TableCell align="left" className="request-columne">
                    :
                  </TableCell>
                  <TableCell align="left" className="subject-name-request">
                    <button
                      style={{
                        color: "white",
                        backgroundColor: "greeen",
                        padding: "10px 10px",
                      }}
                      onClick={openGoogleMeet}
                    >
                      Join Meet
                    </button>
                  </TableCell>
                </TableRow>
              )}
            </Table>
          </TableContainer>
        </div>

        <div className="request-info">
          <div className="bio-info">
            <Typography variant="h6">Goal Of Tutoring :</Typography>
            <Typography variant="body1">{goal_of_tutoring}</Typography>
          </div>
          <hr />
          {status < 5 && (
            <div
              className="sm:w-1/2 border-r"
              style={{
                alignItems: "center",
                display: "flex",
                justifyContent: "center",
                margin: "40px 0",
              }}
            >
              {" "}
              <div className="flex flex-col w-full ">
                <h3
                  className="font-medium sm:text-center"
                  style={{
                    marginBottom: "15px",
                  }}
                >
                  Request Status :
                </h3>
                <TrackStepper activeStep={status} />
              </div>{" "}
            </div>
          )}
        </div>
        {status < 5 &&
          (status == 0 ? (
            <div className="time-slots">
              <Typography variant="h6">Requested Time Slots:</Typography>
              <div style={{ marginLeft: "40px" }}>
                {slot_1 && <Typography>Slot 1 : {slot_1}</Typography>}
                {slot_2 && <Typography>Slot 2 : {slot_2}</Typography>}
                {slot_3 && <Typography>Slot 2 : {slot_3}</Typography>}
              </div>
            </div>
          ) : (
            <div>
              {" "}
              <Typography>
                Approved Slot :{" "}
                {approve_slot == 1
                  ? slot_1
                  : approve_slot == 2
                  ? slot_2
                  : slot_3}
              </Typography>
            </div>
          ))}
        {status < 5 && (
          <div
            style={{
              marginTop: "40px",
              padding: "30px 20px",
              display: "flex",
              justifyContent: "space-between",
              border: "2px solid #eeeeee",
            }}
          >
            <div>
              <button
                style={{
                  color: "#fff",
                  backgroundColor: "#d32f2f",
                  padding: "10px 10px",
                }}
                onClick={handle_cancel_request}
              >
                Cancel Request
              </button>
            </div>
            <div>
              {status == 1 && (
                <button
                  style={{
                    color: "white",
                    backgroundColor: "greeen",
                    padding: "10px 10px",
                  }}
                  onClick={handle_payment_success}
                >
                  Payment Successfull
                </button>
              )}
            </div>
          </div>
        )}
        {status == 1 && (
          <Typography variant="body2" color="#828282">
            Tip : Make Payment to{" "}
            <Typography display="inline" color="black">
              {phone_no}
            </Typography>{" "}
            via various method like phone-pay,google-pay , once the payment get
            Successfull click on payment Successfull button
          </Typography>
        )}
      </div>
    </div>
  );
};

export default RequestDetails;
