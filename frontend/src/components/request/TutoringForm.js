import React, { useEffect, useState } from "react";
import AddRequestForm from "./AddRequestForm";
import "./style.css";
import TeacherCard from "./TeacherCard";
import { useLocation, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { get_teacher_for_request_form } from "../../actions/teacher_action";
const TutoringForm = () => {
  const location = useLocation();
  const { teacher_id, subject_id } = useParams();
  const [teacher, SetTeacher] = useState({});
  const searchParams = new URLSearchParams(location.search);
  const subject_name = searchParams.get("subject_name");
  const level_of_knowledge = searchParams.get("level_of_knowledge");
  const hourly_rate = searchParams.get("hourly_rate");
  const dispatch = useDispatch();
  const tempteacher = useSelector(
    (state) => state.subjectTeacherReducer.teacher_details
  );
  useEffect(() => {
    SetTeacher(tempteacher);
  }, [tempteacher]);
  useEffect(() => {
    if (teacher_id) dispatch(get_teacher_for_request_form(teacher_id));
  }, [teacher_id]);
  return (
    <div className="form-container">
      {" "}
      {/* Apply the centering flexbox layout */}
      <AddRequestForm
        subject_name={subject_name}
        available_slots={teacher?.available_slots}
        subject_id={subject_id}
        teacher_id={teacher_id}
        teacher_email={teacher?.email}
      />
      <TeacherCard
        image_url={teacher.image_url}
        name={teacher.name}
        level_of_knowledge={level_of_knowledge}
        hourly_rate={hourly_rate}
        email={teacher.email}
      />
    </div>
  );
};

export default TutoringForm;
