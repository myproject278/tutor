import React, { useEffect, useState } from "react";
import { format, addDays } from "date-fns"; // Import date-fns library for date operations
import {
  TextField,
  Checkbox,
  FormControlLabel,
  Button,
  p,
  Typography,
} from "@mui/material";
import "./add_request_style.css";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import { add_request } from "../../actions/request_action";
import { useNavigate } from "react-router-dom";
import { send_email } from "../../actions/mail_actions";
import { useDispatch, useSelector } from "react-redux";
import { add_notification } from "../../actions/notification_action";

const get_time_am_pm = (i) => {
  if ((i + 1) % 12 == 0) return `12 ${i == 0 ? "Pm" : "Am"}`;
  else return `${(i + 1) % 12} ${i / 12 > 0 ? "Pm" : "Am"}`;
};
const time_from_arr = (arr = []) => {
  const res = [];
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] == 1) {
      res.push(get_time_am_pm(i));
    }
  }
  return res;
};
const AddRequestForm = ({
  subject_name,
  available_slots,
  teacher_id,
  teacher_email,
  subject_id,
}) => {
  const [formData, setFormData] = useState({
    goal_of_tutoring: "",
    total_hours: 1,
    timeSlots: {},
  });
  const { user } = useSelector((state) => state.authReducer);

  const [total_slots, setTotalSlots] = useState(0);
  let hours_arr = {};
  if (available_slots) hours_arr = JSON.parse(available_slots);
  const [selectedDay, setSelectedDay] = useState(null);
  const [selectedHour, setSelectedHour] = useState(null);
  const [showHoursDiv, setShowHoursDiv] = useState(false);
  const [selectedArrowRow, setSelectedArrowRow] = useState(undefined); // Set to undefined initially
  const today = new Date();
  const [dates, setDates] = useState([]);
  const week_days = [
    "sunday",
    "monday",
    "tuesday",
    "wednesday",
    "thursday",
    "friday",
    "saturday",
  ];

  useEffect(() => {
    const tempdates = [];
    today.setHours(0, 0, 0, 0);
    for (let i = 1; i <= 7; i++) {
      const da = addDays(today, i);
      tempdates.push(da);
    }
    setDates(tempdates);
  }, []);
  const get_time = (hour) => {
    return parseInt(hour.split(" ")[0]);
  };
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const handleSubmit = async (e) => {
    e.preventDefault();
    const slots_for_date = [];
    Object.entries(formData.timeSlots).forEach(([d, selected_hours]) => {
      var date = new Date(d);
      selected_hours.forEach((h) => {
        let time = 0;
        if (h.includes("Pm")) {
          const val = get_time(h);
          time = val == 12 ? 12 : 12 + val;
        } else {
          const val = get_time(h);
          time = val == 12 ? 0 : val;
        }

        date.setHours(time);
        slots_for_date.push(date);
      });
    });

    try {
      const data = await add_request({
        ...formData,
        timeSlots: slots_for_date,
        teacher_id: teacher_id,
        subject_id: subject_id,
      });

      if (data && data.status == "success") {
        if (teacher_email) {
          await send_email({
            to: teacher_email,
            subject: "New Tutoring Request",
            message: `${user?.name} request for teaching ${subject_name}`,
          });
        }

        const notification = {
          message: `${user?.name} request for teaching ${subject_name}`,
          redirect_link: `/requests/${data.data.insertId}`,
        };
        dispatch(add_notification(notification));
        navigate("/requests");
      }
    } catch (err) {
      console.log("An error occurred while adding the meeting.");
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleTimeSlotSelect = (date, hour) => {
    const newSlot = { ...formData.timeSlots };
    if (!newSlot[date]) {
      if (total_slots == 3) return;
      setTotalSlots((prev) => prev + 1);
      newSlot[date] = [hour];
    } else if (newSlot[date].includes(hour)) {
      setTotalSlots((prev) => prev - 1);
      newSlot[date] = newSlot[date].filter((slot) => slot !== hour);
      if (newSlot[date].length == 0) {
        delete newSlot[date];
      }
      setSelectedHour(null);
    } else {
      if (total_slots == 3) {
        return;
      }

      setTotalSlots((prev) => prev + 1);
      newSlot[date].push(hour);
      setSelectedHour(hour);
    }
    setFormData({ ...formData, timeSlots: newSlot });
  };

  const toggleArrowDirectionForRow = (date) => {
    setSelectedArrowRow((prevSelectedArrowRow) =>
      prevSelectedArrowRow === date ? undefined : date
    );
  };

  return (
    <div className="form-outer-div">
      <Typography
        className="typorgraph-request-form"
        variant="h5"
        fullWidth
        textAlign="center"
      >
        Tell us More
      </Typography>
      <hr />
      <form onSubmit={handleSubmit} className="form-outer-div2">
        <div>
          <p className="typorgraph-request-form">
            Tell Something about your goals for tutoring
          </p>
          <TextField
            label="goals for tutoring"
            variant="outlined"
            name="goal_of_tutoring"
            fullWidth
            multiline
            rows={3}
            rowsMax={3}
            value={formData.goals}
            onChange={handleChange}
            required
          />
        </div>
        <div className="talbe-form-request">
          <TableContainer component={Paper} className="table-container1">
            <Table>
              <TableBody>
                <TableRow>
                  <TableCell component="th" scope="row">
                    <p className="typorgraph-request-form">Subject : </p>
                  </TableCell>
                  <TableCell>
                    {" "}
                    <p className="typorgraph-request-form">{subject_name}</p>
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell component="th" scope="row">
                    <p className="typorgraph-request-form">Lesson Type : </p>
                  </TableCell>
                  <TableCell>
                    {" "}
                    <p className="typorgraph-request-form">Online</p>
                  </TableCell>
                </TableRow>

                <TableRow>
                  <TableCell component="th" scope="row">
                    <p className="typorgraph-request-form">No of hours : </p>
                  </TableCell>
                  <TableCell>
                    {" "}
                    <p className="typorgraph-request-form">
                      {" "}
                      <TextField
                        type="number"
                        name="total_hours"
                        variant="standard"
                        value={formData.total_hours}
                        onChange={(e) => {
                          if (e.target.value >= 0) handleChange(e);
                        }}
                      />
                    </p>
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
          <div>
            <p
              className="typorgraph-request-form"
              fontSize="18px"
              color="#5f5959"
            >
              When would you like to meet? Request up to three times that work
              for you:
            </p>
            <div style={{ display: "flex", justifyContent: "flex-end" }}>
              <p className="typorgraph-request-form">
                {total_slots} Slots selected
              </p>
            </div>
            <div className="outer-div-table-requrest">
              <TableContainer component={Paper} style={{ margin: "20px 0" }}>
                <Table className="table-container2" aria-label="simple table">
                  <TableBody>
                    <TableRow>
                      <TableCell component="th" scope="row">
                        <p className="typorgraph-request-form">today</p>
                      </TableCell>
                      <TableCell>
                        <p className="typorgraph-request-form">
                          ask tutor for same day availability
                        </p>
                      </TableCell>
                    </TableRow>

                    {dates.map((date, index) => (
                      <TableRow
                        key={index}
                        sx={{
                          "&:last-child td, &:last-child th": { border: 0 },
                          backgroundColor:
                            selectedArrowRow === date
                              ? "#d4d4d4"
                              : "transparent",
                        }}
                      >
                        <TableCell component="td" scope="row">
                          <p
                            className="typorgraph-request-form"
                            style={{ color: "black", cursor: "pointer" }}
                            onClick={() => {
                              setSelectedDay((prevSelectedDay) =>
                                prevSelectedDay === date ? null : date
                              );
                              toggleArrowDirectionForRow(date);
                            }}
                          >
                            {format(date, "EEE, MMM dd, yyyy")}
                          </p>
                        </TableCell>
                        <TableCell>
                          <div style={{ position: "relative" }}>
                            <p
                              className="typorgraph-request-form"
                              style={{ cursor: "pointer" }}
                              onClick={() => {
                                setSelectedDay((prevSelectedDay) =>
                                  prevSelectedDay === date ? null : date
                                );
                                toggleArrowDirectionForRow(date);
                              }}
                            >
                              midnight - midnight{" "}
                              {selectedArrowRow === date ? (
                                <KeyboardArrowUpIcon />
                              ) : (
                                <KeyboardArrowDownIcon />
                              )}
                            </p>
                            {selectedDay === date && (
                              <div
                                style={{
                                  border: "1px solid #ccc",
                                  padding: "10px",
                                  position: "absolute",
                                  background: "rgb(172 219 255)",
                                  zIndex: 5,
                                  left: "40%",
                                  top: "100%",
                                  transform: "translate(-50%, 0)",
                                  display: "flex",
                                  width: "max-content",
                                  flexWrap: "wrap", // Add this style to wrap content to the next line
                                }}
                              >
                                <div
                                  style={{
                                    display: "grid",
                                    gridTemplateColumns: "repeat(5,1fr)",
                                  }}
                                >
                                  {time_from_arr(
                                    hours_arr[week_days[selectedDay?.getDay()]]
                                  )?.map((hour, index) => {
                                    const isChecked =
                                      formData.timeSlots[date] &&
                                      formData.timeSlots[date].includes(hour);
                                    return (
                                      <FormControlLabel
                                        key={hour}
                                        control={
                                          <Checkbox
                                            checked={isChecked}
                                            onChange={() =>
                                              handleTimeSlotSelect(date, hour)
                                            }
                                            disabled={
                                              !isChecked && total_slots === 3
                                            }
                                          />
                                        }
                                        label={hour}
                                        style={{
                                          display: "block",
                                          marginTop: "10px",
                                        }}
                                      />
                                    );
                                  })}
                                </div>
                              </div>
                            )}
                          </div>
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </div>
          </div>
        </div>
        <div
          style={{
            marginTop: "20px",
            border: "1px solid #ccc",
            padding: "10px",
          }}
        >
          <p className="typorgraph-request-form" variant="subtitle1">
            Selected Hours:
          </p>
          {Object.entries(formData.timeSlots).map(([date, selectedHours]) => (
            <div key={date}>
              <p className="typorgraph-request-form">
                {format(new Date(date), "EEE, MMM dd, yyyy")}:
              </p>
              {selectedHours.map((hour) => (
                <p className="typorgraph-request-form" key={hour}>
                  {hour}
                </p>
              ))}
            </div>
          ))}
        </div>
        <Button
          style={{ marginTop: "30px" }}
          variant="contained"
          type="submit"
          color="primary"
          fullWidth
        >
          Submit
        </Button>
      </form>
    </div>
  );
};

export default AddRequestForm;
