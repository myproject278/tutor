import React from 'react'
import {Grid,IconButton,InputAdornment,TextField,} from '@material-ui/core'
import  Visibility from '@material-ui/icons/Visibility'
import VisibilityOff from '@material-ui/icons/VisibilityOff'


const  Input = ({name,handlechange,half,label,autoFocus,type,handleShowPassword})=> {
  return (
       <Grid item xs={12} sm={half?6:12}>
           <TextField
               name={name}
               onChange={handlechange}
               label={label}
               autoFocus={autoFocus}
               type={type}
               variant='outlined'
               required
               fullWidth
               InputProps={name=='password' ?
                 {
                     endAdornment:(  
                         <InputAdornment position='end'>
                             <IconButton onClick={handleShowPassword}>
                                 {type=='password' ?<Visibility/>:<VisibilityOff/>}
                             </IconButton>
                         </InputAdornment>
                     )
                 }:null
            }
           />
       </Grid>
  )
};

export default Input



