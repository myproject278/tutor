import React, { useState,useEffect } from "react";
import { Button, Grid, Avatar, Container, Paper, Typography, TextField } from "@material-ui/core";
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import Input from "./Input";
import style from './style'
import { useSnackbar } from 'notistack';
import { useDispatch, useSelector } from "react-redux";
import { signIn, signUp } from "../../actions/auth_action";
import { useLocation, useNavigate } from "react-router-dom";
import authImg from '../../images/authimg.png'


const Auth = () => {
    const classes = style();
    const [isSignIn, setIsSignIn] = useState(true);
    const [showPassword, setShowPassword] = useState(false);
    const [checkPasswod, setCheckPassword] = useState(false);
    const [image , setImage]=useState()

    const [user, setUser] = useState({
        name: '', mobile_no: '', email: '', password: '', confirmPassword: ''
    });

    const navigate = useNavigate();
    const location = useLocation();
    const { enqueueSnackbar } = useSnackbar();
    const { authenticate, loading, error, path } = useSelector(state => state.authReducer);
    const dispatch = useDispatch();

    const auth = useSelector((state) => state.authReducer);
    useEffect(() => {
      if (error) {
        enqueueSnackbar(error, {
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        });
      }
      if (authenticate) {
        if (path) navigate(path);
        else navigate("/");
      }
    }, [authenticate, error, auth]);

    const handleImageChange = (e) => {
      const file = e.target.files[0];
      setImage(file);
    };

    const submitHandler = async (e) => {
      e.preventDefault();
      if (isSignIn) {
        dispatch(signIn({ email: user.email, password: user.password }));
      } else {
        if (user.password !== user.confirmPassword) {
          setCheckPassword(true);
        } else {
          const form = new FormData();
          {
            Object.entries(user).map(([key, value]) => form.set(key, value));
          }
          try {
            form.set("images", image);

            const res = await signUp(form);
            if (res.status == "success") {
              enqueueSnackbar(res.data, {
                variant: "success",
                anchorOrigin: {
                  vertical: "top",
                  horizontal: "right",
                },
              });
              switchMod();
            } else {
              enqueueSnackbar(res.error?.sqlMessage, {
                variant: "error",
                anchorOrigin: {
                  vertical: "top",
                  horizontal: "right",
                },
              });
            }
          } catch (err) {
            enqueueSnackbar(err, {
              variant: "error",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            });
          }
        }
      }
    };

    const changeHandler = (e) => {
        setUser({ ...user, [e.target.name]: e.target.value });
    }

    const handleShowPassword = () => {
        setShowPassword(prev => !prev);
    }

    const switchMod = () => {
        setIsSignIn(prev => !prev);
    }

    return (
        <Container >
            <Paper className={classes.paper} elevation={3}>
                <Grid container spacing={3}>
                    <Grid item xs={12} sm={6}>
                        {/* Add your image of a student studying here */}
                        <img src={authImg} alt="Student Studying"  className={classes.studentImage} />
                    </Grid>
                    <Grid item xs={12} sm={6} style={{textAlign:'center'}}>
                        <div style={{    display: 'flex',
    justifyContent: 'center'}}>
                        <Avatar className={classes.avatar}>
                            <LockOutlinedIcon />
                            </Avatar>
                            </div>
                        <Typography variant="h5">{isSignIn ? 'Sign In' : 'Sign Up'}</Typography>
                        <form onSubmit={submitHandler} className={classes.form}>
                            <Grid container spacing={3}>
                                {
                                    auth.error && (
                                        <Grid item xs={12}>
                                            <Typography className={classes.errormsg} variant="h6" align="center">{auth.error}</Typography>
                                        </Grid>
                                    )
                                }
                                {
                                    !isSignIn &&
                                    <>
                                        <Grid item xs={12}>
                                            <Input name='name' type="text" label='Name' handlechange={changeHandler} autoFocus />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <Input name='mobile_no' type="text"  label='Mobile No' handlechange={changeHandler} />
                                        </Grid>
                                    </>
                                }
                                <Grid item xs={12}>
                                    <Input name='email' type="email" label='Email'  handlechange={changeHandler} />
                                </Grid>
                                <Grid item xs={12}>
                                    <Input name='password' type={showPassword ? 'text' : 'password'} label='Password' handlechange={changeHandler} handleShowPassword={handleShowPassword} />
                                </Grid>
                                {
                                    !isSignIn &&
                                    <Grid item xs={12}>
                                        <Input name='confirmPassword' type='password' label={checkPasswod ? 'Password Not Match ReEnter' : 'Confirm Password'} handlechange={changeHandler} />
                                    </Grid>
                                }
                                {
                          !isSignIn &&   <div className={classes.fileInput}>  
                             <Typography  align="center" >Profile_pic</Typography>
                            <input type="file" accept="image/*" onChange={handleImageChange} />

</div>
                      
                       }
                                <Grid item xs={12}>
                                    <Button className={classes.submit} type='submit' variant="contained" fullWidth color="primary">{isSignIn ? 'Sign in' : 'Sign Up'}</Button>
                                </Grid>
                                <Grid item xs={12}>
                                    <Button onClick={switchMod} fullWidth>
                                        {isSignIn ? `Don't Have an Account? Sign Up` : `Already Have an Account? Sign In`}
                                    </Button>
                                </Grid>
                            </Grid>
                        </form>
                    </Grid>
                </Grid>
            </Paper>
        </Container>
    )
}

export default Auth;
