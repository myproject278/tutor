import React, { useEffect, useState } from "react";
import {
  Badge,
  IconButton,
  Tooltip,
  Typography,
  List,
  ListItem,
  ListItemText,
  Button,
} from "@material-ui/core";
import { useSelector, useDispatch } from "react-redux";
import Popover from "@material-ui/core/Popover";
import { Box, useTheme } from "@mui/material";
import NotificationsOutlinedIcon from "@mui/icons-material/NotificationsOutlined";
import {
  get_notifications,
  handle_notification_read,
} from "../../actions/notification_action";
import { makeStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  profileDropdown: {
    position: "relative",
    cursor: "pointer",
    
  },
  modalContent: {
    backgroundColor: "white",
    color: "black",
    padding: "20px",
    maxWidth: "400px",
    margin: "0 auto",
    borderRadius: "10px",
    outline: "none",
    boxShadow: "0px 4px 6px rgba(0, 0, 0, 0.1)",
  },
  unreadItem: {
    borderLeft: "3px solid #ff9800",
    margin: "10px 0",
    padding: "10px",
    backgroundColor: "#f7f7f7",
    borderRadius: "5px",
    display: "flex",
    alignItems: "center",
    "&:hover": {
      backgroundColor: "#fff",
    },
  },
  readItem: {
    borderLeft: "3px solid #4caf50",
    margin: "10px 0",
    padding: "10px",
    backgroundColor: "#f0f0f0",
    borderRadius: "5px",
    display: "flex",
    alignItems: "center",
    "&:hover": {
      backgroundColor: "#fff",
    },
  },
  listItemText: {
    flex: 1,
    marginLeft: "10px",
  },
}));

const NotificationComponent = () => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState(null);

  const [open, setOpen] = useState(false);
  const unread_count = useSelector(
    (state) => state.notificationReducer.unread_count
  );
  const dispatch = useDispatch();
  useEffect(() => {
    if (open) dispatch(get_notifications());
  }, [dispatch, open]);

  const notificationList = useSelector(
    (state) => state.notificationReducer.notifications
  );

  function handleNotificationClick(id) {
    // Your code for handling notification clicks
  }

  const handleClose = () => {
    setOpen(false);
  };

  const notification_click_handler = (is_read, notification_id) => {
    if (is_read === 0) dispatch(handle_notification_read({ notification_id }));
    setOpen(false);
  };

  return (
    <div className={classes.profileDropdown}>
      {/* Wrap the IconButton with a Tooltip */}
      <Tooltip title="Notifications" placement="top">
        <IconButton
          onClick={(event) => {
            console.log("in bu");
            setAnchorEl(event.currentTarget); // Set anchorEl to the clicked IconButton
            setOpen(true);
          }}
        >
          <Badge badgeContent={unread_count} color="error">
            <NotificationsOutlinedIcon fontSize="medium" />
          </Badge>
        </IconButton>
      </Tooltip>
      <Popover
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "right",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
      >
        <div className={classes.modalContent}>
          {notificationList.length > 0 ? (
            <List>
              {notificationList.map((notification) => (
                <ListItem
                  key={notification.id}
                  className={
                    notification.is_read ? classes.unreadItem : classes.readItem
                  }
                  onClick={() => handleNotificationClick(notification.id)}
                >
                  <Badge
                    variant="dot"
                    color="secondary"
                    invisible={!notification.isUnread}
                  >
                    <div className={classes.listItemText}>
                      <Link
                        to={notification.redirect_link}
                        onClick={() =>
                          notification_click_handler(
                            notification.is_read,
                            notification.id
                          )
                        }
                      >
                        {notification.message}
                      </Link>
                    </div>
                  </Badge>
                </ListItem>
              ))}
            </List>
          ) : (
            <Typography>No new notifications.</Typography>
          )}
          <Button onClick={handleClose}>Close</Button>
        </div>
      </Popover>
    </div>
  );
};

export default NotificationComponent;
