import React, { useEffect, useState } from "react";
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction";
import listPlugin from "@fullcalendar/list";
import "./style.css";
import {
  Box,
  List,
  ListItem,
  ListItemText,
  Typography,
  useTheme,
} from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { formatDate } from "../../utils/functions";
import { get_request_for_student_calender } from "../../actions/request_action";
import Back from "../common/back/Back";
import Heading from "../common/heading/Heading";

const Calendar = () => {
  const theme = useTheme();
  const requests = useSelector(
    (state) => state.requestReducer.reqeusts_for_calender
  );
  const [currentEvents, setCurrentEvents] = useState([]);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(get_request_for_student_calender());
  }, []);

  const handleDateClick = (selected) => {
    const title = prompt("Please enter a new title for your event");
    const calendarApi = selected.view.calendar;
    calendarApi.unselect();

    if (title) {
      calendarApi.addEvent({
        id: `${selected.dateStr}-${title}`,
        title,
        start: selected.startStr,
        end: selected.endStr,
        allDay: selected.allDay,
        approve_time: new Date(), // Add approve_time property
        total_hour: 2, // Add total_hour property
      });
    }
  };

  const handleEventClick = (selected) => {
    if (
      window.confirm(
        `Are you sure you want to delete the event '${selected.event.title}'`
      )
    ) {
      selected.event.remove();
    }
  };

  return (
    <>
      <Heading subtitle="Your Sessions" title="Calender" />

      <Box className="calernder-box">
        <Box
          display="flex"
          justifyContent="space-between"
          className="outer-div-all"
        >
          {/* CALENDAR SIDEBAR */}
          <Box
            flex="1 1 20%"
            backgroundColor="#e3dede"
            p="15px"
            borderRadius="4px"
            className="event-outer"
          >
            <Typography variant="h5">Events</Typography>
            <List>
              {currentEvents.map((event) => (
                <ListItem
                  key={event.id}
                  sx={{
                    backgroundColor: "#a5daa4",
                    margin: "10px 0",
                    borderRadius: "2px",
                  }}
                >
                  <ListItemText
                    primary={event.title}
                    secondary={
                      <Typography>{formatDate(event.start)}</Typography>
                    }
                  />
                </ListItem>
              ))}
            </List>
          </Box>

          {/* CALENDAR */}
          <Box flex="1 1 100%" className="cl-1-outer">
            <FullCalendar
              height="75vh"
              plugins={[
                dayGridPlugin,
                timeGridPlugin,
                interactionPlugin,
                listPlugin,
              ]}
              headerToolbar={{
                left: "prev,next today",
                center: "title",
                right: "dayGridMonth,timeGridWeek,timeGridDay,listMonth",
              }}
              initialView="dayGridMonth"
              editable={true}
              selectable={true}
              selectMirror={true}
              dayMaxEvents={true}
              select={handleDateClick}
              eventClick={handleEventClick}
              eventsSet={(events) => setCurrentEvents(events)}
              initialEvents={requests.map((r) => {
                let d = new Date(r.approve_time);
                d.setHours(d.getHours() + r.total_hours);
                return {
                  id: r.id,
                  title: `${r.name} ( ${r.subject_name} )`,
                  start: new Date(r.approve_time),
                  end: d,
                };
              })}
            />
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default Calendar;
