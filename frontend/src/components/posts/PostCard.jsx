import React from "react";
import { format } from "date-fns"; // Only import the 'format' function
import ImageCard from "./ImageCard";
import { useNavigate } from "react-router-dom";

const PostCard = ({ posts }) => {
  const navigate = useNavigate();
  return (
    <>
      {posts.map(
        ({ date, description, header, id, image_urls, name, teacher_id }) => (
          <div
            className="items shadow"
            key={id}
            onClick={() => navigate(`/posts/${id}`)}
            style={{ cursor: "pointer" }}
          >
            <ImageCard image_urls={image_urls} />
            <div className="text">
              <div className="admin flexSB">
                <span>
                  <i className="fa fa-user"></i>
                  {name}
                </span>
                <span>
                  <i className="fa fa-calendar-alt"></i>
                  {/* Format the date */}
                  {format(new Date(date), "EEE, MMM dd, yyyy")}
                </span>
              </div>
              <h1>{header}</h1>
              <p>{description}</p>
            </div>
          </div>
        )
      )}
    </>
  );
};

export default PostCard;
