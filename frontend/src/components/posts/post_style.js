import { makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) => ({
  appBarSearch: {
    borderRadius: 4,
    marginBottom: "1rem",
    display: "flex",
    padding: "16px",
    width: "90%",
    boxShadow: "none",
    [theme.breakpoints.down("xs")]: {
      width: "100%",
    },
  },
  current_position_style: {
    [theme.breakpoints.down("xs")]: {
      fontSize: "12px",
    },
  },
  related_post: {
    display: "flex",
    overflowX: "scroll",
    [theme.breakpoints.down("xs")]: {
      flexDirection: "column",
    },
  },
  pagination: {
    borderRadius: 4,
    marginTop: "1rem",
    padding: "16px",
  },
  gridContainer: {
    // [theme.breakpoints.down('xs')]: {
    //   flexDirection: 'column-reverse',
    // },
  },
}));
