import React, { useEffect, useState } from "react";
import Back from "../common/back/Back";
import PostCard from "./PostCard";
import "./blog.css";

import {
  Container,
  Grid,
  Grow,
  Paper,
  AppBar,
  TextField,
  Button,
} from "@material-ui/core";

import { useDispatch, useSelector } from "react-redux";
import { get_posts, get_posts_by_search_tage } from "../../actions/post_action";
import { useNavigate } from "react-router-dom";
import useStyle from "./post_style";
import ChipInput from "material-ui-chip-input";
import InputModel from "../common/InputModel/Index";
const Posts = () => {
  const classes = useStyle();
  const [search, setSearch] = useState("");
  const [tags, setTages] = useState([]);
  const [open, setOpen] = useState(false);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const posts = useSelector((state) => state.postReducer.posts);
  useEffect(() => {
    dispatch(get_posts());
  }, []);
  const handleKeyPress = (e) => {
    if (e.key == "Enter") searchPost();
  };
  const addTag = (tag) => {
    setTages([...tags, tag]);
  };
  const deleteTag = (tag) => {
    setTages(tags.filter((t) => t != tag));
  };
  const searchPost = () => {
    if (search || tags) {
      dispatch(get_posts_by_search_tage({ search, tags: tags.join("|") }));
      setOpen(false);
    }
  };

  return (
    <>
      <Back title="Blog Posts" style={{ height: "30vh" }} />
      <section className="blog padding" style={{ marginTop: "5%" }}>
        <div
          className="container flex"
          style={{ justifyContent: "flex-end", marginBottom: "20px" }}
        >
          <Button onClick={() => setOpen(true)} variant="contained">
            Search Post
          </Button>
        </div>
        <div className="container grid2">
          <PostCard posts={posts} />
        </div>
      </section>
      <InputModel open={open} setOpen={setOpen}>
        <AppBar
          className={classes.appBarSearch}
          position="static"
          color="inherit"
        >
          <TextField
            name="search"
            variant="outlined"
            label="Search Memories"
            fullWidth
            onKeyPress={handleKeyPress}
            value={search}
            onChange={(e) => setSearch(e.target.value)}
          />
          <ChipInput
            style={{ margin: "10px 0" }}
            value={tags}
            label="Search Tags"
            variant="outlined"
            onAdd={addTag}
            onDelete={deleteTag}
          />
          <Button onClick={searchPost} variant="outlined" color="primary">
            Search
          </Button>
        </AppBar>
      </InputModel>
    </>
  );
};

export default Posts;
