import { get_image_url } from "../../../helper/util";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { format } from "date-fns"; // Only import the 'format' function
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import {
  Typography,
  Divider,
  Paper,
  CircularProgress,
  Grid,
} from "@material-ui/core";
import useStyle from "./style";
import { useNavigate, useParams } from "react-router-dom";

import Comments from "./Comments";
import {
  get_posts_by_search_tage,
  get_single_post,
} from "../../../actions/post_action";
import ImageCard from "../ImageCard";
import { Avatar } from "@mui/material";
import Header from "../../common/header/Header";
import Head from "../../common/header/Head";

const PostDetails = () => {
  const [rc_post, setRCPosts] = useState([]);
  const classes = useStyle();
  const { post_id } = useParams();
  const dispatch = useDispatch();
  const post = useSelector((state) => state.postReducer.post_details);
  const posts = useSelector((state) => state.postReducer.posts);
  const navigate = useNavigate();
  console.log(rc_post);
  useEffect(() => {
    setRCPosts(posts.filter((rp) => rp.id != post_id));
  }, [post_id, posts]);
  useEffect(() => {
    dispatch(get_single_post(post_id));
  }, [post_id]);
  useEffect(() => {
    if (post?.tags) {
      const t = JSON.parse(post?.tags).join("|");
      dispatch(get_posts_by_search_tage({ tags: t }));
    }
  }, [post?.tags]);
  return (
    <>
      <div style={{ padding: "20px", borderRadius: "15px" }} elevation={6}>
        <div className={classes.card}>
          <Grid container spacing={3} alignItems="stretch">
            <Grid item lg={6}>
              <div className={classes.section}>
                <div className="flex">
                  <Avatar
                    src={get_image_url(post.teacher_image)}
                    style={{ width: "100px", height: "100px" }}
                  />
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      alignItems: "center", // This centers content horizontally
                      justifyContent: "center", // This centers content vertically
                      marginLeft: "31px",
                      color: "#5a5d5f",
                    }}
                  >
                    <Typography>{post.name}</Typography>
                    <Typography className={classes.current_position_style}>
                      {post.current_position}
                    </Typography>
                  </div>
                </div>
                <Typography
                  variant="h3"
                  component="h2"
                  className={classes.post_header}
                >
                  {post.header}
                </Typography>
                <Typography gutterBottom color="textSecondary" component="h5">
                  tags :&nbsp;
                  {post?.tags &&
                    JSON.parse(post?.tags).map((tag) => `#${tag} `)}
                </Typography>
                <Typography
                  gutterBottom
                  variant="body1"
                  component="p"
                  style={{ marginTop: "20px" }}
                >
                  {post.description}
                </Typography>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "flex-end",
                    marginTop: "30px",
                  }}
                >
                  {post.date && (
                    <Typography style={{ color: "#6a6c6d" }}>
                      Created at:{" "}
                      {format(new Date(post.date), "EEE, MMM dd, yyyy")}
                    </Typography>
                  )}
                </div>

                <Divider style={{ margin: "20px 0" }} />
                <Comments post_id={post.id} />
                <Divider style={{ margin: "20px 0" }} />
              </div>
            </Grid>
            <Grid item lg={6}>
              <div className={classes.imageSection}>
                <ImageCard image_urls={post.image_urls} />
              </div>
            </Grid>
          </Grid>
        </div>

        {rc_post.length && (
          <div className={classes.section}>
            <Typography gutterBottom variant="h5">
              You might also like:
            </Typography>
            <Divider />
            <div className={classes.recommendedPosts}>
              {rc_post.map(
                ({ header, description, name, image_urls, id }, index) => (
                  <div
                    style={{
                      minWidth: "22%",
                      maxWidth: "326px",
                      margin: "15px",
                      cursor: "pointer",
                      padding: "12px",
                      border: "1px solid #e3e2e2",
                      borderRadius: "10px",
                    }}
                    key={id}
                    onClick={() => navigate(`/posts/${id}`)}
                  >
                    <Typography gutterBottom variant="h6">
                      {header}
                    </Typography>
                    <Typography gutterBottom variant="subtitle2">
                      by : {name}
                    </Typography>
                    <Typography
                      gutterBottom
                      variant="subtitle2"
                      style={{ color: "#7e7f7f" }}
                    >
                      {description}
                    </Typography>
                    {/* Assuming ImageCard is your custom component for rendering images */}
                    <ImageCard image_urls={image_urls} />
                  </div>
                )
              )}
            </div>
          </div>
        )}
      </div>
    </>
  );
};

export default PostDetails;
