import { Avatar, Button, TextField, Typography } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import useStyle from "./style";
import {
  add_comment,
  get_comments_by_post,
} from "../../../actions/comment_actions";
import SendIcon from "@mui/icons-material/Send";
import { get_image_url } from "../../../helper/util";

const Comments = ({ post_id }) => {
  const [comment, setComment] = useState("");
  const classes = useStyle();
  const dispatch = useDispatch();
  const comments = useSelector((state) => state.postReducer.comments);

  useEffect(() => {
    dispatch(get_comments_by_post(post_id));
  }, [post_id]);
  const commentHandler = () => {
    dispatch(add_comment({ post_id, comment }));
    setComment("");
  };

  return (
    <div>
      <div className={classes.commentsOuterContainer}>
        <div className={classes.commmentsInnterContainer}>
          <Typography gutterBottom variant="h6" color="textPrimary">
            Comments
          </Typography>
          {comments.map((c, i) => (
            <div key={i} className="flex" style={{ marginTop: "10px" }}>
              <Avatar
                src={get_image_url(c.image_url)}
                style={{ width: "60px", height: "60px" }}
              />
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "center", // This centers content vertically
                  marginLeft: "31px",
                }}
              >
                <Typography>{c.name}</Typography>
                <Typography style={{ color: "#5a5d5f" }}>
                  {c.comment}
                </Typography>
              </div>
            </div>
          ))}
        </div>

        <div>
          <Typography gutterBottom variant="h6">
            Write a Commnt
          </Typography>
          <div className="flex">
            <TextField
              fullWidth
              variant="standard"
              label="Comment"
              multiline
              value={comment}
              onChange={(e) => setComment(e.target.value)}
            />
            <Button
              onClick={commentHandler}
              variant="contained"
              color="primary"
            >
              <SendIcon />
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Comments;
