import React, { useEffect } from "react";
import Heading from "../common/heading/Heading";
import "../subjects/courses.css";
import SubjectHome from "../subjects/SubjectHome";
import { useDispatch, useSelector } from "react-redux";
import { getSubjects, getTopSubjects } from "../../actions/subject_action";
import SubjectCard from "../subjects/SubjectCard";

const HAbout = () => {
  const dispatch = useDispatch();
  const subjects = useSelector((state) => state.subjectReducer.subjects);
  const auth = useSelector((state) => state.authReducer);
  useEffect(() => {
    dispatch(getTopSubjects());
  }, []);
  return (
    <>
      <section className="homeAbout">
        <div className="container"></div>
        <SubjectCard subjects={subjects} />
      </section>
    </>
  );
};

export default HAbout;
