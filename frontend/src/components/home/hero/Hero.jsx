import React from "react"
import Heading from "../../common/heading/Heading"
import "./Hero.css"
import { useNavigate } from "react-router-dom";

const Hero = () => {
  const navigate = useNavigate();
  return (
    <>
      <section className="hero">
        <div className="container">
          <div className="row">
            <Heading
              subtitle="WELCOME TO SANGAM"
              title="Best Online Education Expertise"
            />
            <p className="hero_text">
              LEARN WITH EXPERTIES ON HOURLY BASIC AND LEARN WHAT YOU NEEDED.
            </p>
          </div>
        </div>
      </section>
      <div className="hero-margin" />
    </>
  );
};

export default Hero
