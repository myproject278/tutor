import React, { useEffect } from "react";
import { testimonal } from "../../../dummydata";
import Heading from "../../common/heading/Heading";
import "./style.css";
import { useDispatch, useSelector } from "react-redux";
import { get_top_teacher } from "../../../actions/teacher_action";
import { get_image_url } from "../../../helper/util";
import { useNavigate } from "react-router-dom";

const TopTeacher = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const teacher_list = useSelector(
    (state) => state.teacherReducer.teacher_list
  );
  console.log(teacher_list);
  useEffect(() => {
    dispatch(get_top_teacher());
  }, []);
  return (
    <>
      <section className="testimonal padding">
        <div className="container">
          <Heading subtitle="Teachers" title="Our Successful teachers" />

          <div className="content grid2">
            {teacher_list.map(
              ({ id, name, current_position, image_url, bio }) => (
                <div
                  className="items shadow"
                  onClick={() => navigate(`/teachers/${id}`)}
                >
                  <div className="box flex">
                    <div className="img">
                      <img src={get_image_url(image_url)} alt="" />
                      <i className="fa fa-quote-left icon"></i>
                    </div>
                    <div className="name">
                      <h2>{name}</h2>
                      <span>{current_position}</span>
                    </div>
                  </div>
                  <p>{bio}</p>
                </div>
              )
            )}
          </div>
        </div>
      </section>
    </>
  );
};

export default TopTeacher;
