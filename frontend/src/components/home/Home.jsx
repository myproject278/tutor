import React from "react"
import HPosts from "./HPosts";
import HAbout from "./HAbout"
import Hero from "./hero/Hero";
import TopTeacher from "./testimonal/TopTeacher";

const Home = () => {
  return (
    <>
      <Hero />

      <HAbout />
      <TopTeacher />
      <HPosts />
    </>
  );
};

export default Home
