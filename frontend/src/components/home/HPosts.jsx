import React, { useEffect } from "react";
import "../posts/blog.css";
import Heading from "../common/heading/Heading";
import { useDispatch, useSelector } from "react-redux";
import { get_recent_and_famous_posts } from "../../actions/post_action";
import PostCard from "../posts/PostCard";

// copy code of blog => blogCard

const HPosts = () => {
  const dispatch = useDispatch();
  const posts = useSelector((state) => state.postReducer.posts);
  useEffect(() => {
    dispatch(get_recent_and_famous_posts());
  }, []);
  return (
    <>
      <section className="blog">
        <div className="container">
          <Heading subtitle="OUR Posts" title="Recent From Posts" />
          <div className="grid2">
            <PostCard posts={posts} />
          </div>
        </div>
      </section>
    </>
  );
};

export default HPosts;
