import React, { useEffect, useState } from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { CardActionArea } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { useParams, useNavigate } from "react-router-dom";
import { get_image_url } from "../../helper/util";
import Heading from "../common/heading/Heading";
import "../subjects/courses.css";
import { getSubjects } from "../../actions/subject_action";
import "./styles/styles.scss";

const Index = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const subjects = useSelector((state) => state.subjectReducer.subjects);
  useEffect(() => {
    dispatch(getSubjects(0));
  }, []);

  const click_subject = (s) => {
    navigate(`/quiz/instructions/${s.id}`);
  };
  return (
    <>
      <section className="online">
        <div className="container">
          <Heading subtitle="Quiz" title="select subject for Quiz" />
          <div className="content grid3">
            {subjects.map((s) => (
              <div className="box" onClick={() => click_subject(s)}>
                <div className="img">
                  <img src={get_image_url(s.image_url)} />
                </div>
                <h1>{s.name}</h1>
              </div>
            ))}
          </div>
        </div>
      </section>
    </>
  );
};

export default Index;
