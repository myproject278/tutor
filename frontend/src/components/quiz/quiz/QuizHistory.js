import * as React from "react";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import { useDispatch, useSelector } from "react-redux";
import { get_quiz_history } from "../../../actions/quiz_action";
import Header from "../../common/header/Header";
import Heading from "../../common/heading/Heading";
import { Typography } from "@mui/material";

const columns = [
  { id: "name", label: "Subject Name", minWidth: 100, align: "center" },
  { id: "score", label: "Score", align: "center" },

  { id: "num_correct_answers", label: "Correct Answer", align: "center" },
  {
    id: "num_wrong_answers",
    label: "Wrong Answer",
    align: "center",
  },
  {
    id: "hints_used",
    label: "Hint Used",
    align: "center",
  },
  {
    id: "fifty_fifty_used",
    label: "Fifty-Fifty Used",
  },
];

export default function QuizHistory() {
  const dispatch = useDispatch();

  const quiz_history_temp = useSelector(
    (state) => state.quizReducer.quiz_history
  );
  React.useEffect(() => {
    dispatch(get_quiz_history());
  }, []);
  const [quiz_history, SetQuizHistory] = React.useState([]);
  React.useEffect(() => {
    SetQuizHistory(
      quiz_history_temp.map((q) => ({
        ...q,
        score: ((q.num_correct_answers / 15) * 100).toFixed(0) + "%",
      }))
    );
  }, [quiz_history_temp]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <>
      <Paper sx={{ width: "80%", overflow: "hidden", margin: "2% 10%" }}>
        <Typography variant="h5" color="primary">
          Quiz History :{" "}
        </Typography>
        <TableContainer sx={{ maxHeight: 440 }}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {quiz_history
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row) => {
                  return (
                    <TableRow
                      hover
                      role="checkbox"
                      tabIndex={-1}
                      key={row.code}
                    >
                      {columns.map((column) => {
                        const value = row[column.id];
                        return (
                          <TableCell key={column.id} align={column.align}>
                            {column.format && typeof value === "number"
                              ? column.format(value)
                              : value}
                          </TableCell>
                        );
                      })}
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={quiz_history.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
    </>
  );
}
