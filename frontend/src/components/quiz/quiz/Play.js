import React, { Component, Fragment } from "react";
import { Helmet } from "react-helmet";
import classnames from "classnames";
import { toast } from "react-toastify";
import correctNotification from "../audio/correct-answer.mp3";
import wrongNotification from "../audio/wrong-answer.mp3";
import buttonSound from "../audio/button-sound.mp3";
import { useNavigate } from "react-router";
import Sidebar from "./Sidebar";
import { connect } from "react-redux";
import {
  add_quiz_result,
  get_questions_for_quiz,
} from "../../../actions/quiz_action";
import { useParams } from "react-router-dom";
export const withRouter = (Component) => {
  const Wrapper = (props) => {
    const history = useNavigate();
    const { subject_id } = useParams();
    return <Component history={history} subject_id={subject_id} {...props} />;
  };
  return Wrapper;
};

const isEmpty = (value) =>
  value === undefined ||
  value == null ||
  (typeof value === "object" && Object.keys(value).length === 0) ||
  (typeof value === "string" && value.trim().length === 0);

class Play extends Component {
  constructor(props) {
    super(props);
    this.state = {
      questions: [],
      currentQuestion: {},
      selected_option: [
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
      ],
      nextQuestion: {},
      previousQuestion: {},
      answer: "",
      numberOfQuestions: 0,
      numberOfAnsweredQuestions: 0,
      currentQuestionIndex: 0,
      score: 0,
      correctAnswers: 0,
      wrongAnswers: 0,
      hints: 5,
      fiftyFifty: 2,
      usedFiftyFifty: false,
      nextButtonDisabled: false,
      previousButtonDisabled: true,
      previousRandomNumbers: [],
      time: {},
    };
    this.interval = null;
    this.correctSound = React.createRef();
    this.wrongSound = React.createRef();
    this.buttonSound = React.createRef();
    const { subject_id } = this.props;

    if (subject_id) this.props.dispatch(get_questions_for_quiz(subject_id));
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  displayQuestions = (
    questions = this.state.questions,
    currentQuestion,
    nextQuestion,
    previousQuestion
  ) => {
    let { currentQuestionIndex } = this.state;
    if (!isEmpty(this.state.questions)) {
      questions = this.state.questions;
      currentQuestion = questions[currentQuestionIndex];
      nextQuestion = questions[currentQuestionIndex + 1];
      previousQuestion = questions[currentQuestionIndex - 1];
      const answer = currentQuestion.correct_option;
      this.setState(
        {
          currentQuestion,
          nextQuestion,
          previousQuestion,
          numberOfQuestions: questions.length,
          answer,
          previousRandomNumbers: [],
        },
        () => {
          this.showOptions();
          this.handleDisableButton();
        }
      );
    }
  };

  handleOptionClick = (selected_answer) => {
    this.setState({
      selected_option: this.state.selected_option.map((op, ind) =>
        ind == this.state.currentQuestionIndex ? selected_answer : op
      ),
    });

    if (selected_answer == this.state.answer) {
      this.correctTimeout = setTimeout(() => {
        this.correctSound.current.play();
      }, 5);
      this.correctAnswer();
    } else {
      this.wrongTimeout = setTimeout(() => {
        this.correctSound.current.play();
      }, 5);
      this.wrongAnswer();
    }
  };

  handleQuestionSelectButtonClick = (index) => {
    this.playButtonSound();
    this.setState(
      (prevState) => ({
        currentQuestionIndex: index,
      }),
      () => {
        this.displayQuestions(
          this.state.state,
          this.state.currentQuestion,
          this.state.nextQuestion,
          this.state.previousQuestion
        );
      }
    );
  };

  handleNextButtonClick = () => {
    this.playButtonSound();
    if (this.state.nextQuestion !== undefined) {
      this.setState(
        (prevState) => ({
          currentQuestionIndex: prevState.currentQuestionIndex + 1,
        }),
        () => {
          this.displayQuestions(
            this.state.state,
            this.state.currentQuestion,
            this.state.nextQuestion,
            this.state.previousQuestion
          );
        }
      );
    }
  };

  handlePreviousButtonClick = () => {
    this.playButtonSound();
    if (this.state.previousQuestion !== undefined) {
      this.setState(
        (prevState) => ({
          currentQuestionIndex: prevState.currentQuestionIndex - 1,
        }),
        () => {
          this.displayQuestions(
            this.state.state,
            this.state.currentQuestion,
            this.state.nextQuestion,
            this.state.previousQuestion
          );
        }
      );
    }
  };

  handleQuitButtonClick = () => {
    this.playButtonSound();
    if (window.confirm("Are you sure you want to quit?")) {
      this.props.history("/quiz");
    }
  };

  handleButtonClick = (e) => {
    switch (e.target.id) {
      case "next-button":
        this.handleNextButtonClick();
        break;

      case "previous-button":
        this.handlePreviousButtonClick();
        break;

      case "quit-button":
        this.handleQuitButtonClick();
        break;

      default:
        break;
    }
  };

  playButtonSound = () => {
    this.buttonSound.current.play();
  };

  correctAnswer = () => {
    this.setState(
      (prevState) => ({
        score: prevState.score + 1,
        correctAnswers: prevState.correctAnswers + 1,
        currentQuestionIndex: prevState.currentQuestionIndex + 1,
        numberOfAnsweredQuestions: prevState.numberOfAnsweredQuestions + 1,
      }),
      () => {
        if (this.state.nextQuestion === undefined) {
          this.setState((prevState) => ({
            currentQuestionIndex: prevState.currentQuestionIndex - 1,
          }));
        } else {
          this.displayQuestions(
            this.state.questions,
            this.state.currentQuestion,
            this.state.nextQuestion,
            this.state.previousQuestion
          );
        }
      }
    );
  };

  wrongAnswer = () => {
    navigator.vibrate(1000);
    this.setState(
      (prevState) => ({
        wrongAnswers: prevState.wrongAnswers + 1,
        currentQuestionIndex: prevState.currentQuestionIndex + 1,
        numberOfAnsweredQuestions: prevState.numberOfAnsweredQuestions + 1,
      }),
      () => {
        if (this.state.nextQuestion === undefined) {
          this.setState((prevState) => ({
            currentQuestionIndex: prevState.currentQuestionIndex - 1,
          }));
        } else {
          this.displayQuestions(
            this.state.questions,
            this.state.currentQuestion,
            this.state.nextQuestion,
            this.state.previousQuestion
          );
        }
      }
    );
  };

  showOptions = () => {
    const options = Array.from(document.querySelectorAll(".option"));

    options.forEach((option) => {
      option.style.visibility = "visible";
    });

    this.setState({
      usedFiftyFifty: false,
    });
  };

  handleHints = () => {
    if (this.state.hints > 0) {
      const options = Array.from(document.querySelectorAll(".option"));
      let indexOfAnswer;

      options.forEach((option, index) => {
        if (option.attributes[1].value === this.state.answer) {
          indexOfAnswer = index;
        }
      });

      while (true) {
        const randomNumber = Math.round(Math.random() * 3);
        if (
          randomNumber !== indexOfAnswer &&
          !this.state.previousRandomNumbers.includes(randomNumber)
        ) {
          options.forEach((option, index) => {
            if (index === randomNumber) {
              option.style.visibility = "hidden";
              this.setState((prevState) => ({
                hints: prevState.hints - 1,
                previousRandomNumbers:
                  prevState.previousRandomNumbers.concat(randomNumber),
              }));
            }
          });
          break;
        }
        if (this.state.previousRandomNumbers.length >= 3) break;
      }
    }
  };

  handleFiftyFifty = () => {
    if (this.state.fiftyFifty > 0 && this.state.usedFiftyFifty === false) {
      const options = document.querySelectorAll(".option");
      const randomNumbers = [];
      let indexOfAnswer;
      options.forEach((option, index) => {
        if (option.attributes[1].value === this.state.answer) {
          indexOfAnswer = index;
        }
      });

      let count = 0;
      do {
        const randomNumber = Math.round(Math.random() * 3);
        if (randomNumber !== indexOfAnswer) {
          if (
            randomNumbers.length < 2 &&
            !randomNumbers.includes(randomNumber) &&
            !randomNumbers.includes(indexOfAnswer)
          ) {
            randomNumbers.push(randomNumber);
            count++;
          } else {
            while (true) {
              const newRandomNumber = Math.round(Math.random() * 3);
              if (
                !randomNumbers.includes(newRandomNumber) &&
                newRandomNumber !== indexOfAnswer
              ) {
                randomNumbers.push(newRandomNumber);
                count++;
                break;
              }
            }
          }
        }
      } while (count < 2);

      options.forEach((option, index) => {
        if (randomNumbers.includes(index)) {
          option.style.visibility = "hidden";
        }
      });
      this.setState((prevState) => ({
        fiftyFifty: prevState.fiftyFifty - 1,
        usedFiftyFifty: true,
      }));
    }
  };

  startTimer = () => {
    const countDownTime = Date.now() + 180000;
    this.interval = setInterval(() => {
      const now = new Date();
      const distance = countDownTime - now;

      const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      const seconds = Math.floor((distance % (1000 * 60)) / 1000);

      if (distance < 0) {
        clearInterval(this.interval);
        this.setState(
          {
            time: {
              minutes: 0,
              seconds: 0,
            },
          },
          () => {
            this.endGame();
          }
        );
      } else {
        this.setState({
          time: {
            minutes,
            seconds,
            distance,
          },
        });
      }
    }, 1000);
  };

  handleDisableButton = () => {
    if (
      this.state.previousQuestion === undefined ||
      this.state.currentQuestionIndex === 0
    ) {
      this.setState({
        previousButtonDisabled: true,
      });
    } else {
      this.setState({
        previousButtonDisabled: false,
      });
    }

    if (
      this.state.nextQuestion === undefined ||
      this.state.currentQuestionIndex + 1 === this.state.numberOfQuestions
    ) {
      this.setState({
        nextButtonDisabled: true,
      });
    } else {
      this.setState({
        nextButtonDisabled: false,
      });
    }
  };

  endGame = async () => {
    alert("Quiz has eneded!");
    const { state } = this;
    try {
      const { subject_id } = this.props;
      let res = {};
      if (subject_id)
        res = await add_quiz_result({
          num_correct_answers: state.correctAnswers,
          num_wrong_answers: state.wrongAnswers,
          hints_used: 5 - state.hints,
          fifty_fifty_used: 2 - state.fiftyFifty,
          subject_id,
        });
      setTimeout(() => {
        if (res.status == "success")
          this.props.history(`/quiz/quizSummary/${res.data.insertId}`);
        else this.props.history(`/quiz/quizSummary/0`);
      }, 1000);
    } catch (err) {
      this.props.history(`/quiz/quizSummary/0`);
    }
  };
  componentDidUpdate(prevProps) {
    if (this.props.questions !== prevProps.questions) {
      this.setState({ questions: this.props.questions });
      const { questions, currentQuestion, nextQuestion, previousQuestion } =
        this.state;
      this.displayQuestions(
        questions,
        currentQuestion,
        nextQuestion,
        previousQuestion
      );
      this.startTimer();
    }
  }
  render() {
    const {
      currentQuestion,
      currentQuestionIndex,
      fiftyFifty,
      hints,
      numberOfQuestions,
      time,
    } = this.state;
    return (
      <div className="flex" style={{ padding: "0 8%" }}>
        {this.state?.questions?.length > 0 && (
          <Sidebar
            questions={this.state.questions}
            currentQuestionIndex={currentQuestionIndex}
            selected_options={this.state.selected_option}
            handleQuestionSelectButtonClick={
              this.handleQuestionSelectButtonClick
            }
          />
        )}
        <div style={{ width: "100%" }}>
          <Fragment>
            <Helmet>
              <title>Quiz Page</title>
            </Helmet>
            <Fragment>
              <audio ref={this.correctSound} src={correctNotification}></audio>
              <audio ref={this.wrongSound} src={wrongNotification}></audio>
              <audio ref={this.buttonSound} src={buttonSound}></audio>
            </Fragment>
            <div className="questions">
              <h2>Quiz Mode</h2>
              <div className="lifeline-container">
                <p>
                  <span
                    onClick={this.handleFiftyFifty}
                    className="mdi mdi-set-center mdi-24px lifeline-icon"
                  >
                    <span className="lifeline">{fiftyFifty}</span>
                  </span>
                </p>
                <p>
                  <span
                    onClick={this.handleHints}
                    className="mdi mdi-lightbulb-on-outline mdi-24px lifeline-icon"
                  >
                    <span className="lifeline">{hints}</span>
                  </span>
                </p>
              </div>
              <div className="timer-container">
                <p>
                  <span className="left" style={{ float: "left" }}>
                    {currentQuestionIndex + 1} of {numberOfQuestions}
                  </span>
                  <span
                    className={classnames("right valid", {
                      warning: time.distance <= 120000,
                      invalid: time.distance < 30000,
                    })}
                  >
                    {time.minutes}:{time.seconds}
                    <span className="mdi mdi-clock-outline mdi-24px"></span>
                  </span>
                </p>
              </div>
              <h5>{currentQuestion.question_text}</h5>
              <div style={{ display: "flex", justifyContent: "center" }}>
                <div className="options-container">
                  <p
                    onClick={() => this.handleOptionClick("A")}
                    className="option"
                    name="A"
                    style={{
                      backgroundColor: `${
                        this.state.selected_option[currentQuestionIndex] == "A"
                          ? "green"
                          : ""
                      }`,
                    }}
                  >
                    {currentQuestion.option_a}
                  </p>
                  <p
                    onClick={() => this.handleOptionClick("B")}
                    className="option"
                    name="B"
                    style={{
                      backgroundColor: `${
                        this.state.selected_option[currentQuestionIndex] == "B"
                          ? "green"
                          : ""
                      }`,
                    }}
                  >
                    {currentQuestion.option_b}
                  </p>
                  <p
                    onClick={() => this.handleOptionClick("C")}
                    className="option"
                    name="C"
                    style={{
                      backgroundColor: `${
                        this.state.selected_option[currentQuestionIndex] == "C"
                          ? "green"
                          : ""
                      }`,
                    }}
                  >
                    {currentQuestion.option_c}
                  </p>
                  <p
                    onClick={() => this.handleOptionClick("D")}
                    className="option"
                    name="D"
                    style={{
                      backgroundColor: `${
                        this.state.selected_option[currentQuestionIndex] == "D"
                          ? "green"
                          : ""
                      }`,
                    }}
                  >
                    {currentQuestion.option_d}
                  </p>
                </div>
              </div>
            </div>
            <div className="button-container">
              <button
                className={classnames("", {
                  disable: this.state.previousButtonDisabled,
                })}
                id="previous-button"
                onClick={this.handleButtonClick}
              >
                Previous
              </button>
              <button
                className={classnames("", {
                  disable: this.state.nextButtonDisabled,
                })}
                id="next-button"
                onClick={this.handleButtonClick}
              >
                Next
              </button>

              <button id="quit-button" onClick={this.handleButtonClick}>
                Quit
              </button>
            </div>
            <div style={{ display: "flex", justifyContent: "flex-end" }}>
              <button onClick={() => this.endGame()}>Finish</button>
            </div>
          </Fragment>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    questions: state?.quizReducer?.questions.map((q) => ({
      ...q,
      selected_op: "",
    })),
  };
};

export default connect(mapStateToProps)(withRouter(Play));
