import React, { useEffect } from "react";
import { Helmet } from "react-helmet";
import { useDispatch, useSelector } from "react-redux";
import { Link, useParams } from "react-router-dom";
import { get_quiz_result_by_id } from "../../../actions/quiz_action";
import QuizHistory from "./QuizHistory";

const QuizSummary = (props) => {
  const state = useSelector((state) => state.quizReducer.quiz);

  const userScore = state ? (state.num_correct_answers / 15) * 100 : 0;
  let remark;
  if (userScore <= 30) {
    remark = "You need more practice!";
  } else if (userScore > 30 && userScore <= 50) {
    remark = "Better luck next time!";
  } else if (userScore <= 70 && userScore > 50) {
    remark = "You can do better!";
  } else if (userScore >= 71 && userScore <= 84) {
    remark = "You did great!";
  } else {
    remark = "You're an absolute genius!";
  }
  const { quiz_id } = useParams();
  const dispatch = useDispatch();
  useEffect(() => {
    if (quiz_id) dispatch(get_quiz_result_by_id(quiz_id));
  }, [quiz_id]);

  return (
    <>
      <div style={{ marginTop: "5%" }}></div>
      <Helmet>
        <title>Quiz App - Summary</title>
      </Helmet>
      <div className="quiz-summary">
        {state ? (
          <>
            <div style={{ textAlign: "center" }}>
              <span className="mdi mdi-check-circle-outline success-icon"></span>
            </div>
            <h1>Quiz has ended</h1>
            <div className="container stats">
              <h4>{remark}</h4>
              <h2>Your Score: {userScore.toFixed(0)}&#37;</h2>
              <span className="stat left">Total number of questions: </span>
              <span className="right">15</span>
              <br />
              <span className="stat left">Number of attempted questions: </span>
              <span className="right">
                {state.num_correct_answers + state.num_wrong_answers}
              </span>
              <br />
              <span className="stat left">Number of Correct Answers: </span>
              <span className="right">{state.num_correct_answers}</span> <br />
              <span className="stat left">Number of Wrong Answers: </span>
              <span className="right">{state.num_wrong_answers}</span>
              <br />
              <span className="stat left">Hints Used: </span>
              <span className="right">{state.hints_used}</span>
              <br />
              <span className="stat left">50-50 Used: </span>
              <span className="right">{state.fifty_fifty_used}</span>
            </div>
            <section>
              <ul>
                <li>
                  <Link to={`/quiz/play/${state.subject_id}`}>Play Again</Link>
                </li>
                <li>
                  <Link to="/quiz">Back to Home</Link>
                </li>
              </ul>
            </section>
          </>
        ) : (
          <section>
            <h1 className="no-stats">No Statistics Available</h1>
            <ul>
              <li>
                <Link to={`/quiz/play/${state.subject_id}`}>Take a Quiz</Link>
              </li>
              <li>
                <Link to="/quiz">Back to Home</Link>
              </li>
            </ul>
          </section>
        )}
      </div>
      {state.subject_id && <QuizHistory subject_id={state.subject_id} />}
    </>
  );
};

export default QuizSummary;
