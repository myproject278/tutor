import React from "react";
import PublicIcon from "@material-ui/icons/Public";
import StarsIcon from "@material-ui/icons/Stars";
import WorkIcon from "@material-ui/icons/Work";
import "../../question_answer/Main/css/Sidebar.css";
import { Link } from "react-router-dom";
import { Input } from "@material-ui/core";

function Sidebar({
  questions,
  currentQuestionIndex,
  handleQuestionSelectButtonClick,
  selected_options,
}) {
  return (
    <div className="sidebar" style={{ maxWidth: "300px" }}>
      <div className="sidebar-container">
        <div className="sidebar-options" style={{ width: "270px" }}>
          <div className="sidebar-option">
            <Link to="/">Home</Link>

            {/* <a href="/">Home</a> */}
          </div>

          <div
            className="sidebar-option"
            style={{ maxHeight: "69vh", overflow: "scroll" }}
          >
            <div className="link">
              {questions.map((q, index) => (
                <div className="link-tag">
                  {currentQuestionIndex == index ? (
                    <button
                      style={{
                        backgroundColor: "#ff9b00",
                        color: "#0f0d0d",
                        width: "310px",
                        textAlign: "start",
                      }}
                    >
                      {`${index + 1}) `}
                      {q.question_text.slice(0, 30)}....
                    </button>
                  ) : (
                    <button
                      style={{
                        width: "310px",
                        textAlign: "start",
                        backgroundColor: `${
                          selected_options[index] != ""
                            ? "rgb(111 114 113)"
                            : ""
                        }`,
                      }}
                      onClick={() => {
                        handleQuestionSelectButtonClick(index);
                      }}
                    >
                      {`${index + 1}) `}
                      {q.question_text.slice(0, 30)}....
                    </button>
                  )}
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Sidebar;
