import React, { useState } from 'react';
import { Box, Button, Typography } from '@mui/material';
import { useDropzone } from 'react-dropzone';

const ImageUpload = ({images, setImages}) => {
  

    const onDrop = (acceptedFiles) => {
      console.log(acceptedFiles)
      setImages((prevImages) => [...prevImages, acceptedFiles[0]]);
     
  };

  const { getRootProps, getInputProps } = useDropzone({ onDrop });

  return (
    <Box>
      <div {...getRootProps()}>
        <input {...getInputProps()} />
        <Button variant="contained" color="primary">
        Upload
      </Button>
      </div>
      <Box mt={2}>
        {images.map((image, index) => (
          <Typography key={index} variant="body1">
            {image.name}
          </Typography>
        ))}
      </Box>
     
    </Box>
  );
};

export default ImageUpload;
