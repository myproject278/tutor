import React, { useEffect, useState } from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { CardActionArea } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { getSubjects } from "../../actions/subject_action";
import { useParams, useNavigate } from "react-router-dom";
import { get_image_url } from "../../helper/util";
import Heading from "../common/heading/Heading";
import "./courses.css";

const SubjectCard = ({ subjects = [] }) => {
  const navigate = useNavigate();
  const click_subject = (s) => {
    console.log(s);
    if (s.parent_id != 0)
      navigate(
        `/subject/teacher/${s.id}?name=${s.name}&sort=${s.sorting_column}`
      );
    else if (s.parent_id == 0) navigate(`/subjects/${s.id}?name=${s?.name}`);
  };
  return (
    <>
      <section className="online">
        <div className="container">
          <Heading subtitle="Subjects" title="Browse Our Online subjects" />
          <div className="content grid3">
            {subjects.map((s) => (
              <div className="box" onClick={() => click_subject(s)}>
                <div className="img">
                  <img src={get_image_url(s.image_url)} />
                </div>
                <h1>{s.name}</h1>
              </div>
            ))}
          </div>
        </div>
      </section>
    </>
  );
};

export default SubjectCard;
