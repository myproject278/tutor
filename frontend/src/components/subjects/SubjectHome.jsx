import React, { useEffect, useState } from "react";
import Back from "../common/back/Back";
import SubjectCard from "./SubjectCard";
import { useDispatch, useSelector } from "react-redux";
import { getSubjects } from "../../actions/subject_action";
import { useParams } from "react-router-dom";

const SubjectHome = () => {
  const { id } = useParams();
  const dispatch = useDispatch();
  const subjects = useSelector((state) => state.subjectReducer.subjects);
  const auth = useSelector((state) => state.authReducer);
  useEffect(() => {
    dispatch(getSubjects(id ? id : 0));
  }, [id, auth.authenticate]);

  return (
    <>
      <Back />
      <div style={{ marginTop: "5%" }}></div>
      <SubjectCard subjects={subjects} />
    </>
  );
};

export default SubjectHome;
