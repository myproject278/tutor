import React from "react"
import { useLocation } from "react-router-dom"
import "./back.css";
const Back = ({ title,style }) => {
  const location = useLocation()

  return (
    <>
      <section className="back" style={style}>
        <h1 className="back-title">{title}</h1>
      </section>
    </>
  );
}

export default Back
