import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { get_unread_notification_count } from "../../../actions/notification_action";
import { Box, IconButton, useTheme } from "@mui/material";

import NotificationComponent from "../../notifications";
import ProfileDropdown from "../profile/ProfileDropDown";
import "./header.css";
const Head = () => {
  const { authenticate, loading } = useSelector((state) => state.authReducer);

  const dispatch = useDispatch();
  useEffect(() => {
    if (authenticate) dispatch(get_unread_notification_count());
  }, [dispatch, authenticate]);

  return (
    <>
      <section className="head">
        <div className="container flexSB">
          <div className="logo">
            <h1>SANGAM</h1>
            <span>ONLINE EDUCATION & LEARNING</span>
          </div>

          {authenticate && (
            <div className="social flex">
              <i>
                <NotificationComponent />
              </i>
              <i style={{ marginLeft: "8px" }}>
                <ProfileDropdown />
              </i>
            </div>
          )}
        </div>
      </section>
    </>
  );
};

export default Head;
