import React, { useEffect } from "react";
import { get_image_url } from "../../../helper/util";

import { Card, CardContent, Typography, Grid, Divider } from "@mui/material";
const UserEdit = ({ user }) => {
  return (
    <Card sx={{ mb: 4 }}>
      <CardContent>
        <Grid container>
          <Grid item sm={3}>
            <Typography variant="body1">Full Name</Typography>
          </Grid>
          <Grid item sm={9}>
            <Typography variant="body2" color="textSecondary">
              {user.name}
            </Typography>
          </Grid>
        </Grid>
        <Divider sx={{ my: 2 }} />
        <Grid container>
          <Grid item sm={3}>
            <Typography variant="body1">Email</Typography>
          </Grid>
          <Grid item sm={9}>
            <Typography variant="body2" color="textSecondary">
              {user.email}
            </Typography>
          </Grid>
        </Grid>
        <Divider sx={{ my: 2 }} />
        <Grid container>
          <Grid item sm={3}>
            <Typography variant="body1">Phone</Typography>
          </Grid>
          <Grid item sm={9}>
            <Typography variant="body2" color="textSecondary">
              {user.phone_no}
            </Typography>
          </Grid>
        </Grid>
        <Divider sx={{ my: 2 }} />
        <Grid container>
          <Grid item sm={3}>
            <Typography variant="body1">Current Position</Typography>
          </Grid>
          <Grid item sm={9}>
            <Typography variant="body2" color="textSecondary">
              {user.current_position}
            </Typography>
          </Grid>
        </Grid>
        <Divider sx={{ my: 2 }} />
        <Grid container>
          <Grid item sm={3}>
            <Typography variant="body1">Last Education </Typography>
          </Grid>
          <Grid item sm={9}>
            <Typography variant="body2" color="textSecondary">
              {user.last_education}
            </Typography>
          </Grid>
        </Grid>
        <Divider sx={{ my: 2 }} />
        <Grid container>
          <Grid item sm={3}>
            <Typography variant="body1">Secound Last Education </Typography>
          </Grid>
          <Grid item sm={9}>
            <Typography variant="body2" color="textSecondary">
              {user.secound_last_education}
            </Typography>
          </Grid>
        </Grid>
        <Divider sx={{ my: 2 }} />
        <Grid container>
          <Grid item sm={3}>
            <Typography variant="body1">Bio </Typography>
          </Grid>
          <Grid item sm={9}>
            <Typography variant="body2" color="textSecondary">
              {user.bio}
            </Typography>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

export default UserEdit;
