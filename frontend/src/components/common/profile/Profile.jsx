import React, { useEffect } from "react";
import { get_image_url } from "../../../helper/util";

import {
  Container,
  Breadcrumbs,
  Link,
  Card,
  CardContent,
  Button,
  Typography,
  List,
  ListItem,
  ListItemText,
  Grid,
  Divider,
  LinearProgress,
  Box,
  IconButton,
  Avatar,
} from "@mui/material";
import {
  GitHub,
  Twitter,
  Instagram,
  Facebook,
  Language,
} from "@mui/icons-material";
import { useSelector, useDispatch } from "react-redux";
import { get_user_info } from "../../../actions/auth_action";
import { get_quiz_score_for_subjects } from "../../../actions/quiz_action";
import UserEdit from "./UserEdit";

export default function ProfilePage() {
  const dispatch = useDispatch();
  const { user } = useSelector((state) => state.authReducer);
  const quiz_score_for_subjects = useSelector(
    (state) => state.quizReducer.quiz_score_for_subjects
  );
  useEffect(() => {
    dispatch(get_user_info());
    dispatch(get_quiz_score_for_subjects());
  }, []);
  console.log(user);
  return (
    <section style={{ backgroundColor: "#eee" }}>
      <Container sx={{ py: 5 }}>
        <Grid container spacing={2}>
          <Grid item xs={12} lg={4}>
            <Card sx={{ mb: 4 }}>
              <CardContent>
                <Breadcrumbs
                  separator="›"
                  aria-label="breadcrumb"
                  sx={{ mb: 4 }}
                >
                  <Link color="inherit" href="#">
                    Home
                  </Link>
                  <Link color="inherit" href="#">
                    User
                  </Link>
                </Breadcrumbs>
                <div
                  style={{
                    height: "113px",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Avatar
                    src={get_image_url(user.image_url)}
                    style={{
                      height: "100%",
                      width: "113px",
                      borderRadius: "50%", // Add this line to create a circular avatar
                    }}
                  />
                </div>

                <Typography
                  variant="subtitle1"
                  color="textSecondary"
                  align="center"
                  sx={{ mb: 1 }}
                >
                  {user.name}
                </Typography>
                <Typography
                  variant="subtitle2"
                  color="textSecondary"
                  sx={{ mb: 1 }}
                  align="center"
                >
                  {user.email}
                </Typography>
                <Typography
                  variant="subtitle2"
                  color="textSecondary"
                  sx={{ mb: 1 }}
                  align="center"
                >
                  {user.current_position}
                </Typography>
              </CardContent>
            </Card>

            <Card>
              <CardContent>
                <List sx={{ borderRadius: 3 }}>
                  <ListItem>
                    <IconButton color="warning">
                      <Language />
                    </IconButton>
                    <ListItemText primary="https://mdbootstrap.com" />
                  </ListItem>
                  <ListItem>
                    <IconButton color="primary">
                      <GitHub />
                    </IconButton>
                    <ListItemText primary="mdbootstrap" />
                  </ListItem>
                  <ListItem>
                    <IconButton color="info">
                      <Twitter />
                    </IconButton>
                    <ListItemText primary="@mdbootstrap" />
                  </ListItem>
                  <ListItem>
                    <IconButton color="error">
                      <Instagram />
                    </IconButton>
                    <ListItemText primary="mdbootstrap" />
                  </ListItem>
                  <ListItem>
                    <IconButton color="info">
                      <Facebook />
                    </IconButton>
                    <ListItemText primary="mdbootstrap" />
                  </ListItem>
                </List>
              </CardContent>
            </Card>
          </Grid>
          <Grid item xs={12} lg={8}>
            <UserEdit user={user} />
            <Grid container spacing={2} xs={12}>
              <Grid item md={12}>
                <Card>
                  <CardContent>
                    <Typography variant="subtitle1" sx={{ mb: 4 }}>
                      <span
                        style={{
                          color: "#1976D2",
                          fontStyle: "italic",
                          marginRight: "4px",
                        }}
                      >
                        Quiz
                      </span>{" "}
                      Average Score
                    </Typography>
                    {quiz_score_for_subjects.map((q) => (
                      <>
                        <Typography
                          variant="body2"
                          sx={{ mb: 1, fontSize: ".77rem" }}
                        >
                          {q.name}
                        </Typography>
                        <LinearProgress
                          variant="determinate"
                          value={q.quiz_score}
                          sx={{ mb: 2 }}
                        />
                      </>
                    ))}
                  </CardContent>
                </Card>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Container>
    </section>
  );
}
