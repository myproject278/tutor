import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";
import Popover from "@material-ui/core/Popover";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Divider from "@material-ui/core/Divider";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import { get_image_url } from "../../../helper/util";
import { useNavigate } from "react-router-dom";
import { logoutUser } from "../../../actions/auth_action";

const useStyles = makeStyles((theme) => ({
  profileDropdown: {
    position: "relative",
    cursor: "pointer",
  },
  dropdownContent: {
    padding: theme.spacing(2),
    minWidth: 250,
  },
  avatar: {
    width: theme.spacing(6),
    height: theme.spacing(6),
    cursor: "pointer",
  },
  userName: {
    marginTop: theme.spacing(1),
    fontWeight: "bold",
  },
  userEmail: {
    color: theme.palette.text.secondary,
  },
  manageProfileBtn: {
    marginTop: theme.spacing(2),
    width: "100%",
  },
  logoutBtn: {
    marginTop: theme.spacing(2),
    width: "100%",
    color: theme.palette.error.main,
  },
  logoutIcon: {
    marginRight: theme.spacing(1),
  },
}));

const ProfileDropdown = () => {
  const classes = useStyles();
  const navigate = useNavigate();
  const [anchorEl, setAnchorEl] = useState(null);

  const { user } = useSelector((state) => state.authReducer);

  const handleAvatarClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleDropdownClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? "profile-popover" : undefined;
  const dispatch = useDispatch();
  const handleLogout = () => {
    dispatch(logoutUser());
    navigate("/auth");
  };

  return (
    <div className={classes.profileDropdown}>
      <Avatar
        alt="User Avatar"
        src={get_image_url(user.image_url)}
        className={classes.avatar}
        onClick={handleAvatarClick}
      />
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleDropdownClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "right",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
      >
        <div className={classes.dropdownContent}>
          <div className="flex" style={{ alignItems: "center" }}>
            <Avatar
              alt="User Avatar"
              src={get_image_url(user.image_url)}
              className={classes.avatar}
            />
            <div style={{ marginLeft: "4px" }}>
              <Typography className={classes.userName}>{user.name}</Typography>
              <Typography className={classes.userEmail}>
                {user.email}
              </Typography>
            </div>
          </div>
          <Button
            className={classes.manageProfileBtn}
            variant="contained"
            color="primary"
            onClick={() => navigate("/profile")}
          >
            Manage Your Account
          </Button>
          <Divider />
          <Button
            className={classes.logoutBtn}
            variant="outlined"
            onClick={handleLogout}
          >
            <ExitToAppIcon className={classes.logoutIcon} />
            Sign out
          </Button>
        </div>
      </Popover>
    </div>
  );
};

export default ProfileDropdown;
