import React from "react";
import styles from "./heading.css";

const Heading = ({ subtitle, title }) => {
  return (
    <div className="heading">
      <h3>{subtitle}</h3>
      <h1 style={{ fontSize: "29px" }}>{title}</h1>
    </div>
  );
};

export default Heading;
