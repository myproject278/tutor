import React, { useEffect } from "react";
import { blog } from "../../../dummydata";
import "./footer.css";
import { useDispatch, useSelector } from "react-redux";
import { get_recent_and_famous_posts } from "../../../actions/post_action";
import { get_image_url } from "../../../helper/util";
import { format } from "date-fns"; // Only import the 'format' function

const Footer = () => {
  const dispatch = useDispatch();
  const posts = useSelector((state) => state.postReducer.posts);
  useEffect(() => {
    dispatch(get_recent_and_famous_posts());
  }, []);
  return (
    <>
      <footer style={{ marginTop: "20px" }}>
        <div className="container padding">
          <div className="box logo">
            <h1>SANGAM</h1>
            <span>ONLINE EDUCATION & LEARNING</span>
            <p>
              Education is the most powerful weapon which you can use to change
              the world.
            </p>

            {/* <i className="fab fa-facebook-f icon"></i>
            <i className="fab fa-twitter icon"></i>
            <i className="fab fa-instagram icon"></i> */}
          </div>
          <div className="box link">
            <h3>Explore</h3>
            <ul>
              <li>Subjects</li>
              <li>Teachers</li>
              <li>Requests</li>
              <li>Quotions</li>
              <li>Posts</li>
            </ul>
          </div>

          <div className="box posts_footer">
            <h3>Recent Post</h3>
            {posts.slice(0, 3).map(({ date, header, id, image_urls, name }) => {
              let image = null;
              if (image_urls && image_urls.length > 0) {
                image = JSON.parse(image_urls)[0].img;
              }
              return (
                <div className="items flex">
                  <div className="img">
                    <img src={get_image_url(image)} alt="" />
                  </div>
                  <div className="text">
                    <span>
                      <i className="fa fa-calendar-alt"></i>
                      <label htmlFor="">
                        {format(new Date(date), "EEE, MMM dd, yyyy")}
                      </label>
                    </span>

                    <h4>{header.slice(0, 40)}...</h4>
                  </div>
                </div>
              );
            })}
          </div>
          <div className="box last">
            <h3>Have a Questions?</h3>
            <ul>
              <li>
                <i className="fa fa-map"></i>
                Pune Maharastra
              </li>
              <li>
                <i className="fa fa-phone-alt"></i>
                9145125535
              </li>
              <li>
                <i className="fa fa-paper-plane"></i>
                karankadam540@gmail.com
              </li>
            </ul>
          </div>
        </div>
      </footer>
    </>
  );
};

export default Footer;
