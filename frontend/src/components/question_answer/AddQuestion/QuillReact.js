import React, { useState, useRef } from "react";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import "./index.css";

const MyEditor = ({ body, setBody }) => {
  const quillRef = useRef();

  const toolbarOptions = [
    ["bold", "italic", "code", "link"],
    [{ list: "ordered" }, { list: "bullet" }],
    [],
    [{ header: 1 }, { header: 2 }],
    [{ align: [] }],
    [],
  ];

  const handleEditorChange = (content) => {
    setBody(content);
  };

  return (
    <div>
      <ReactQuill
        ref={quillRef}
        value={body}
        onChange={handleEditorChange}
        modules={{
          toolbar: {
            container: toolbarOptions,
          },
        }}
      />
    </div>
  );
};

export default MyEditor;
