import { Avatar } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import BookmarkIcon from "@material-ui/icons/Bookmark";
import HistoryIcon from "@material-ui/icons/History";
import axios from "axios";
import ReactHtmlParser from "react-html-parser";
import { Link, useParams } from "react-router-dom";
import "./index.css";
import { useDispatch, useSelector } from "react-redux";
import {
  add_answer_for_question,
  get_answers_by_questions,
} from "../../../actions/question_action";
import { get_image_url } from "../../../helper/util";
import { useSnackbar } from "notistack";

function MainQuestion() {
  const { question_id } = useParams();
  const { question_answers } = useSelector((state) => state.questionReducer);
  const question = question_answers?.question || {};
  const answers = question_answers?.anwers || [];
  console.log(answers);
  const dispatch = useDispatch();
  const [questionData, setQuestionData] = useState();
  const [answer, setAnswer] = useState("");
  const [show, setShow] = useState(false);
  const [comment, setComment] = useState("");
  // const [comments, setComments] = useState([]);
  const user = { name: "ar" };

  const handleQuill = (value) => {
    setAnswer(value);
  };

  useEffect(() => {
    if (question_id) dispatch(get_answers_by_questions(question_id));
  }, [question_id]);

  async function getUpdatedAnswer() {
    await axios
      .get(`/api/question/${question_id}`)
      .then((res) => setQuestionData(res.data[0]))
      .catch((err) => console.log(err));
  }
  const { enqueueSnackbar } = useSnackbar();

  // console.log(questionData);
  const handleSubmit = async () => {
    if (answer)
      try {
        const result = await add_answer_for_question({
          question_id: question_id,
          answer: answer,
        });
        if (result.status == "success") {
          enqueueSnackbar("Answer added SuccessFully !!", {
            variant: "success",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          });
          setAnswer("");
          setShow(false);
          dispatch(get_answers_by_questions(question_id));
        } else {
          enqueueSnackbar("Someting went Wrong !!!", {
            variant: "error",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          });
        }
      } catch (err) {
        enqueueSnackbar("Someting went Wrong !!!", {
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        });
      }
    else {
      enqueueSnackbar("Please write answer first!!", {
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      });
    }
  };

  const handleAnswer = async () => {
    if (comment !== "") {
      const body = {
        question_id: question_id,
        comment: comment,
        user: user,
      };
      await axios.post(`/api/comment/${question_id}`, body).then((res) => {
        setComment("");
        setShow(false);
        getUpdatedAnswer();
        // console.log(res.data);
      });
    }

    // setShow(true)
  };
  return (
    <div className="main-qa-text">
      <div className="main-container">
        <div className="main-top">
          <h2 className="main-question">{question?.title} </h2>
          <Link to="/add-question">
            <button>Ask Question</button>
          </Link>
          {/* <a href="/add-question">
            <button>Ask Question</button>
          </a> */}
        </div>
        <div className="main-desc">
          <div className="info">
            <p>
              Asked
              <span>{new Date(question?.created_at).toLocaleString()}</span>
            </p>
            <p style={{ color: `${question?.active ? "green" : "red"}` }}>
              {question?.active ? "Active" : "Closed"}
            </p>
          </div>
        </div>
        <div className="all-questions">
          <div className="all-questions-container">
            <div className="all-questions-left">
              <div className="all-options">
                <p className="arrow">▲</p>

                <p className="arrow">{question?.answer_count}</p>

                <p className="arrow">▼</p>

                <BookmarkIcon />

                <HistoryIcon />
              </div>
            </div>
            <div className="question-answer">
              <p>{ReactHtmlParser(question?.body)}</p>

              <div className="author">
                <small>
                  asked {new Date(question?.created_at).toLocaleString()}
                </small>
                <div className="auth-details">
                  <Avatar
                    alt={question?.name}
                    src={get_image_url(question?.image_url)}
                  />
                  <p>{question?.name}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div
          style={{
            flexDirection: "column",
          }}
          className="all-questions"
        >
          <p
            style={{
              marginBottom: "20px",
              fontSize: "1.3rem",
              fontWeight: "300",
            }}
          >
            Answers
          </p>
          {answers.map((_a) => (
            <>
              <div
                style={{
                  borderBottom: "1px solid #eee",
                }}
                key={_a.id}
                className="all-questions-container"
              >
                <div className="all-questions-left">
                  <div className="all-options">
                    <BookmarkIcon />

                    <HistoryIcon />
                  </div>
                </div>
                <div className="question-answer">
                  {ReactHtmlParser(_a.answer)}
                  <div className="author">
                    <small>
                      Answered {new Date(_a.created_at).toLocaleString()}
                    </small>
                    <div className="auth-details">
                      <Avatar
                        alt={_a?.name}
                        src={get_image_url(_a?.answer_user_image)}
                      />
                      <p>{_a?.name}</p>
                    </div>
                  </div>
                </div>
              </div>
            </>
          ))}
        </div>
        {/* <div className="questions">
          <div className="question">
            <AllQuestions />
            <AllQuestions />
            <AllQuestions />
            <AllQuestions />
          </div>
        </div> */}
      </div>
      <div className="main-answer">
        <h3
          style={{
            margin: "10px 0",
            fontWeight: "400",
            color: "#0e76d6",
          }}
          onClick={() => setShow(!show)}
        >
          ADD Your Answer
        </h3>
      </div>
      {show && (
        <div className="title">
          <textarea
            style={{
              margin: "5px 0px",
              padding: "10px",
              border: "1px solid rgba(0, 0, 0, 0.2)",
              borderRadius: "3px",
              outline: "none",
            }}
            value={answer}
            onChange={(e) => setAnswer(e.target.value)}
            type="text"
            placeholder="Add your answer..."
            rows={5}
          />
          <button
            onClick={handleSubmit}
            style={{
              marginTop: "10px",
              maxWidth: "fit-content",
            }}
          >
            Post your answer
          </button>
        </div>
      )}
    </div>
  );
}

export default MainQuestion;
