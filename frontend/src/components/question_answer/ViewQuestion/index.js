import React from "react";
import "./index.css";
import MainQuestion from "./MainQuestion";
import Sidebar from "../Main/Sidebar";
import StarsIcon from "@material-ui/icons/Stars";
import WorkIcon from "@material-ui/icons/Work";
import { Input } from "@material-ui/core";

function Index() {
  return (
    <div className="stack-index">
      <div className="stack-index-content">
        <Sidebar></Sidebar>
        <MainQuestion />
      </div>
    </div>
  );
}

export default Index;
