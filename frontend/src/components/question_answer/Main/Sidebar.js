import React from "react";
import PublicIcon from "@material-ui/icons/Public";
import StarsIcon from "@material-ui/icons/Stars";
import WorkIcon from "@material-ui/icons/Work";
import "./css/Sidebar.css";
import { Link } from "react-router-dom";
import { Input } from "@material-ui/core";

function Sidebar({ children }) {
  return (
    <div className="sidebar">
      <div className="sidebar-container">
        <div className="sidebar-options">
          <div className="sidebar-option">
            <Link to="/">Home</Link>

            {/* <a href="/">Home</a> */}
          </div>
          <div className="sidebar-option">
            <div className="link">
              <div className="link-tag">
                <PublicIcon />
                <Link to="/questions">Question</Link>

                {/* <a href="/">Question</a> */}
              </div>
            </div>
          </div>
          {children}
        </div>
      </div>
    </div>
  );
}

export default Sidebar;
