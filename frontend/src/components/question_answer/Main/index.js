import React, { useEffect, useState } from "react";
import Sidebar from "./Sidebar";
import "./css/index.css";
import Main from "./Main";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import {
  get_questions,
  get_questions_by_search_tage,
} from "../../../actions/question_action";
import StarsIcon from "@material-ui/icons/Stars";
import WorkIcon from "@material-ui/icons/Work";
import { Button, Input, TextField } from "@material-ui/core";
import ChipInput from "material-ui-chip-input";
import { useSnackbar } from "notistack";

function Index() {
  const { questions } = useSelector((state) => state.questionReducer);
  const dispatch = useDispatch();
  const [tags, setTages] = useState([]);
  const [search, setSearch] = useState("");

  useEffect(() => {
    dispatch(get_questions());
  }, []);
  const addTag = (tag) => {
    setTages([...tags, tag]);
  };
  const deleteTag = (tag) => {
    setTages(tags.filter((t) => t != tag));
  };
  const { enqueueSnackbar } = useSnackbar();

  const search_question_on_tags_name = () => {
    if (search || tags.length > 0) {
      dispatch(get_questions_by_search_tage({ search, tags: tags.join("|") }));
    } else {
      enqueueSnackbar("Please enter value in tags or Input !!!", {
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      });
    }
  };

  return (
    <div className="stack-index">
      <div className="stack-index-content">
        <Sidebar>
          <div className="sidebar-option">
            <div className="link">
              <div className="link-tag">
                <StarsIcon />
                <p>Search Tags</p>
              </div>
              <ChipInput
                style={{ margin: "10px 0" }}
                value={tags}
                label="Search Tags"
                variant="outlined"
                onAdd={addTag}
                onDelete={deleteTag}
              />
            </div>
          </div>
          <div className="sidebar-option" style={{ display: "flex" }}>
            Find Answer By User Name or Title Of Question
            <TextField
              name="search"
              label="User Name or Title ....."
              fullWidth
              value={search}
              onChange={(e) => setSearch(e.target.value)}
            />
          </div>
          <Button
            onClick={search_question_on_tags_name}
            variant="outlined"
            color="primary"
          >
            Search
          </Button>
        </Sidebar>
        <Main questions={questions} />
      </div>
    </div>
  );
}

export default Index;
