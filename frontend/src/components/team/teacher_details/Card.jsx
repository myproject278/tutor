import React from "react";
import "./details.css";
import { get_image_url } from "../../../helper/util";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { Button, CardActionArea } from "@mui/material";
import SubjectCard from "./SubjectCard";
const expertiese_level_arr = ["Basic", "Intermediate", "Expert"];

const get_time_am_pm = (i) => {
  if ((i + 1) % 12 == 0) return `12 ${i == 0 ? "Pm" : "Am"}`;
  else return `${(i + 1) % 12} ${i / 12 > 0 ? "Pm" : "Am"}`;
};
const time_from_arr = (arr = []) => {
  const res = [];
  let flag = false;
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] == 1 && flag == false) {
      flag = true;
      res.push(get_time_am_pm(i));
    } else if (arr[i] == 0 && flag == true) {
      res[res.length - 1] = res[res.length - 1] + " - " + get_time_am_pm(i);
      flag = false;
    }
  }
  if (flag) res[res.length - 1] = res[res.length - 1] + " - 12 Am";
  return res;
};
const CardOuter = ({ teacher }) => {
  let available_days = [];
  if (teacher && teacher.available_slots)
    available_days = JSON.parse(teacher.available_slots);
  return (
    <>
      <section className="aboutHome">
        <div className="container flexSB">
          <div className="left">
            <Card>
              <CardActionArea>
                <div style={{ height: "280px" }}>
                  <CardMedia
                    component="img"
                    image={get_image_url(teacher?.image_url)}
                    alt="green iguana"
                    style={{ objectFit: "cover" }}
                  />
                </div>
                <CardContent>
                  <Typography
                    gutterBottom
                    variant="h5"
                    component="div"
                    align="center"
                  >
                    {teacher?.name}
                  </Typography>
                  <Typography gutterBottom component="div" align="center">
                    {teacher?.email}
                  </Typography>
                  <hr />

                  <Typography variant="h6" align="center">
                    {teacher?.current_position}
                  </Typography>
                </CardContent>
              </CardActionArea>
            </Card>
          </div>
          <div className="right row">
            <div>
              <Typography variant="h6">Education : </Typography>
              <div style={{ padding: "12px 20px" }}>
                <Typography color="black">
                  {teacher?.secound_last_education}
                </Typography>
                <Typography style={{ marginTop: "13px" }}>
                  {teacher?.last_education}
                </Typography>
              </div>
            </div>
            <hr />
            <div style={{ marginTop: "30px" }}>
              <Typography variant="h6">Bio : </Typography>
              <Typography style={{ padding: "12px 20px" }} color="black">
                {teacher?.bio}
              </Typography>
            </div>

            <hr />
            <div style={{ marginTop: "30px" }}>
              <Typography variant="h6">Speaking Language : </Typography>

              <Typography
                style={{ padding: "12px 20px", marginLeft: "50px" }}
                color="black"
              >
                <ul style={{ listStyle: "none", paddingLeft: "20px" }}>
                  {JSON.parse(
                    teacher?.speaking_languaes
                      ? teacher?.speaking_languaes
                      : null
                  )?.map((lan) => (
                    <li key={lan} style={{ position: "relative" }}>
                      <span style={{ paddingLeft: "15px" }}>{lan}</span>
                      <span
                        style={{
                          position: "absolute",
                          left: "0",
                          top: "50%",
                          transform: "translateY(-50%)",
                          width: "8px",
                          height: "8px",
                          borderRadius: "50%",
                          backgroundColor: "black",
                        }}
                      ></span>
                    </li>
                  ))}
                </ul>
              </Typography>
            </div>

            <hr />
            <div style={{ marginTop: "30px" }}>
              <Typography variant="h6">Specialized Subjects : </Typography>
              <SubjectCard
                teacher_id={teacher?.id}
                teacher_name={teacher.name}
              />
            </div>

            <div style={{ marginTop: "40px" }}>
              <hr />
              <div className="text" style={{ padding: "18px" }}>
                <h3> Available Time On Hour Basis : </h3>
                <div>
                  <div
                    className="availabilityslotdiv"
                    style={{ color: "#6a6969" }}
                  >
                    <p>
                      Sunday :{" "}
                      <Typography color="black" style={{ display: "inline" }}>
                        {time_from_arr(available_days?.sunday)?.join(" , ")}
                      </Typography>{" "}
                    </p>
                    <p>
                      Monday :{" "}
                      <Typography color="black" style={{ display: "inline" }}>
                        {" "}
                        {time_from_arr(available_days?.monday)?.join(" , ")}
                      </Typography>
                    </p>
                    <p>
                      Tuesday :
                      <Typography color="black" style={{ display: "inline" }}>
                        {time_from_arr(available_days?.tuesday)?.join(" , ")}
                      </Typography>{" "}
                    </p>
                    <p>
                      Wednesday :{" "}
                      <Typography color="black" style={{ display: "inline" }}>
                        {time_from_arr(available_days?.wednesday)?.join(" , ")}
                      </Typography>{" "}
                    </p>
                    <p>
                      Thursday :{" "}
                      <Typography color="black" style={{ display: "inline" }}>
                        {time_from_arr(available_days?.thursday)?.join(" , ")}
                      </Typography>{" "}
                    </p>
                    <p>
                      Friday :{" "}
                      <Typography color="black" style={{ display: "inline" }}>
                        {time_from_arr(available_days?.friday)?.join(" , ")}
                      </Typography>{" "}
                    </p>
                    <p>
                      Saturday :{" "}
                      <Typography color="black" style={{ display: "inline" }}>
                        {time_from_arr(available_days?.saturday)?.join(" , ")}
                      </Typography>{" "}
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default CardOuter;
