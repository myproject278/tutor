import React, { useEffect } from "react";
import "./details.css";
import Back from "../../common/back/Back";
import Card from "./Card";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { get_teacher_by_id } from "../../../actions/teacher_action";

const TeacherDetails = () => {
  const { teacher_id } = useParams();
  const dispatch = useDispatch();
  const teacher = useSelector((state) => state.teacherReducer.teacher_details);
  console.log(teacher);
  useEffect(() => {
    if (teacher_id) dispatch(get_teacher_by_id(teacher_id));
  }, [teacher_id]);
  return (
    <>
      <Back
        title={teacher.name}
        style={{ height: "30vh", paddingBottom: "12%" }}
      />
      <div style={{ marginTop: "10%" }}></div>
      <Card teacher={teacher} />
    </>
  );
};

export default TeacherDetails;
