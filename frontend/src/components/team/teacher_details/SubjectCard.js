import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { get_subjects_for_teacher } from "../../../actions/subject_teacher_action";
import * as React from "react";
import { useTheme } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import SkipPreviousIcon from "@mui/icons-material/SkipPrevious";
import PlayArrowIcon from "@mui/icons-material/PlayArrow";
import SkipNextIcon from "@mui/icons-material/SkipNext";
import { get_image_url } from "../../../helper/util";
import { Button, CardActionArea } from "@mui/material";
import { useNavigate } from "react-router-dom";
import "./subject_card_style.css";

const expertiese_level_arr = ["Basic", "Intermediate", "Expert"];

const SubjectCard = ({ teacher_id, teacher_name }) => {
  const theme = useTheme();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const subject_list = useSelector(
    (state) => state.subjectTeacherReducer.teacher_subject_list
  );

  useEffect(() => {
    if (teacher_id) dispatch(get_subjects_for_teacher(teacher_id));
  }, [teacher_id]);

  const navigate_teacher_subject_info = (subject) => {
    navigate(
      `/subject/teacher_details/${subject.id}/${teacher_id}?subject_name=${subject.name}`
    );
  };

  return (
    <div className="subject-card-container">
      {subject_list.map((subject) => (
        <div key={subject.id} className="subject-card-item">
          <Card className="subject-card">
            <Box className="subject-card-info">
              <CardContent sx={{ flex: "1 0 auto" }}>
                <Typography
                  component="div"
                  variant="h6"
                  style={{ cursor: "pointer" }}
                  onClick={() => navigate_teacher_subject_info(subject)}
                >
                  {subject.name}
                </Typography>
                <Typography
                  variant="subtitle1"
                  color="text.secondary"
                  component="div"
                >
                  Expertiese Level :{" "}
                  {expertiese_level_arr[subject.level_of_knowledge - 1]}
                </Typography>
                <Typography
                  variant="subtitle1"
                  color="text.secondary"
                  component="div"
                >
                  Hourly Rate : {subject.hourly_rate} ₹
                </Typography>
              </CardContent>
              <CardActionArea>
                <Button
                  variant="outlined"
                  fullWidth
                  onClick={() =>
                    navigate(
                      `/add_request/${subject?.id}/${teacher_id}?name=${teacher_name}&subject_name=${subject?.name}&hourly_rate=${subject?.hourly_rate}&level_of_knowledge=${subject?.level_of_knowledge}`
                    )
                  }
                >
                  Book Now
                </Button>
              </CardActionArea>
            </Box>
            <div
              className="subject-card-image"
              onClick={() => navigate_teacher_subject_info(subject)}
            >
              {" "}
              <img
                src={get_image_url(subject.image_url)}
                alt="Live from space album cover"
                style={{ width: "100%", height: "100%", objectFit: "cover" }}
              />
            </div>
          </Card>
        </div>
      ))}
    </div>
  );
};

export default SubjectCard;
