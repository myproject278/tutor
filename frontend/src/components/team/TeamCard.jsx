import React, { useEffect } from "react"
import { team } from "../../dummydata"
import { useDispatch, useSelector } from "react-redux";
import { get_teacher_list } from "../../actions/teacher_action";
import { get_image_url } from "../../helper/util";
import { useNavigate } from "react-router-dom";


const TeamCard = () => {
  const dispatch = useDispatch()
  const navigate= useNavigate()
  useEffect(() => {
    dispatch(get_teacher_list())
  }, [])
  const teacher_list = useSelector(state => state.teacherReducer.teacher_list)
  
  return (
    <>
      {teacher_list.map((teacher) => (
        <div
          className="items shadow"
          onClick={() => navigate(`/teachers/${teacher.id}`)}
        >
          <div className="img">
            <img src={get_image_url(teacher.image_url)} alt="" />
          </div>
          <div className="details">
            <h2>{teacher.name}</h2>
            <p>{teacher.current_position}</p>
          </div>
        </div>
      ))}
    </>
  );
}

export default TeamCard
