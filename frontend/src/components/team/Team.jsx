import React from "react"
import Back from "../common/back/Back"
import TeamCard from "./TeamCard"
import "./team.css"

const Team = () => {
  return (
    <>
      <Back title='Teachers' style={{height:'30vh'}}/>
      <section className='team padding' style={{marginTop:'5%'}}>
        <div className='container grid'>
          <TeamCard />
        </div>
      </section>
     
    </>
  )
}

export default Team
