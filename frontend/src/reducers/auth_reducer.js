
import {
  AUTH_LOADING,
  AUTH_LOADING_COMPLETE,
  GET_USER_INFO_FAILURE,
  GET_USER_INFO_SUCCESFULL,
  LOGOUT,
  SET_AUTH_PATH,
  SIGN_IN_UP,
  SIGN_UP_IN_FAILURE,
} from "../constant";

const intialState = {
  user: {},
  error: null,
  token: null,
  authenticate: false,
  loading: false,
  path: "",
};
const authReducer = (state = intialState, action) => {
  switch (action.type) {
    case SIGN_IN_UP:
      state = {
        error: null,
        authenticate: true,
        user: action.payload.user,
        token: action.payload.token,
      };
      break;
    case LOGOUT:
      state = intialState;
      break;
    case SIGN_UP_IN_FAILURE:
      state = {
        user: {},
        error: action.error,
        authenticate: false,
        token: "",
      };
      break;
    case AUTH_LOADING:
      state.loading = true;
      break;
    case AUTH_LOADING_COMPLETE:
      state.loading = false;
      break;
    case SET_AUTH_PATH:
      state.path = action.data;
      break;
    case GET_USER_INFO_SUCCESFULL:
      state.user = action.data;
      break;
    case GET_USER_INFO_FAILURE:
      state.error = action.error;
      break;
  }
  return {
    ...state,
  };
};

export default authReducer