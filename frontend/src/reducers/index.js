import { combineReducers } from "redux";
import subjectReducer from "./subject_reducer";
import  subjectTeacherReducer from "./subject_teacher_reducer"
import authReducer from "./auth_reducer";
import requestReducer from './request_reducer'
import wishlistReducer from "./wishlist_reducers";
import transactionReducer from './transactionReducer'
import reviewReducer from "./review_reducer";
import addressReducer from "./address_ruducer";
import teacherReducer from "./teacher_reducer";
import postReducer from "./post_reducer";
import questionReducer from "./question_reducer";
import notificationReducer from "./notification_reducer";
import quizReducer from "./quiz_reducer";
import conversationReducer from "../chat_app/reducer/conversation_reducer";
import chatAppReducer from "../chat_app/reducer/chat_app_reducer";
const reudcers = combineReducers({
  subjectReducer,
  subjectTeacherReducer,
  authReducer,
  requestReducer,
  wishlistReducer,
  transactionReducer,
  reviewReducer,
  addressReducer,
  teacherReducer,
  postReducer,
  questionReducer,
  notificationReducer,
  quizReducer,
  conversationReducer,
  chatAppReducer
});

export default reudcers;
