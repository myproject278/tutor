import { FETCH_SUBJECTS } from "../constant"


const intial_state= {
    subjects : [],
    error :null ,
   
}

const subjectReducer = ( state= intial_state , action)=>{
    switch(action.type){
       case FETCH_SUBJECTS:
        state.subjects = action.data
        break;
       default:
        return state
    }
    return {
        ...state
    }
}
export default subjectReducer