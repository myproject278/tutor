import {
  GET_QUIZ_HISTORY_SUCCESS,
  GET_QUIZ_QUESTION_SUCCESS,
  GET_QUIZ_RESULT_SUCCESS,
  GET_QUIZ_SCORE_SUCCESS,
} from "../constant";

const intial_state = {
  questions: [],
  quiz: [],
  error: null,
  quiz: {},
  quiz_history: [],
  quiz_score_for_subjects: [],
};

const quizReducer = (state = intial_state, action) => {
  switch (action.type) {
    case GET_QUIZ_QUESTION_SUCCESS:
      state.questions = action.data;
      break;
    case GET_QUIZ_RESULT_SUCCESS:
      state.quiz = action.data;
      break;
    case GET_QUIZ_HISTORY_SUCCESS:
      state.quiz_history = action.data;
      break;
    case GET_QUIZ_SCORE_SUCCESS:
      state.quiz_score_for_subjects = action.data;
      break;

    default:
      return state;
  }
  return {
    ...state,
  };
};
export default quizReducer;
