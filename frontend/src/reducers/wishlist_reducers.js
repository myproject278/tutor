import { ADD_REQUEST, DELETE_PRODUCT_FROM_WISHLIST, GET_REQUEST_BY_USER_ID, GET_WISH_LIST_FOR_USER} from "../constant"


const intial_state= {
    wishlists : [],
    error :null 
}

const wishlistReducer = ( state= intial_state , action)=>{
    switch(action.type){
       case GET_WISH_LIST_FOR_USER:
        state.wishlists = action.data
        break;
       case DELETE_PRODUCT_FROM_WISHLIST:
         state.wishlists= state.wishlists.filter(w=>w.id!=action.product_id)
         break;
       default:
        return state
    }
    return {
        ...state
    }
}
export default wishlistReducer