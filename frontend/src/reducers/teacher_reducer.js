import { GET_TEACHERS_LIST, GET_TEACHER_BY_ID } from "../constant"


const intial_state= {
    teacher_list : [],
    teacher_details :{},
    error: null,
   
}

const TeacherReducer = ( state= intial_state , action)=>{
    switch(action.type){
       case GET_TEACHERS_LIST:
        state.teacher_list = action.data
            break;
        case GET_TEACHER_BY_ID:
            state.teacher_details = action.data
            break;
       default:
        return state
    }
    return {
        ...state
    }
}
export default TeacherReducer