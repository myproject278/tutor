import "./App.css"
import { useEffect } from "react";
import Header from "./components/common/header/Header";
import { Routes, Route } from "react-router-dom";
import SubjectHome from "./components/subjects/SubjectHome";
import Team from "./components/team/Team";
import Posts from "./components/posts/Posts";
import Contact from "./components/contact/Contact";
import Footer from "./components/common/footer/Footer";
import Home from "./components/home/Home";
import Auth from "./components/auth/auth";
import ProtectedRoute from "./routes/ProtectedRoute";
import { useDispatch, useSelector } from "react-redux";
import { isUserLoggedIn } from "./actions/auth_action";
import TeacherList from "./components/teacher/TeacherList";
import SubjectTeacherDetails from "./components/teacher/teacher_details/SubjectTeacherDetails";
import TeacherDetails from "./components/team/teacher_details/TeacherDetails";
import TutoringForm from "./components/request/TutoringForm";
import RequestList from "./components/request/show/RequestList";
import RequestDetails from "./components/request/details/RequestDetails";
import PostDetails from "./components/posts/post_details/PostDetails";
import Calendar from "./components/calender/Index";
import Quiz from "./components/quiz";
import QustionAnswer from "./components/question_answer/Main";
import AddQuestion from "./components/question_answer/AddQuestion";
import ViewQuestion from "./components/question_answer/ViewQuestion";
import Play from "./components/quiz/quiz/Play";
import QuizInstructions from "./components/quiz/quiz/QuizInstructions";
import QuizSummary from "./components/quiz/quiz/QuizSummary";
import ProfilePage from "./components/common/profile/Profile";
import ChatApp from "./chat_app/App";
import { BASE_URL, GET_NEW_MESSAGE } from "./constant";
import io from "socket.io-client";
import { addSocket } from "./chat_app/action/socket_action";

function App() {
  const dispatch = useDispatch();
  const { authenticate, user } = useSelector((state) => state.authReducer);
  const { conversations, current_conversation, current_messages } = useSelector(
    (state) => state.conversationReducer.direct_chat
  );
  const socket = useSelector((state) => state.chatAppReducer.socket);
  useEffect(() => {
    if (authenticate) {
      if (socket.socket == null && !socket.loading) {
        dispatch(addSocket(user));
      }
    }
  }, [authenticate]);
  useEffect(() => {
    dispatch(isUserLoggedIn());
  }, []);
  return (
    <>
      <Header />
      <Routes>
        <Route
          exact
          path="/"
          element={
            <ProtectedRoute>
              <ChatApp />
            </ProtectedRoute>
          }
        />

        <Route path="/auth" element={<Auth />} />
        <Route
          path="/profile"
          element={
            <ProtectedRoute>
              <ProfilePage />
            </ProtectedRoute>
          }
        />
        <Route
          path="/subjects"
          element={
            <ProtectedRoute>
              <SubjectHome />
            </ProtectedRoute>
          }
        />
        <Route
          path="/requests"
          element={
            <ProtectedRoute>
              <RequestList />
            </ProtectedRoute>
          }
        />
        <Route
          path="/requests/:request_id"
          element={
            <ProtectedRoute>
              <RequestDetails />
            </ProtectedRoute>
          }
        />
        <Route
          path="/subject/teacher/:id"
          element={
            <ProtectedRoute>
              <TeacherList />
            </ProtectedRoute>
          }
        />
        <Route
          path="/subject/teacher_details/:subject_id/:teacher_id"
          element={
            <ProtectedRoute>
              <SubjectTeacherDetails />
            </ProtectedRoute>
          }
        />
        <Route
          path="/teachers/:teacher_id"
          element={
            <ProtectedRoute>
              <TeacherDetails />
            </ProtectedRoute>
          }
        />
        <Route
          path="/add_request/:subject_id/:teacher_id"
          element={
            <ProtectedRoute>
              <TutoringForm />
            </ProtectedRoute>
          }
        />

        <Route
          path="/subjects/:id"
          element={
            <ProtectedRoute>
              <SubjectHome />
            </ProtectedRoute>
          }
        />
        <Route
          exact
          path="/teachers"
          element={
            <ProtectedRoute>
              <Team />
            </ProtectedRoute>
          }
        />
        <Route
          exact
          path="/calender"
          element={
            <ProtectedRoute>
              <Calendar />
            </ProtectedRoute>
          }
        />

        <Route
          exact
          path="/questions"
          element={
            <ProtectedRoute>
              <QustionAnswer />
            </ProtectedRoute>
          }
        />
        <Route
          exact
          path="/posts"
          element={
            <ProtectedRoute>
              <Posts />
            </ProtectedRoute>
          }
        />
        <Route
          exact
          path="/posts/:post_id"
          element={
            <ProtectedRoute>
              <PostDetails />
            </ProtectedRoute>
          }
        />
        <Route
          exact
          path="/contact"
          element={
            <ProtectedRoute>
              <Contact />
            </ProtectedRoute>
          }
        />
        <Route
          exact
          path="/add-question"
          element={
            <ProtectedRoute>
              <AddQuestion />
            </ProtectedRoute>
          }
        />
        <Route
          exact
          path="/question/:question_id"
          element={
            <ProtectedRoute>
              <ViewQuestion />
            </ProtectedRoute>
          }
        />
        <Route
          exact
          path="/quiz"
          element={
            <ProtectedRoute>
              <Quiz />
            </ProtectedRoute>
          }
        />
        <Route
          exact
          path="/quiz/play/:subject_id"
          element={
            <ProtectedRoute>
              <Play />
            </ProtectedRoute>
          }
        />
        <Route
          exact
          path="/quiz/instructions/:subject_id"
          element={
            <ProtectedRoute>
              <QuizInstructions />
            </ProtectedRoute>
          }
        />
        <Route
          exact
          path="/quiz/quizSummary/:quiz_id"
          element={
            <ProtectedRoute>
              <QuizSummary />
            </ProtectedRoute>
          }
        />
        <Route
          exact
          path="/quiz/quizSummary"
          element={
            <ProtectedRoute>
              <QuizSummary />
            </ProtectedRoute>
          }
        />
      </Routes>
      <Footer />
    </>
  );
}

export default App
