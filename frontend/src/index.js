
import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import { BrowserRouter } from "react-router-dom";
import store from './store/store';
import { Provider } from 'react-redux'
import { SnackbarProvider } from "notistack";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css"; // Import the default CSS
import { HelmetProvider } from "react-helmet-async";


const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <HelmetProvider>
      
  <Provider store={store}>
    <React.StrictMode>
      <BrowserRouter>
        <SnackbarProvider>
          <App />
        </SnackbarProvider>
        <ToastContainer />
      </BrowserRouter>
    </React.StrictMode>
      </Provider>
    </HelmetProvider>
);
