import {applyMiddleware, createStore} from 'redux'
import reudcers from '../reducers/index'
import thunkMiddleware from 'redux-thunk'
import {composeWithDevTools} from 'redux-devtools-extension'
const store=createStore(reudcers,composeWithDevTools(applyMiddleware(thunkMiddleware)))

export default store;
