-- MySQL dump 10.13  Distrib 8.0.33, for macos13.3 (arm64)
--
-- Host: localhost    Database: tutor
-- ------------------------------------------------------
-- Server version	8.0.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `address` (
  `id` int NOT NULL AUTO_INCREMENT,
  `city` varchar(100) DEFAULT NULL,
  `pincode` char(10) DEFAULT NULL,
  `home_no` varchar(20) DEFAULT NULL,
  `road` varchar(100) DEFAULT NULL,
  `extra` varchar(1000) DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `location` varchar(1000) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `answers`
--

DROP TABLE IF EXISTS `answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `answers` (
  `id` int NOT NULL AUTO_INCREMENT,
  `answer` varchar(1000) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `user_id` int DEFAULT NULL,
  `question_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answers`
--

LOCK TABLES `answers` WRITE;
/*!40000 ALTER TABLE `answers` DISABLE KEYS */;
INSERT INTO `answers` VALUES (1,'sasdfsafdljklk','2023-09-19 17:07:32',17,9),(2,'secound answre','2023-09-19 17:25:57',17,9),(3,'third answer','2023-09-19 17:26:09',17,9),(4,'third answer','2023-09-19 17:26:48',17,9),(5,'forth answer','2023-09-19 17:26:57',17,9),(6,'ewfrer2f','2023-09-26 19:12:43',17,1);
/*!40000 ALTER TABLE `answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `comments` (
  `post_id` int DEFAULT NULL,
  `student_id` int DEFAULT NULL,
  `comment` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (1,1,'kdkkk '),(1,1,'rada '),(1,1,'dhurr '),(1,1,'jaal '),(1,1,'sdfasdfasfdasdfasfsaf'),(1,17,'adf');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doubts`
--

DROP TABLE IF EXISTS `doubts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `doubts` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(500) DEFAULT NULL,
  `body` text,
  `tags` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `user_id` int DEFAULT NULL,
  `active` tinyint DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doubts`
--

LOCK TABLES `doubts` WRITE;
/*!40000 ALTER TABLE `doubts` DISABLE KEYS */;
INSERT INTO `doubts` VALUES (1,'ERgefg','<p>werfwe<code>sdvscvsdfv</code></p>','[\"asdf\"]','2023-09-26 19:12:29',17,1);
/*!40000 ALTER TABLE `doubts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enterprises`
--

DROP TABLE IF EXISTS `enterprises`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `enterprises` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `system_flag` tinyint(1) DEFAULT NULL,
  `active_flag` tinyint(1) DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_field_1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_field_2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_field_3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_field_4` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_field_5` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_field_6` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_field_7` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_field_8` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_field_9` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_field_10` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_field_11` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_field_12` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_field_13` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_field_14` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_field_15` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_field_16` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_field_17` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_field_18` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_field_19` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_field_20` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by_id` int DEFAULT NULL,
  `updated_by_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enterprises`
--

LOCK TABLES `enterprises` WRITE;
/*!40000 ALTER TABLE `enterprises` DISABLE KEYS */;
INSERT INTO `enterprises` VALUES (1,'Global Enterprise','GLOBAL',1,1,'Default unified enterprise',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2006-08-28 02:40:04','2019-09-18 08:41:09',1,8),(2,'CC','CC',NULL,1,'Axiata Corporate Center Enterprise [Microsoft D365]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-05-23 02:14:31','2019-05-23 02:14:31',8,8),(3,'Celcom','Celcom',NULL,1,'Enterprise for Celcom SAP ECC',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-06-18 03:19:41','2019-06-18 03:19:41',8,8),(4,'XL','XL',NULL,1,'Enterprise for XL SAP ECC',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-10-16 03:41:25','2019-10-16 03:41:25',1060,1060),(5,'NCell','NCell',NULL,1,'Enterprise for NCell',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-02 08:10:59','2022-07-25 16:45:05',7173,5911),(6,'Robi','Robi',NULL,1,'Enterprise for Robi S4 HANA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-11-25 13:33:38','2020-11-25 13:33:38',7173,7173),(7,'Dialog','Dialog',NULL,1,'Enterprise for Dialog',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-01-25 07:12:17','2021-01-25 07:12:17',5483,5483),(8,'Smart','Smart',NULL,1,'Enterprise for SMART SAP S4 HANA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-12-29 04:24:00','2021-12-29 04:24:00',20009,20009);
/*!40000 ALTER TABLE `enterprises` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `friendships`
--

DROP TABLE IF EXISTS `friendships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `friendships` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user1_id` int DEFAULT NULL,
  `user2_id` int DEFAULT NULL,
  `status` enum('pending','accepted','rejected') DEFAULT 'pending',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user1_id` (`user1_id`),
  KEY `user2_id` (`user2_id`),
  CONSTRAINT `friendships_ibfk_1` FOREIGN KEY (`user1_id`) REFERENCES `user` (`id`),
  CONSTRAINT `friendships_ibfk_2` FOREIGN KEY (`user2_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `friendships`
--

LOCK TABLES `friendships` WRITE;
/*!40000 ALTER TABLE `friendships` DISABLE KEYS */;
INSERT INTO `friendships` VALUES (1,17,18,'accepted','2023-10-08 23:26:35','2023-10-08 23:26:35');
/*!40000 ALTER TABLE `friendships` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;

/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `notifications` (
  `id` int NOT NULL AUTO_INCREMENT,
  `message` varchar(1000) DEFAULT NULL,
  `redirect_link` varchar(200) DEFAULT NULL,
  `is_read` tinyint DEFAULT '0',
  `user_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
INSERT INTO `notifications` VALUES (1,'karan request for teaching DS In java','/requests/27',1,1),(2,'karan request for teaching DS In java','/requests/27',1,1),(3,'karan request for teaching DS In java','/requests/27',1,17),(4,'karan request for teaching DS In java','/requests/27',1,17),(5,'karan request for teaching DS In java','/requests/28',0,NULL),(6,'karan request for teaching DS In java','/requests/29',0,NULL),(7,'Karan request for teaching Core Java Theory','/requests/30',0,NULL),(8,'Karan request for teaching Core Java Theory','/requests/31',0,NULL),(9,'Karan request for teaching DS In java','/requests/32',0,NULL),(10,'Karan request for teaching DS In java','/requests/33',0,NULL),(11,'Karan request for teaching DS In java','/requests/34',0,NULL),(12,'Karan request for teaching DS In java','/requests/47',0,NULL),(13,'Karan request for teaching DS In java','/requests/48',0,NULL),(14,'Karan are payment for teaching the DS In java','/requests/49',0,NULL),(15,'Karan are payment for teaching the DS In java','/requests/49',0,NULL),(16,'Karan are payment for teaching the DS In java','/requests/49',0,NULL),(17,'Karan are payment for teaching the DS In java','/requests/49',1,1),(18,'Karan are payment for teaching the DS In java','/requests/49',0,1),(19,'Karan are payment for teaching the DS In java','/requests/49',0,1),(20,'Karan are payment for teaching the DS In java','/requests/49',0,1),(21,'Karan are cancel request for DS In java','/requests/48',0,1),(22,'Karan are cancel request for DS In java','/requests/47',1,17),(23,'Karan are cancel request for DS In java','/requests/46',0,1),(24,'Karan are cancel request for DS In java','/requests/42',0,1),(25,'Karan are cancel request for DS In java','/requests/45',0,1),(26,'Karan are cancel request for DS In java','/requests/40',0,1),(27,'Karan are cancel request for DS In java','/requests/40',0,1),(28,'Karan are cancel request for DS In java','/requests/40',0,1),(29,'Karan are cancel request for DS In java','/requests/40',0,1),(30,'Karan are cancel request for DS In java','/requests/40',0,1),(31,'Karan are cancel request for DS In java','/requests/40',1,1),(32,'Karan are cancel request for Project Code Help','/requests/54',0,1),(33,'karan are payment for teaching the DS In java','/requests/1',0,18),(34,'karan are payment for teaching the DS In java','/requests/1',0,18),(35,'karan are payment for teaching the DS In java','/requests/1',0,18),(36,'karan are payment for teaching the DS In java','/requests/1',0,18),(37,'karan are payment for teaching the DS In java','/requests/1',0,18),(38,'karan are payment for teaching the DS In java','/requests/1',0,18),(39,'karan are payment for teaching the DS In java','/requests/1',0,18),(40,'karan are payment for teaching the DS In java','/requests/1',0,18),(41,'karan are payment for teaching the DS In java','/requests/1',0,18),(42,'karan are payment for teaching the DS In java','/requests/1',0,18);
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `posts` (
  `id` int NOT NULL AUTO_INCREMENT,
  `header` varchar(1000) DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `date` datetime DEFAULT CURRENT_TIMESTAMP,
  `teacher_id` int DEFAULT NULL,
  `image_urls` varchar(800) DEFAULT NULL,
  `tags` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,'learn React with akash ','learn react with me , i am one of the best react developer who can develop best react app , you can learn with me a basic to advance react ','2023-08-06 09:54:36',18,'[{\"img\":\"5-b0aJXE--auth.png\"},{\"img\":\"PsaWe2cDfQ-Screenshot 2023-08-03 at 11.08.05 AM.png\"}]','[\"java\",\"akash nikam\"]'),(2,'learn mern with me ','learn mern with me , i am one of the best react developer who can develop best react app , you can learn with me a basic to advance react ','2023-08-06 09:55:05',18,'[{\"img\":\"cRRkHpZGq-auth.png\"},{\"img\":\"U1TGuOV-IW-Screenshot 2023-08-03 at 11.08.05 AM.png\"}]','[\"java\",\"akash nikam\"]'),(3,'learn java with me ','learn mern with me , i am one of the best react developer who can develop best react app , you can learn with me a basic to advance react ','2023-08-06 09:55:09',18,'[{\"img\":\"ouST2L2oM-auth.png\"},{\"img\":\"JCXrl0BJEG-Screenshot 2023-08-03 at 11.08.05 AM.png\"}]','[\"java\",\"akash nikam\"]'),(4,'learn java with me ','learn mern with me , i am one of the best react developer who can develop best react app , you can learn with me a basic to advance react ','2023-08-06 10:57:16',18,'[{\"img\":\"q2OWRyP7t-auth.png\"},{\"img\":\"K4HPjyWmSm-Screenshot 2023-08-03 at 11.08.05 AM.png\"}]','[\"java\",\"akash nikam\"]'),(5,'learn java with me ','learn mern with me , i am one of the best react developer who can develop best react app , you can learn with me a basic to advance react ','2023-08-06 10:57:17',1,'[{\"img\":\"WKJ6ln4tb-auth.png\"},{\"img\":\"ueb1AGURJY-Screenshot 2023-08-03 at 11.08.05 AM.png\"}]','[\"java\",\"akash nikam\"]'),(6,'learn java with me ','learn mern with me , i am one of the best react developer who can develop best react app , you can learn with me a basic to advance react ','2023-08-06 10:57:18',1,'[{\"img\":\"butN3zVAM-auth.png\"},{\"img\":\"iW4-Cv6f7K-Screenshot 2023-08-03 at 11.08.05 AM.png\"}]','[\"java\",\"akash nikam\"]'),(7,'learn java with me ','learn mern with me , i am one of the best react developer who can develop best react app , you can learn with me a basic to advance react ','2023-08-06 11:23:18',1,'[{\"img\":\"7HQZjN9do-auth.png\"},{\"img\":\"-pvzV0uMJ6-Screenshot 2023-08-03 at 11.08.05 AM.png\"}]','[\"java\",\"akash nikam\"]'),(8,'learn java with me ','learn mern with me , i am one of the best react developer who can develop best react app , you can learn with me a basic to advance react ','2023-08-06 19:13:11',1,'[{\"img\":\"LhCfKQbDo-auth.png\"},{\"img\":\"_kMv0uoink-Screenshot 2023-08-03 at 11.08.05 AM.png\"}]','[\"react\",\"akash nikam\"]'),(9,'header ','asldfjasldjf','2023-08-17 15:30:59',1,'[]','[\"akash\"]'),(10,'asdf','asdf','2023-08-17 15:32:11',1,'[]','[\"asdf\"]'),(11,'asdf','asdf','2023-08-17 15:34:07',1,'[{\"img\":\"F8Y_Fh7RD-Screenshot 2023-08-17 at 9.40.34 AM.png\"},{\"img\":\"gaOqNz1b0D-Screenshot 2023-08-14 at 9.22.04 PM.png\"}]','[]');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `questions` (
  `question_id` int NOT NULL AUTO_INCREMENT,
  `question_text` text NOT NULL,
  `option_a` text NOT NULL,
  `option_b` text NOT NULL,
  `option_c` text NOT NULL,
  `option_d` text NOT NULL,
  `correct_option` char(1) NOT NULL,
  `subject_id` int DEFAULT NULL,
  PRIMARY KEY (`question_id`)
) ENGINE=InnoDB AUTO_INCREMENT=161 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questions`
--

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
INSERT INTO `questions` VALUES (1,'What is the correct way to declare a variable in Java?','A) var name;','B) int name;','C) variable name;','D) declare name;','B',1),(2,'Which data type is used to store whole numbers in Java?','A) float','B) double','C) int','D) char','C',1),(3,'What is the result of 5 + 3 * 2 in Java?','A) 10','B) 16','C) 11','D) 13','B',1),(4,'Which Java keyword is used to define a subclass of a class?','A) super','B) subclass','C) extends','D) inherits','C',1),(5,'What is the purpose of the break statement in Java?','A) To end the current loop or switch statement and transfer control to the statement after it.','B) To start a new loop.','C) To print the word \"break\" to the console.','D) To terminate the entire Java program.','A',1),(6,'Which access modifier allows a class or method to be accessed from any other class?','A) public','B) private','C) protected','D) default','A',1),(7,'What is the output of the following code snippet?\n\nint x = 5;\nint y = 10;\nSystem.out.println(x + y + \" is the sum.\");','A) 15 is the sum.','B) 510 is the sum.','C) 5 + 10 is the sum.','D) Error: Incompatible types.','B',1),(8,'Which Java loop is ideal for situations where you want to execute a block of code a specific number of times?','A) for loop','B) while loop','C) do-while loop','D) if statement','A',1),(9,'What does the static keyword indicate when applied to a Java method?','A) The method can only be called from within the same class.','B) The method can be called without creating an instance of the class.','C) The method is automatically called when the program starts.','D) The method is not allowed to have parameters.','B',1),(10,'Which Java class is the superclass of all classes?','A) Object','B) Class','C) Superclass','D) Parent','A',1),(11,'What is the output of the following code?\n\npublic static void main(String[] args) {\n    int x = 5;\n    System.out.println(x++); // Post-increment\n    System.out.println(++x); // Pre-increment\n}','A) 5\n   6','B) 6\n   7','C) 5\n   5','D) 6\n   6','B',1),(12,'What is the purpose of the `this` keyword in Java?','A) It refers to the current instance of the class.','B) It is used to create a new object.','C) It represents the parent class.','D) It is a reserved keyword with no specific purpose.','A',1),(13,'Which Java keyword is used to create an instance of a class?','A) create','B) new','C) instance','D) class','B',1),(14,'What is the purpose of the `super` keyword in Java?','A) It is used to define a superclass.','B) It refers to the current instance of the class.','C) It calls the constructor of the superclass.','D) It is used to create a new object.','C',1),(15,'Which Java operator is used for logical AND?','A) &','B) &&','C) ||','D) !','B',1),(16,'What is the output of the following code snippet?\n\nString name = \"Java\";\nSystem.out.println(name.length());','A) Java','B) J','C) 4','D) 6','C',1),(17,'In Java, what is the purpose of the `try-catch` block?','A) To define a new class.','B) To declare variables.','C) To handle exceptions.','D) To create objects.','C',1),(18,'Which Java class is used to read input from the user?','A) Scanner','B) Reader','C) InputReader','D) InputStreamReader','A',1),(19,'What is the purpose of the `toString` method in Java?','A) To convert a string to an integer.','B) To convert an object to a string representation.','C) To concatenate two strings.','D) To compare two strings.','B',1),(20,'Which Java keyword is used to exit from a loop?','A) exit','B) leave','C) break','D) continue','C',1),(21,'What is the purpose of the `finally` block in a Java `try-catch-finally` structure?','A) It defines a new variable.','B) It handles exceptions.','C) It is executed regardless of whether an exception is thrown or not.','D) It terminates the program.','C',1),(22,'Which Java keyword is used to prevent method overriding?','A) prevent','B) final','C) override','D) super','B',1),(23,'What is the default value of an uninitialized integer variable in Java?','A) 0','B) 1','C) -1','D) null','A',1),(24,'Which Java class is used to represent a date and time?','A) DateTime','B) TimeDate','C) Date','D) DateTimeFormatter','C',1),(25,'In Java, what is the purpose of the `break` statement in a loop?','A) It exits the loop and transfers control to the next iteration.','B) It restarts the loop from the beginning.','C) It continues to the next statement outside the loop.','D) It deletes the loop.','A',1),(26,'Which data type is used to store a single character in Java?','A) char','B) character','C) string','D) letter','A',1),(27,'What is the purpose of the `static` keyword in Java?','A) It defines a new class.','B) It allows access to class-level variables and methods without creating an instance of the class.','C) It creates a new object.','D) It restricts access to class members.','B',1),(28,'Which Java operator is used for bitwise OR?','A) |','B) ||','C) &','D) &&','A',1),(29,'What is the output of the following code snippet?\n\nString s1 = \"Hello\";\nString s2 = \"World\";\nSystem.out.println(s1 + s2);','A) Hello','B) World','C) HelloWorld','D) Error: Incompatible types.','C',1),(30,'Which Java class is used to read input from a file?','A) File','B) FileReader','C) FileInputStream','D) Scanner','B',1),(31,'What is the purpose of the `equals` method in Java?','A) To compare two primitive data types.','B) To determine if two objects have the same reference.','C) To compare the contents of two objects for equality.','D) To check if a variable is null.','C',1),(32,'In Java, what does the `throw` keyword do?','A) It catches exceptions.','B) It throws an exception explicitly.','C) It defines a new class.','D) It terminates the program.','B',1),(33,'What is the output of the following code snippet?\n\nint x = 5;\nint y = x++ + ++x;\nSystem.out.println(y);','A) 10','B) 11','C) 12','D) 13','C',1),(34,'Which Java class is used to implement multi-threading?','A) Runnable','B) Threaded','C) Multithread','D) Concurrent','A',1),(35,'What is the purpose of the `continue` statement in a loop in Java?','A) It exits the loop and transfers control to the next iteration.','B) It restarts the loop from the beginning.','C) It continues to the next statement outside the loop.','D) It deletes the loop.','A',1),(36,'In Java, what is the maximum value that can be stored in a byte data type?','A) 128','B) 255','C) 32767','D) 2147483647','B',1),(37,'What is the purpose of the `instanceof` operator in Java?','A) It checks if a class is abstract.','B) It checks if an object is an instance of a particular class or interface.','C) It compares two objects for equality.','D) It checks if a class is final.','B',1),(38,'What is the output of the following code?\n\nString s = \"Hello\";\nSystem.out.println(s.charAt(0));','A) Hello','B) H','C) e','D) 0','B',1),(39,'In Java, what is the purpose of the `default` label in a switch statement?','A) It specifies the default case to execute when no other case matches.','B) It defines the default value of a variable.','C) It represents the starting point of the switch statement.','D) It indicates a compilation error.','A',1),(40,'What is the purpose of the `static` block in a Java class?','A) To declare class-level variables.','B) To create instances of the class.','C) To define constructors.','D) To initialize class-level variables or perform one-time setup.','D',1),(41,'Which Java method is called when an object is no longer referenced and is eligible for garbage collection?','A) delete()','B) finalize()','C) dispose()','D) collect()','B',1),(42,'What is the purpose of the `assert` statement in Java?','A) It is used for exception handling.','B) It is used to check the equality of two objects.','C) It is used to test assertions and validate assumptions.','D) It is used for loop control.','C',1),(43,'In Java, what is the purpose of the `transient` keyword when applied to a variable?','A) It indicates that the variable cannot be changed.','B) It specifies that the variable is shared among multiple threads.','C) It indicates that the variable should not be serialized.','D) It indicates that the variable is immutable.','C',1),(44,'Which Java interface is used to represent a set of related methods with no implementation?','A) AbstractInterface','B) EmptyInterface','C) Interface','D) None of the above','C',1),(45,'What is the purpose of the `volatile` keyword in Java?','A) It prevents a variable from being modified.','B) It ensures that a variable is initialized before it is used.','C) It specifies that a variable is constant.','D) It indicates that a variable may be modified by multiple threads.','D',1),(46,'In Java, what is the role of the `hashCode` method?','A) To compare two objects for equality.','B) To calculate the hash code of an object.','C) To convert an object to a string representation.','D) To create a new object.','B',1),(47,'Which Java class is used to format date and time values?','A) DateFormat','B) DateTimeFormatter','C) SimpleDateFormat','D) DateTimeFormat','C',1),(48,'In Java, which keyword is used to define a constant?','A) constant','B) static','C) final','D) const','C',1),(49,'What is the purpose of the `Math` class in Java?','A) To perform mathematical operations and calculations.','B) To define custom mathematical functions.','C) To create random numbers.','D) To format numbers for display.','A',1),(50,'Which Java exception is thrown when an arithmetic operation results in a value that is too large to represent?','A) StackOverflowError','B) NumberFormatException','C) ArithmeticException','D) OutOfMemoryError','C',1),(51,'What is the purpose of the `interface` keyword in Java?','A) To define a new class.','B) To declare a class as abstract.','C) To define a blueprint for classes that implement it.','D) To create an instance of a class.','C',1),(52,'In Java, what is the role of the `static` block?','A) To declare static variables.','B) To create instances of the class.','C) To initialize static variables or perform one-time setup.','D) To define constructors.','C',1),(53,'What is the result of the following code?\n\nint x = 10;\nint y = 20;\nint z = (x > y) ? x : y;','A) 10','B) 20','C) 30','D) The code will not compile.','B',1),(54,'What is the purpose of the `import` statement in Java?','A) To include external libraries and classes in your program.','B) To export classes from your program.','C) To define variables.','D) To create instances of classes.','A',1),(55,'In Java, what is the purpose of the `for-each` loop?','A) To execute a block of code repeatedly.','B) To iterate over elements of an array or collection.','C) To define classes.','D) To perform mathematical operations.','B',1),(56,'What is the output of the following code?\n\nint i = 5;\nwhile (i > 0) {\n    System.out.print(i + \" \");\n    i--;\n}','A) 5 4 3 2 1','B) 1 2 3 4 5','C) 0 1 2 3 4','D) Infinite loop','A',1),(57,'What is the purpose of the `StringBuilder` class in Java?','A) To format date and time values.','B) To create instances of classes.','C) To build and manipulate strings efficiently.','D) To perform mathematical calculations.','C',1),(58,'Which Java class is used for reading character-based input from a file?','A) FileInputStream','B) FileReader','C) BufferedReader','D) InputStreamReader','B',1),(59,'What is the result of the following code?\n\nString s1 = \"Hello\";\nString s2 = \"Hello\";\nSystem.out.println(s1 == s2);','A) true','B) false','C) Error: Incompatible types.','D) Compilation error','A',1),(60,'In Java, which class is used to create a thread?','A) Runnable','B) Thread','C) Process','D) Executor','B',1),(61,'What is the purpose of the `synchronized` keyword in Java?','A) To specify that a method cannot be overridden.','B) To prevent multiple threads from accessing a block of code concurrently.','C) To declare a method as abstract.','D) To define constructors.','B',1),(62,'What is the output of the following code?\n\nSystem.out.println(Math.abs(-5));','A) 5','B) -5','C) 0','D) Error: Incompatible types.','A',1),(63,'In Java, what is the role of the `public` access modifier?','A) It restricts access to class members.','B) It allows access from any other class.','C) It defines a new package.','D) It creates a new object.','B',1),(64,'What is the purpose of the `package` statement in Java?','A) To declare variables.','B) To create instances of classes.','C) To specify the name of the package that contains the class.','D) To define constructors.','C',1),(65,'Which Java class is used to parse and format XML documents?','A) XMLParser','B) XMLFormatter','C) XMLDocument','D) None of the above','D',1),(66,'What is the purpose of the `try-with-resources` statement in Java?','A) To define new resources.','B) To catch exceptions.','C) To ensure that resources are closed after being used.','D) To create instances of classes.','C',1),(67,'In Java, what is the purpose of the `this` keyword in a constructor?','A) It refers to the current instance of the class.','B) It creates a new instance of the class.','C) It initializes static variables.','D) It defines constructors.','A',1),(68,'What is the output of the following code snippet?\n\nString s = \"Hello, World!\";\nSystem.out.println(s.substring(7, 12));','A) Hello','B) World','C) , Wo','D) Error: Incompatible types.','B',1),(69,'What is the purpose of the `super` keyword in a constructor in Java?','A) It refers to the parent class.','B) It initializes instance variables.','C) It creates a new object.','D) It defines constructors.','A',1),(70,'In Java, what is the purpose of the `enum` keyword?','A) To define a new interface.','B) To create a new class.','C) To declare an enumeration (a set of named values).','D) To define a new method.','C',1),(71,'What is the result of the following code?\n\nint x = 5;\nint y = 2;\nint result = x % y;','A) 5','B) 2','C) 1','D) 0','C',1),(72,'In Java, which collection class is typically used to store elements in key-value pairs?','A) ArrayList','B) LinkedList','C) HashMap','D) Stack','C',1),(73,'What is the purpose of the `break` statement in a switch statement in Java?','A) It continues to the next case.','B) It terminates the loop.','C) It exits the switch statement.','D) It prints the word \"break\".','C',1),(74,'In Java, what is the purpose of the `File` class?','A) To create new files.','B) To read and write binary data.','C) To manipulate text files.','D) To work with files and directories.','D',1),(75,'What is the result of the following code?\n\nint x = 10;\nx += 5;','A) 15','B) 10','C) 5','D) Error: Incompatible types.','A',1),(76,'What is the purpose of the `try-catch` statement in Java?','A) To define a new class.','B) To declare variables.','C) To handle exceptions.','D) To create objects.','C',1),(77,'In Java, what is the role of the `instanceof` operator?','A) It checks if an object is an instance of a particular class or interface.','B) It compares two strings for equality.','C) It creates a new object.','D) It declares a variable.','A',1),(78,'What is the output of the following code snippet?\n\nString s = \"Java\";\nSystem.out.println(s.indexOf(\"a\"));','A) 0','B) 1','C) -1','D) Error: Incompatible types.','B',1),(79,'In Java, what is the purpose of the `String` class?','A) To perform mathematical calculations.','B) To define constructors.','C) To work with text strings.','D) To create instances of classes.','C',1),(80,'Which Java keyword is used to define a new class?','A) new','B) class','C) define','D) create','B',1),(81,'What is the purpose of the `break` statement in a loop?','A) It exits the loop and transfers control to the next iteration.','B) It continues to the next statement outside the loop.','C) It restarts the loop from the beginning.','D) It deletes the loop.','A',1),(82,'In Java, what is the result of dividing an integer by zero?','A) 0','B) 1','C) Infinity','D) Error: Division by zero','D',1),(83,'What is the purpose of the `new` keyword in Java?','A) To define a new class.','B) To create a new object.','C) To declare a variable.','D) To perform mathematical calculations.','B',1),(84,'What is the output of the following code snippet?\n\nint x = 5;\nint y = 3;\nSystem.out.println(x / y);','A) 8','B) 1.6667','C) 1','D) Error: Incompatible types.','C',1),(85,'In Java, which operator is used for logical OR?','A) &','B) &&','C) ||','D) !','C',1),(86,'What is the purpose of the `NullPointerException` in Java?','A) To indicate an invalid cast operation.','B) To represent an invalid array index.','C) To handle division by zero.','D) To indicate an attempt to access an object that is null.','D',1),(87,'In Java, what is the purpose of the `default` access modifier?','A) It restricts access to class members.','B) It allows access from any other class.','C) It specifies the default value of a variable.','D) It defines a new class.','B',1),(88,'What is the output of the following code?\n\nint i = 0;\nwhile (i < 5) {\n    System.out.print(i + \" \");\n    i++;\n}','A) 0 1 2 3 4','B) 4 3 2 1 0','C) 5 4 3 2 1','D) Infinite loop','A',1),(89,'In Java, what is the purpose of the `final` keyword?','A) To declare a variable as constant.','B) To define constructors.','C) To create a new object.','D) To declare a class as abstract.','A',1),(90,'What is the output of the following code?\n\nSystem.out.println(\"Hello, \" + \"World!\");','A) Hello,','B) World!','C) Hello, World!','D) Error: Incompatible types.','C',1),(91,'In Java, what is the purpose of the `double` data type?','A) To store a single character.','B) To store whole numbers.','C) To store decimal numbers.','D) To store true or false values.','C',1),(92,'What is the result of the following code?\n\nint x = 10;\nint y = x--;','A) 11','B) 10','C) 9','D) Error: Incompatible types.','B',1),(93,'In Java, what is the purpose of the `public` keyword in a class definition?','A) To declare a class as abstract.','B) To specify the default value of a variable.','C) To allow access from any other class.','D) To restrict access to class members.','C',1),(94,'What is the output of the following code snippet?\n\nString s = \"Hello, World!\";\nSystem.out.println(s.toUpperCase());','A) hello, world!','B) HELLO, WORLD!','C) Hello, World!','D) Error: Incompatible types.','B',1),(95,'In Java, what is the purpose of the `instanceof` operator?','A) It checks if an object is an instance of a particular class or interface.','B) It compares two strings for equality.','C) It creates a new object.','D) It declares a variable.','A',1),(96,'What is the purpose of the `continue` statement in a loop in Java?','A) It exits the loop and transfers control to the next iteration.','B) It continues to the next statement outside the loop.','C) It restarts the loop from the beginning.','D) It deletes the loop.','A',1),(97,'What is the result of the following code?\n\nString s = \"Java\";\nSystem.out.println(s.charAt(10));','A) J','B) a','C) v','D) Error: String index out of range.','D',1),(98,'In Java, what is the purpose of the `else` keyword?','A) To declare variables.','B) To define constructors.','C) To specify the default value of a variable.','D) To specify an alternative block of code to execute.','D',1),(99,'What is the output of the following code?\n\nint x = 5;\nint y = 3;\nSystem.out.println(x == y);','A) true','B) false','C) Error: Incompatible types.','D) Compilation error','B',1),(100,'In Java, which operator is used for equality comparison?','A) ==','B) !=','C) >','D) <','A',1),(101,'What is the purpose of the `static` keyword in Java?','A) To declare a variable as constant.','B) To specify the default value of a variable.','C) To define a class-level variable or method.','D) To restrict access to class members.','C',1),(102,'In Java, what is the result of dividing a double by zero?','A) 0','B) 1','C) Infinity','D) Error: Division by zero','C',1),(103,'What is the output of the following code?\n\nint x = 10;\nint y = 5;\nif (x > y) {\n    System.out.println(\"x is greater\");\n} else {\n    System.out.println(\"y is greater\");\n}','A) x is greater','B) y is greater','C) Both x and y are equal','D) Error: Incompatible types.','A',1),(104,'In Java, which keyword is used to create a new instance of a class?','A) new','B) class','C) create','D) instance','A',1),(105,'What is the result of the following code?\n\nint x = 5;\nint y = x + 3;','A) 5','B) 8','C) 3','D) Error: Incompatible types.','B',1),(106,'In Java, what is the purpose of the `do-while` loop?','A) To execute a block of code repeatedly until a condition is true.','B) To define a class.','C) To declare variables.','D) To perform mathematical calculations.','A',1),(107,'What is the output of the following code snippet?\n\nString s1 = \"Java\";\nString s2 = \"java\";\nSystem.out.println(s1.equals(s2));','A) true','B) false','C) Error: Incompatible types.','D) Compilation error','B',1),(108,'In Java, which keyword is used to define a class as a subclass of another class?','A) extend','B) superclass','C) implements','D) subclass','A',1),(109,'What is the purpose of the `continue` statement in a loop in Java?','A) It exits the loop and transfers control to the next iteration.','B) It continues to the next statement outside the loop.','C) It restarts the loop from the beginning.','D) It deletes the loop.','A',1),(110,'What is the output of the following code?\n\nString s = \"Java\";\nSystem.out.println(s.length());','A) 5','B) 4','C) 3','D) Error: Incompatible types.','A',1),(111,'In Java, which keyword is used to define a block of code that should be executed if a condition is true?','A) if','B) else','C) switch','D) case','A',1),(112,'What is the result of the following code?\n\nint x = 5;\nint y = 3;\nint result = (x > y) ? x : y;','A) 5','B) 3','C) 8','D) Error: Incompatible types.','A',1),(113,'In Java, what is the purpose of the `public` access modifier?','A) It restricts access to class members.','B) It allows access from any other class.','C) It defines a new package.','D) It creates a new object.','B',1),(114,'What is the purpose of the `throw` statement in Java?','A) To catch exceptions.','B) To specify a case in a switch statement.','C) To create a new object.','D) To explicitly throw an exception.','D',1),(115,'What is the output of the following code?\n\nint x = 5;\nint y = 3;\nSystem.out.println(x > y);','A) true','B) false','C) Error: Incompatible types.','D) Compilation error','A',1),(116,'In Java, what is the purpose of the `finally` block?','A) To specify the default value of a variable.','B) To handle exceptions.','C) To declare a class as abstract.','D) To declare variables.','B',1),(117,'What is the result of the following code?\n\nint x = 10;\nint y = 5;\nSystem.out.println(x != y);','A) true','B) false','C) Error: Incompatible types.','D) Compilation error','A',1),(118,'In Java, which operator is used for bitwise AND?','A) &','B) &&','C) |','D) ||','A',1),(119,'What is the purpose of the `StringBuilder` class in Java?','A) To format date and time values.','B) To create instances of classes.','C) To build and manipulate strings efficiently.','D) To perform mathematical calculations.','C',1),(120,'In Java, what is the purpose of the `break` statement in a loop?','A) It exits the loop and transfers control to the next iteration.','B) It continues to the next statement outside the loop.','C) It restarts the loop from the beginning.','D) It deletes the loop.','A',1),(121,'What is the output of the following code?\n\nint x = 5;\nint y = 3;\nSystem.out.println(x >= y);','A) true','B) false','C) Error: Incompatible types.','D) Compilation error','A',1),(122,'In Java, which operator is used for logical NOT?','A) &','B) &&','C) |','D) !','D',1),(123,'What is the result of the following code?\n\nString s = \"Java\";\nSystem.out.println(s.substring(1, 3));','A) Jav','B) av','C) va','D) Error: Incompatible types.','B',1),(124,'In Java, which class is used to create a random number generator?','A) Random','B) Math','C) Randomizer','D) RNG','A',1),(125,'What is the purpose of the `do-while` loop in Java?','A) To execute a block of code repeatedly until a condition is false.','B) To declare variables.','C) To create new objects.','D) To perform mathematical calculations.','A',1),(126,'In Java, what is the purpose of the `break` statement in a switch statement?','A) It continues to the next case.','B) It terminates the loop.','C) It exits the switch statement.','D) It prints the word \"break\".','C',1),(127,'What is the result of the following code?\n\nint x = 5;\nint y = 2;\nint result = x % y;','A) 5','B) 2','C) 1','D) 0','C',1),(128,'In Java, which class is used to read character-based input from the keyboard?','A) Scanner','B) InputStreamReader','C) FileReader','D) BufferedReader','A',1),(129,'What is the purpose of the `else` keyword in Java?','A) To declare variables.','B) To define constructors.','C) To specify the default value of a variable.','D) To specify an alternative block of code to execute.','D',1),(130,'In Java, what is the purpose of the `break` statement in a loop?','A) It exits the loop and transfers control to the next iteration.','B) It continues to the next statement outside the loop.','C) It restarts the loop from the beginning.','D) It deletes the loop.','A',1),(131,'What is the purpose of the `equals` method in Java?','A) To compare two primitive data types for equality.','B) To compare two objects for structural equality.','C) To create a new object.','D) To define constructors.','B',1),(132,'In Java, what is the purpose of the `default` keyword in a switch statement?','A) To specify the default value of a variable.','B) To create a new object.','C) To declare variables.','D) To specify the code to execute if no case matches.','D',1),(133,'What is the output of the following code?\n\nint x = 5;\nint y = 2;\nSystem.out.println(x / y);','A) 5','B) 2.5','C) 2','D) Error: Incompatible types.','C',1),(134,'In Java, which class is used to create a graphical user interface (GUI) window?','A) JFrame','B) Scanner','C) String','D) ArrayList','A',1),(135,'What is the result of the following code?\n\nint x = 5;\nint y = 3;\nSystem.out.println(x >= y);','A) true','B) false','C) Error: Incompatible types.','D) Compilation error','A',1),(136,'In Java, what is the purpose of the `catch` block in a try-catch-finally statement?','A) To define a new class.','B) To declare variables.','C) To handle exceptions.','D) To specify the default value of a variable.','C',1),(137,'What is the output of the following code snippet?\n\nint x = 5;\nSystem.out.println(x++);','A) 5','B) 6','C) 4','D) Error: Incompatible types.','A',1),(138,'In Java, what is the purpose of the `continue` statement in a loop?','A) It exits the loop and transfers control to the next iteration.','B) It continues to the next statement outside the loop.','C) It restarts the loop from the beginning.','D) It deletes the loop.','A',1),(139,'What is the purpose of the `StringBuilder` class in Java?','A) To format date and time values.','B) To create instances of classes.','C) To build and manipulate strings efficiently.','D) To perform mathematical calculations.','C',1),(140,'In Java, what is the purpose of the `switch` statement?','A) To declare variables.','B) To define constructors.','C) To specify the default value of a variable.','D) To perform different actions based on the value of an expression.','D',1),(141,'What is the result of the following code?\n\nint x = 5;\nint y = 3;\nSystem.out.println(x <= y);','A) true','B) false','C) Error: Incompatible types.','D) Compilation error','B',1),(142,'In Java, which operator is used for bitwise OR?','A) &','B) &&','C) |','D) ||','C',1),(143,'What is the purpose of the `finally` block in Java?','A) To specify the default value of a variable.','B) To handle exceptions.','C) To declare a class as abstract.','D) To declare variables.','B',1),(144,'What is the result of the following code?\n\nString s = \"Java\";\nSystem.out.println(s.contains(\"ja\"));','A) true','B) false','C) Error: Incompatible types.','D) Compilation error','A',1),(145,'In Java, which class is used to represent a collection of elements with no duplicates?','A) HashSet','B) ArrayList','C) LinkedList','D) HashMap','A',1),(146,'What is the purpose of the `for-each` loop in Java?','A) To execute a block of code repeatedly until a condition is true.','B) To declare variables.','C) To iterate over elements of an array or collection.','D) To perform mathematical calculations.','C',1),(147,'What is the output of the following code snippet?\n\nint x = 10;\nint y = 5;\nSystem.out.println(x += y);','A) 10','B) 15','C) 5','D) Error: Incompatible types.','B',1),(148,'In Java, what is the purpose of the `import` statement?','A) To define constructors.','B) To specify the default value of a variable.','C) To allow access to classes from other packages.','D) To create a new object.','C',1),(149,'What is the output of the following code?\n\nint x = 5;\nint y = 3;\nSystem.out.println(x << y);','A) 8','B) 15','C) 2','D) Error: Incompatible types.','A',1),(150,'In Java, what is the purpose of the `super` keyword?','A) To define a subclass.','B) To specify the default value of a variable.','C) To access the superclass members in a subclass.','D) To create a new object.','C',1),(151,'What is the result of the following code?\n\nint x = 5;\nint y = 3;\nSystem.out.println(x & y);','A) 5','B) 3','C) 1','D) Error: Incompatible types.','1',1),(152,'In Java, what is the purpose of the `package` statement?','A) To create a new package.','B) To declare variables.','C) To specify the default value of a variable.','D) To define the package of a class.','D',1),(153,'What is the output of the following code snippet?\n\nString s = \"Java\";\nSystem.out.println(s.toLowerCase());','A) JAVA','B) java','C) Error: Incompatible types.','D) Compilation error','B',1),(154,'In Java, which operator is used to create an array?','A) new','B) class','C) create','D) instance','A',1),(155,'What is the purpose of the `synchronized` keyword in Java?','A) To create a synchronized block of code.','B) To specify the default value of a variable.','C) To allow access to classes from other packages.','D) To create a new object.','A',1),(156,'What is the output of the following code?\n\nint x = 5;\nint y = 3;\nSystem.out.println(x | y);','A) 5','B) 3','C) 7','D) Error: Incompatible types.','7',1),(157,'In Java, which class is used to read binary data from files?','A) Scanner','B) InputStreamReader','C) FileReader','D) FileInputStream','D',1),(158,'What is the purpose of the `volatile` keyword in Java?','A) To declare a variable as constant.','B) To specify the default value of a variable.','C) To indicate that a variable may be modified by multiple threads.','D) To restrict access to class members.','C',1),(159,'What is the result of the following code?\n\nString s = \"Java\";\nSystem.out.println(s.indexOf(\"a\"));','A) 0','B) 1','C) 2','D) -1','B',1),(160,'In Java, which keyword is used to define a class as abstract?','A) extend','B) superclass','C) abstract','D) subclass','C',1);
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quizzes`
--

DROP TABLE IF EXISTS `quizzes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `quizzes` (
  `quiz_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `num_correct_answers` int NOT NULL,
  `num_wrong_answers` int NOT NULL,
  `hints_used` int NOT NULL,
  `fifty_fifty_used` int NOT NULL,
  `subject_id` int DEFAULT NULL,
  PRIMARY KEY (`quiz_id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quizzes`
--

LOCK TABLES `quizzes` WRITE;
/*!40000 ALTER TABLE `quizzes` DISABLE KEYS */;
INSERT INTO `quizzes` VALUES (1,17,4,11,0,0,1),(2,17,1,14,0,0,1),(3,17,0,1,0,0,1),(4,17,0,1,0,0,1),(5,17,0,1,0,0,1),(6,17,0,0,0,0,1),(7,17,0,0,0,0,1),(8,17,0,0,0,0,1),(9,17,0,0,0,0,1),(10,17,0,0,0,0,1),(11,17,0,0,0,0,1),(12,17,0,0,0,0,1),(13,17,0,0,0,0,1),(14,17,0,0,0,0,1),(15,17,0,0,0,0,1),(16,17,0,0,0,0,1),(17,17,0,0,0,0,1),(18,17,0,0,0,0,1),(19,17,0,0,0,0,1),(20,17,0,1,0,0,1),(21,17,0,1,0,0,1),(22,17,0,0,0,0,1),(23,17,0,0,0,0,1),(24,17,0,0,0,0,1),(25,17,0,0,0,0,1),(26,17,0,0,0,0,1),(27,17,0,0,0,0,1),(28,17,1,3,0,0,1),(29,17,1,3,0,0,1),(30,17,1,3,0,0,1),(31,17,3,3,0,0,1),(32,17,3,3,0,0,1),(33,17,3,3,0,0,1),(34,17,1,1,0,0,1),(35,17,0,0,0,0,1),(36,17,1,2,0,1,1),(37,17,2,6,3,0,1),(38,17,8,9,0,0,1),(39,17,5,10,1,2,1),(40,17,0,0,0,0,1),(41,17,0,0,0,0,1),(42,17,0,0,0,0,1),(43,17,0,0,0,0,1),(44,17,0,0,0,0,1),(45,17,0,0,0,0,1),(46,17,0,4,0,0,1),(47,17,1,1,0,0,1),(48,17,9,11,0,0,1),(49,17,9,11,0,0,1),(50,17,9,11,0,0,1),(51,17,0,0,0,0,1),(52,17,0,0,0,0,1),(53,17,3,7,0,0,1),(54,17,3,7,0,0,1),(55,17,2,6,0,0,1),(56,17,0,0,0,0,1),(57,17,0,0,0,0,1),(58,17,0,0,0,0,1),(59,17,2,4,0,0,1),(60,17,0,0,0,0,1),(61,17,0,0,0,0,1),(62,17,0,0,0,0,1),(63,17,12,6,0,0,1),(64,17,0,3,0,0,1);
/*!40000 ALTER TABLE `quizzes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `requests`
--

DROP TABLE IF EXISTS `requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `requests` (
  `id` int NOT NULL AUTO_INCREMENT,
  `subject_id` int DEFAULT NULL,
  `teacher_id` int DEFAULT NULL,
  `student_id` int DEFAULT NULL,
  `total_hours` double DEFAULT NULL,
  `slot_1` varchar(50) DEFAULT NULL,
  `slot_2` varchar(50) DEFAULT NULL,
  `slot_3` varchar(50) DEFAULT NULL,
  `status` int DEFAULT '0',
  `goal_of_tutoring` varchar(1000) DEFAULT NULL,
  `request_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `approve_time` varchar(50) DEFAULT NULL,
  `meet_link` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `requests`
--

LOCK TABLES `requests` WRITE;
/*!40000 ALTER TABLE `requests` DISABLE KEYS */;
INSERT INTO `requests` VALUES (1,10,18,17,1,'2023-09-25T10:30:00.000Z','2023-09-25T10:30:00.000Z',NULL,2,'asdf','2023-09-20 18:22:02','2023-09-25T10:30:00.000Z','https://meet.google.com/xzh-rszc-zac'),(2,10,18,17,1,'2023-09-29T09:30:00.000Z',NULL,NULL,0,'sdc ','2023-09-26 19:10:30',NULL,NULL),(3,10,18,17,1,'2023-10-13T11:30:00.000Z','2023-10-13T11:30:00.000Z','2023-10-14T10:30:00.000Z',0,'graph ','2023-10-11 22:07:10',NULL,NULL),(4,10,18,17,1,'2023-10-13T11:30:00.000Z','2023-10-13T11:30:00.000Z','2023-10-14T10:30:00.000Z',0,'graph ','2023-10-11 22:07:15',NULL,NULL);
/*!40000 ALTER TABLE `requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reviews` (
  `subject_id` int NOT NULL,
  `teacher_id` int NOT NULL,
  `student_id` int NOT NULL,
  `rating` double DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `date` datetime DEFAULT CURRENT_TIMESTAMP,
  `image_urls` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`subject_id`,`teacher_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews`
--

LOCK TABLES `reviews` WRITE;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
INSERT INTO `reviews` VALUES (10,1,1,0,'nice teaching ','2023-08-24 18:24:55','[]'),(10,18,17,4,'nice teaching','2023-09-20 18:50:36','[]'),(11,1,1,3.5,'Nice Teaching','2023-07-25 17:29:44','[]');
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subjects_info`
--

DROP TABLE IF EXISTS `subjects_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `subjects_info` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(400) DEFAULT NULL,
  `image_url` varchar(800) DEFAULT NULL,
  `parent_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subjects_info`
--

LOCK TABLES `subjects_info` WRITE;
/*!40000 ALTER TABLE `subjects_info` DISABLE KEYS */;
INSERT INTO `subjects_info` VALUES (1,'java','ZE7eEHzXe-Screenshot 2023-07-21 at 4.15.18 PM.png',0),(2,'c++','WTAopWR2X-Screenshot 2023-07-21 at 4.23.44 PM.png',0),(3,'mysql','lBhV697TN-Screenshot 2023-07-21 at 4.24.45 PM.png',0),(4,'React','zMeV5Q-wv-Screenshot 2023-07-21 at 4.25.30 PM.png',0),(5,'C language','Lqz5Sw2Bt-Screenshot 2023-07-21 at 4.26.13 PM.png',0),(6,'Node Js','JNnSMHaOv-Screenshot 2023-07-21 at 4.27.45 PM.png',0),(7,'Angular','4XO5WTXoZ-Screenshot 2023-07-21 at 4.28.48 PM.png',0),(8,'Date Stucture and Algorithm','n7c9w4iqy-Screenshot 2023-07-21 at 4.29.46 PM.png',0),(9,'Mongo DB','kI1-N068w-Screenshot 2023-07-21 at 4.30.45 PM.png',0),(10,'DS In java','Jkdw4IeWG-Screenshot 2023-07-22 at 4.51.43 PM.png',1),(11,'Core Java Theory','PpNBGx5Mv-Screenshot 2023-07-22 at 4.54.07 PM.png',1),(12,'Advance Java Theory','RS6xcwEYX-Screenshot 2023-07-22 at 4.55.54 PM.png',1),(13,'Project Code Help','kRK2Hcput-Screenshot 2023-07-22 at 4.57.09 PM.png',1);
/*!40000 ALTER TABLE `subjects_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Transactions`
--

DROP TABLE IF EXISTS `Transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Transactions` (
  `student_id` int NOT NULL,
  `teacher_id` int NOT NULL,
  `request_id` int DEFAULT NULL,
  `total_rate` double DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  PRIMARY KEY (`student_id`,`teacher_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Transactions`
--

LOCK TABLES `Transactions` WRITE;
/*!40000 ALTER TABLE `Transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `Transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tutor_subject_info`
--

DROP TABLE IF EXISTS `tutor_subject_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tutor_subject_info` (
  `teacher_id` int NOT NULL,
  `subject_id` int NOT NULL,
  `hourly_rate` double DEFAULT NULL,
  `level_of_knowledge` int DEFAULT NULL,
  `header_info` varchar(500) DEFAULT NULL,
  `other_info` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`teacher_id`,`subject_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tutor_subject_info`
--

LOCK TABLES `tutor_subject_info` WRITE;
/*!40000 ALTER TABLE `tutor_subject_info` DISABLE KEYS */;
INSERT INTO `tutor_subject_info` VALUES (18,10,200,2,'Hignly Knowledgable in java with OOPs concept','As a writing coach I like to take a hands-on approach by working with the student on their manuscript or project. Asking brain storming questions and, really diving into the heart of their narrative to help the student'),(18,11,500,1,'Hignly Knowledgable in java with OOPs ','As a writing coach I like to take a hands-on approach by working with the student on their manuscript or project. Asking brain storming questions and, really diving into the heart of their narrative to help the student'),(18,12,100,3,'Hignly Knowledgable in java with OOPs concept','As a writing coach I like to take a hands-on approach by working with the student on their manuscript or project. Asking brain storming questions and, really diving into the heart of their narrative to help the student'),(18,13,179,3,'I am really expert in this ','i have expriance with this ');
/*!40000 ALTER TABLE `tutor_subject_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `type` tinyint DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone_no` varchar(12) DEFAULT NULL,
  `image_url` varchar(800) DEFAULT NULL,
  `secound_last_education` varchar(600) DEFAULT NULL,
  `last_education` varchar(600) DEFAULT NULL,
  `current_position` varchar(1000) DEFAULT NULL,
  `bio` varchar(3000) DEFAULT NULL,
  `available_slots` varchar(700) DEFAULT NULL,
  `speaking_languaes` varchar(500) DEFAULT NULL,
  `password` varchar(800) DEFAULT NULL,
  `Monday` tinyint DEFAULT '0',
  `Tuesday` tinyint DEFAULT '0',
  `Wednesday` tinyint DEFAULT '0',
  `Thursday` tinyint DEFAULT '0',
  `Friday` tinyint DEFAULT '0',
  `Saturday` tinyint DEFAULT '0',
  `Sunday` tinyint DEFAULT '0',
  `authenticated` tinyint DEFAULT '0',
  `online` tinyint DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `phone_no` (`phone_no`),
  status tinyint default 0
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (17,'karan',1,'karankadam540@gmail.com','123456789','Uc1ehRuU3-awrapper.webp','Batu university loneri','CDAC karad','Software developer at John deer ','I graduated with my first MA from George Mason University in 2021 with a degree in English concentrating in Folklore. As an undergraduate I studied at Temple University where I got a degree',NULL,NULL,'46ed260db5a4cb33871f0b308aae3e899602cd7f20c6841677e4079d8b9e5ec3',0,0,0,0,0,0,0,1,1),(18,'Akash Nikam',0,'karanpatil951999@gmail.com','1234567890','YGEt5W4Sk-Screenshot 2023-07-22 at 5.05.36 PM.png','Batu university loneri','CDAC karad','Software developer at John deer ','I graduated with my first MA from George Mason University in 2021 with a degree in English concentrating in Folklore. As an undergraduate I studied at Temple University where I got a degree','{\"sunday\":[0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,0,1,1,1,1,1],\"monday\":[0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1],\"tuesday\":[0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1],\"wednesday\":[0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1],\"thursday\":[0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,2,1,1,1,1],\"friday\":[0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1],\"saturday\":[0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1]}','[\"English\",\"Marathi\",\"Hindi\"]','a6eac1a0ae42d3e466b45dd4610e18decdb339b59bda075853ae0229fd4edc35',1,1,1,1,1,0,1,0,1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `set_available_slots` BEFORE INSERT ON `user` FOR EACH ROW BEGIN
    IF NEW.type = 0 THEN
        SET NEW.available_slots = '{"sunday":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],"monday":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],"tuesday":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],"wednesday":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],"thursday":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],"friday":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],"saturday":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]}';
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `Wishlist`
--

DROP TABLE IF EXISTS `Wishlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Wishlist` (
  `student_id` int NOT NULL,
  `subject_id` int NOT NULL,
  `teacher_id` int NOT NULL,
  PRIMARY KEY (`student_id`,`subject_id`,`teacher_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Wishlist`
--

LOCK TABLES `Wishlist` WRITE;
/*!40000 ALTER TABLE `Wishlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `Wishlist` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-10-12 21:51:36


CREATE TABLE `groups` (
    group_id INT AUTO_INCREMENT PRIMARY KEY,
    group_name VARCHAR(100) NOT NULL,
    group_description VARCHAR(1000),
    image_url VARCHAR(500),
    created_by INT,
    FOREIGN KEY (created_by) REFERENCES user(id)
);

CREATE TABLE user_group (
    user_id INT,
    group_id INT,
    is_admin tinyint default 0,
    PRIMARY KEY (user_id, group_id),
    FOREIGN KEY (user_id) REFERENCES user(id),
    FOREIGN KEY (group_id) REFERENCES `groups`(group_id)
);



CREATE TABLE conversations (
    conversation_id INT AUTO_INCREMENT PRIMARY KEY,
    user1_id INT,
    user2_id INT,
    FOREIGN KEY (user1_id) REFERENCES user(id),
    FOREIGN KEY (user2_id) REFERENCES user(id)
);


CREATE TABLE messages (
    message_id INT AUTO_INCREMENT PRIMARY KEY,
    sender_user_id INT,
    group_id INT, -- Set to NULL for one-to-one messages
    receiver_user_id INT, -- Set to NULL for group messages
    content TEXT NOT NULL,
    file varchar(1000) DEFAULT NULL,
    timestamp DATETIME NOT NULL,
    created_at datetime DEFAULT CURRENT_TIMESTAMP,
    type enum('Text','Media','Document','Link') DEFAULT NULL,
    FOREIGN KEY (sender_user_id) REFERENCES user(id),
    FOREIGN KEY (group_id) REFERENCES `groups`(group_id),
    FOREIGN KEY (receiver_user_id) REFERENCES user(id)
);


 CREATE TABLE `friendships` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user1_id` int DEFAULT NULL,
  `user2_id` int DEFAULT NULL,
  `status` enum('pending','accepted','rejected') DEFAULT 'pending',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `last_message_id` int DEFAULT NULL,
  `unread_count` int DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user1_id` (`user1_id`),
  KEY `user2_id` (`user2_id`),
  CONSTRAINT `friendships_ibfk_1` FOREIGN KEY (`user1_id`) REFERENCES `user` (`id`),
  CONSTRAINT `friendships_ibfk_2` FOREIGN KEY (`user2_id`) REFERENCES `user` (`id`)
)